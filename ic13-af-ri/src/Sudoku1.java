import java.util.HashSet;
import java.util.Set;

/**
 * Mutable type representing Sudoku puzzles.  
 * 
 * A Sudoku puzzle is a square NxN grid of cells in which the numbers 1..N
 * should be written so that each number appears exactly once in each row, column, and block
 * (where a block is a sqrt(N) x sqrt(N) grid of cells, and the whole grid is tiled by N blocks).
 * See http://en.wikipedia.org/wiki/Sudoku for more information.
 */
public class Sudoku1 {
    
    private final int[][] puzzle;
    private final int blockSize;
    private final int puzzleSize;
    
    // Abstraction function:
    //     Represent sudoku puzzle of size puzzleSize as a set of coordinates (i,j) contains a number, or is blank.
    //Number is in range (0, puzzleSize)
    
    // Rep invariant:
    //   - TODO: some conditions are missing here
    //   - containing only numbers or blanks:
    //       puzzle[i][j] in { 0, ..., puzzleSize } for all 0 <= i, j < puzzleSize
    //  - puzzle[i].length = puzzleSize?
    //  - puzzle.length = puzzleSize
    //   - no positive number appears more than once in any row, column, or block
    //   - puzzleSize = blockSize*blockSize
    
    // Safety from rep exposure:
    //     TODO
    
    
    //////////////////////////////////////////
    // private helper methods
    
    // assert the rep invariant
    private void checkRep() {
        
        assert puzzle.length == puzzleSize;
        assert puzzleSize == blockSize*blockSize;
        for(int i=0; i<puzzleSize; ++i) {
            assert puzzle[i].length == puzzleSize;
        }
        
        Set<Integer> allowed = new HashSet<>();
        for (int number = 0; number <= puzzleSize; ++number) {
            allowed.add(number);
        }
        
        for (int row = 0; row < puzzleSize; ++row) {
            Set<Integer> found = new HashSet<>();
            for (int column = 0; column < puzzleSize; ++column) {
                int cell = puzzle[row][column];
                assert allowed.contains(cell) : "invalid number " + cell;
                assert ! found.contains(cell) : "duplicate " + cell + " in row " + row;
                if (cell != 0) {
                    found.add(cell);
                }
            }
        }
        
        for (int column = 0; column < puzzleSize; ++column) {
            Set<Integer> found = new HashSet<>();
            for (int row = 0; row < puzzleSize; ++row) {
                int cell = puzzle[row][column];
                assert allowed.contains(cell) : "invalid number " + cell;
                assert ! found.contains(cell) : "duplicate " + cell + " in column " + column;
                if (cell != 0) {
                    found.add(cell);
                }
            }
        }

        for (int block = 0; block < puzzleSize; ++block) {
            Set<Integer> found = new HashSet<>();
            for (int r = 0; r < blockSize; ++r) {
                for (int c = 0; c < blockSize; ++c) {
                    int row = (block/blockSize)*blockSize + r;
                    int column = (block%blockSize)*blockSize + c;
                    int cell = puzzle[row][column];
                    assert allowed.contains(cell) : "invalid number " + cell;
                    assert ! found.contains(cell) : "duplicate " + cell + " in block " + block;
                    if (cell != 0) {
                        found.add(cell);
                    }
                }
            }
        }
    }
    
    /**
     * @param puzzle (see rep invariant for precondition on puzzle)
     * @return a copy of puzzle, so that mutating the original puzzle
     *  won't affect the copy.
     */
    private static int[][] copyPuzzle(final int[][] puzzle) {
        final int size = puzzle.length;
        final int[][] newPuzzle = new int[size][size];
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                newPuzzle[i][j] = puzzle[i][j];
            }
        }
        return newPuzzle;
    }


    //////////////////////////////////////////
    // public operations
    
    /**
     * Make an empty Sudoku, with only blank squares, no digits.
     * @param puzzleSize size of the puzzle, must be a perfect square
     *  (typical Sudoku puzzles have puzzleSize = 9)
     */
    public Sudoku1(int puzzleSize) {
        this.puzzleSize = puzzleSize;
        this.puzzle = new int[puzzleSize][puzzleSize]; // filled with 0 by default
        this.blockSize = (int)Math.sqrt(puzzleSize);
        checkRep();
    }
    
    /**
     * Make a Sudoku and initializes its squares.
     * @param entries must be a 2D square array of dimension N x N, where
     *  entries[row][column] is a number in 1..N, or 0 to represent a blank square,
     *  and the entries follow the Sudoku row, column, and block constraints
     */
    public Sudoku1(int[][] entries) {
        this.puzzleSize = entries.length;
        this.blockSize = (int)Math.sqrt(puzzleSize);
        this.puzzle = copyPuzzle(entries);
        checkRep();
    }
    
    /**
     * Copy a Sudoku.
     * @param that Sudoku to copy
     */
    public Sudoku1(Sudoku1 that) {
        this.puzzleSize = that.puzzleSize;
        this.blockSize = that.blockSize;
        this.puzzle = copyPuzzle(that.puzzle);
        checkRep();
    }
    
    // TODO: ok to use puzzleSize in specs below? NOOOOOOOOOOOOOOOOOOOOOOOOOOOO
    
    /**
     * @param row    0 <= row < puzzleSize
     * @param column 0 <= column < puzzleSize
     * @return the set of numbers that are entered in the same row of the puzzle
     *  as the cell at [row][column], all elements in the set are in [1...puzzleSize]
     */
    public Set<Integer> getRow(int row, int column) {
        Set<Integer> numbers = new HashSet<>();
        for (int i = 0; i < puzzleSize; ++i) {
            numbers.add(puzzle[row][i]);
        }
        numbers.remove(0); // don't include empty cells
        return numbers;
    }

    /**
     * @param row    0 <= row < puzzleSize
     * @param column 0 <= column < puzzleSize
     * @return the set of numbers that are entered in the same column of the puzzle
     *  as the cell at [row][column], all elements in the set are in [1...puzzleSize]
     */
    public Set<Integer> getColumn(int row, int column) {
        Set<Integer> numbers = new HashSet<>();
        for (int i = 0; i < puzzleSize; ++i) {
            numbers.add(puzzle[i][column]);
        }
        numbers.remove(0); // don't include empty cells
        return numbers;
    }

    /**
     * @param row    0 <= row < puzzleSize
     * @param column 0 <= column < puzzleSize
     * @return the set of digits that are entered in the same block of the puzzle
     *  as the cell at [row][column], all elements in the set are in [1...puzzleSize]
     */
    public Set<Integer> getBlock(int row, int column) {
        Set<Integer> numbers = new HashSet<>();
        int firstRowOfBlock = (row / blockSize) * blockSize;
        int firstColumnOfBlock = (column / blockSize) * blockSize;
        for (int i = 0; i < puzzleSize; ++i) {
            int cellRow = firstRowOfBlock + (i / blockSize);
            int cellColumn = firstColumnOfBlock + (i % blockSize); 
            numbers.add(puzzle[cellRow][cellColumn]);
        }
        numbers.remove(0); // don't include empty cells
        return numbers;
    }
    

    /**
     * @return true if the puzzle can be solved, and modifies
     *  this puzzle to fill in blank cells with a solution.
     *  Returns false if no solution exists.
     */    
    public boolean solve() {
        // find an empty cell
        for (int row = 0; row < puzzleSize; ++row) {
            for (int column = 0; column < puzzleSize; ++column) {
                if (puzzle[row][column] == 0) {
                    // found an empty cell; try to fill it
                    Set<Integer> alreadyUsed = new HashSet<>();
                    alreadyUsed.addAll(getRow(row, column));
                    alreadyUsed.addAll(getColumn(row, column));
                    alreadyUsed.addAll(getBlock(row, column));

                    for (int number = 1; number <= puzzleSize; ++number) {
                        if ( ! alreadyUsed.contains(number)) {
                            puzzle[row][column] = number;
                            if (solve()) {
                                return true;
                            }
                            // couldn't solve it with that choice,
                            // so clear the cell again and backtrack
                            puzzle[row][column] = 0;
                        }
                    }
                    
                    return false; // nothing works for this cell! give up and backtrack
                }
            }
        }
        return true; // no empty cells found, so puzzle is already solved
    }
    
    /**
     * @return true if and only if each row, column, and block of
     *  this puzzle uses all the numbers 1...puzzleSize exactly once.
     */
    public boolean isSolved() {
        for (int row = 0; row < puzzleSize; ++row) {
            if (getRow(row, 0).size() != puzzleSize) return false;
        }
        for (int column = 0; column < puzzleSize; ++column) {
            if (getColumn(0, column).size() != puzzleSize) return false;
        }
        for (int row = 0; row < puzzleSize; row += blockSize) {
            for (int column = 0; column < puzzleSize; column += blockSize) {
                if (getBlock(row, column).size() != puzzleSize) return false;
            }
        }
        return true;
    }
    
    /**
     * @return string representation of this puzzle, suitable for printing.
     */
    @Override
    public String toString() {
        String result = "";
        for (int row = 0; row < puzzleSize; ++row) {
            if (row > 0 && row % blockSize == 0) {
                result += "\n";
            }
            for (int column = 0; column < puzzleSize; ++column) {
                if (column > 0 && column % blockSize == 0) {
                    result += " ";
                }
                int cell = puzzle[row][column];
                if (cell == 0) {
                    result += "_";
                } else {
                    result += puzzle[row][column];
                }
            }
            result += "\n";
        }
        return result;
    }
}
