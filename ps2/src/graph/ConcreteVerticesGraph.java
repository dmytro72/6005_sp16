/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An implementation of Graph.
 * 
 * <p>PS2 instructions: you MUST use the provided rep.
 */
public class ConcreteVerticesGraph<L> implements Graph<L> {
    
    private final List<Vertex<L>> vertices = new ArrayList<>();
    
    // Abstraction function:
    //     Represents a graph composed of vertices, connected by edges 
    //
    // Representation invariant:
    //   Weights must be positive 
    //  Vertices must have unique string names 
    //      
    // Safety from rep exposure:
    //   vertices is a private variable.
    
    
    /**
     * Default constructor for ConcreteVerticesGraph. Creates a new instance of ConcreteVerticesGraph
     */
   public ConcreteVerticesGraph() {
       checkRep();
   }
    
   private void checkRep(){
       for (Vertex<L> v: vertices){
           Map<L, Integer> inMap = v.getInEdges();
           Map<L, Integer> outMap = v.getOutEdges();
           
           for (Integer w: inMap.values()){
               assert w > 0;
           }
           
           for (Integer w: outMap.values()){
               assert w > 0;
           }
           
       }
   }
   
   /**
    * Finds a Vertex in vertices that has the name of string s
    * 
    * @param s, s MUST BE present in the graph
    * @return Vertex with s as its name
    */
   private Vertex<L> findVertex(L s){
       for (Vertex<L> v: vertices){
           if (v.getName().equals(s)){
               return v;
           }
       }
       return new Vertex<L>(); //This line should never be called
   }
   
   /**
    * Checks if vertex is in graph
    * @return boolean
    */
   private boolean vertexIsInGraph(L vertex){
       Set<L> set = new HashSet<>();
       
       for (Vertex<L> v: vertices){
          set.add(v.getName());
       }
       
       if (set.contains(vertex)){return true;};
       return false;
   }
   
    @Override public boolean add(L vertex) {
        if (vertexIsInGraph(vertex)){
            return false;
        }
        else{
            vertices.add(new Vertex<L>(vertex)); 
            return true;
        }
    }
    
    @Override public int set(L source, L target, int weight) {
        //Check if edge exists
        
        Vertex<L> srcVertex;
        if (vertexIsInGraph(source)){
            srcVertex = findVertex(source);
        }
        else{
            srcVertex = new Vertex<L>(source);
            if (weight != 0){
                vertices.add(srcVertex);}
        }
        
        Map<L, Integer> srcOutEdges = srcVertex.getOutEdges();

        Vertex<L> targetVertex;
        if (vertexIsInGraph(target)){
            targetVertex = findVertex(target);
        }    
        else{   
            targetVertex = new Vertex<L>(target);
            if (weight != 0){
                vertices.add(targetVertex);}
        }
       
        Map<L, Integer> targetInEdges = targetVertex.getInEdges();
        
        if (srcOutEdges.containsKey(target) && targetInEdges.containsKey(source)){
            //add edge to vertices
            Integer prevWeight = srcOutEdges.get(target);
            
            //If weight is 0, remove the edge from each vertex
            if (weight == 0){
                targetVertex.removeEdgeIn(srcVertex);
                return prevWeight;
            }
            
            srcVertex.addOutEdge(target, weight);
            targetVertex.addInEdge(source, weight);
            return prevWeight;
        }
        else{
            if (weight != 0){
                srcVertex.addOutEdge(target, weight);
                targetVertex.addInEdge(source, weight);
                return 0;
            }
        }
        
        return 0;
    }
    
    
    @Override public boolean remove(L vertex) {
        
        if (vertexIsInGraph(vertex)){
            Vertex<L> v = findVertex(vertex);
       
            //remove vertex from all vertices in list
            for (Vertex<L> temp: vertices){
                temp.removeVertex(v);
            }
            
          //remove vertex itself
            vertices.remove(v);
            return true;
        }
        else {
            return false;
        }
    }
    
    @Override public Set<L> vertices() {
        Set<L> set = new HashSet<>();
        
        for (Vertex<L> v: vertices){
           set.add(v.getName());
        }
        
        return set; 
    }
    
    @Override public Map<L, Integer> sources(L target) {
        Vertex<L> targetVertex;
        if (vertexIsInGraph(target)){
            targetVertex = findVertex(target);
        }    
        else{   
            targetVertex = new Vertex<L>(target);
            vertices.add(targetVertex);
        }       
        
        return targetVertex.getInEdges();
    }
    
    @Override public Map<L, Integer> targets(L source) {
        Vertex<L> srcVertex;
        if (vertexIsInGraph(source)){
            srcVertex = findVertex(source);
        }
        else{
            srcVertex = new Vertex<L>(source);
            vertices.add(srcVertex);
        }
              
        
        return srcVertex.getOutEdges();
    }
    
    @Override
    public String toString(){
        String s = "";
        for (Vertex<L> v: vertices){
            s = s + v.toString();
        }
        
        return s;
    }
    
}

/**
 * Mutable.
 * This class is internal to the rep of ConcreteVerticesGraph.
 *
 * Vertex has two maps, inEdges and outEdges. inEdges contains the incoming vertices with positive edge weights
 *  outEdges contains the target vertices for this vertex with positive edge weights
 * 
 * @param inEdges
 * @param outEdges
 */
class Vertex<L> {
    
    private L name;
    private Map<L, Integer> inEdges;
    private Map<L, Integer> outEdges;
    
    // Abstraction function:
    //   Represents a vertex with a set of inward edges and a set of outward edges
    //
    // Representation invariant:
    //   inEdges and outEdges contain positive weights. 
    //   
    // Safety from rep exposure:
    // All fields inEdges, outEdges, and s are private. 
    // getInEdges and getOutEdges return defensive copies of their class variables
    //
    
    
    /**
     * Constructor for Vertex, creates a new Vertex with lab
     * @param s
     */
    public Vertex(L s){
        this.name = s;
        this.outEdges = new HashMap<L, Integer>();
        this.inEdges = new HashMap<L, Integer>();
        checkRep();
    }
    
    /**
     * Default constructor for Vertex, creates a new Vertex
     */
    public Vertex(){
        checkRep();
    }
    
    private void checkRep(){
        if (inEdges != null){
            for (Integer w: inEdges.values()){
                assert w > 0;
            }
        }
        
        if (outEdges != null){
            
            for (Integer w: outEdges.values()){
                assert w > 0;
            }
        }
    }
    
    
    /**
     * Returns a copy of the incoming edges map of this vertex
     * 
     * @return map of inward edges of vertex
     */
    public Map<L, Integer> getInEdges(){
        Map<L, Integer> defCopy = new HashMap<L, Integer>();
        defCopy.putAll(inEdges);
        return defCopy;
    }
    
    
    /**
     * Returns a copy of the outgoing edges map of this vertex
     * 
     * @return map of outward edges of vertex
     */
    public Map<L, Integer> getOutEdges(){
        Map<L, Integer> defCopy = new HashMap<L, Integer>();
        defCopy.putAll(outEdges);
        return defCopy;
    }
    
    
    /**
     * Returns name of vertex
     * 
     * @return name of vertex
     */
    public L getName(){
        return name;
    }
    
    
    /**
     * Adds an edge to Vertex's inward edges map 
     * Replaces current weight value, if key present
     * 
     * @param source, name of source vertex
     * @param weight, must be >0
     */
    public void addInEdge(L source, Integer weight){
        inEdges.put(source, weight);
        checkRep();
    }
    
    /**
     * Adds an edge to Vertex's outward edges map
     * Replaces current weight value, if key present
     * 
     * @param target, name of target vertex
     * @param weight, >0
     */
    public void addOutEdge(L target, Integer weight){
        outEdges.put(target, weight);
    }
    
    
    /**
     * removes a vertex from the outEdges and inEdges maps of vertex
     * 
     * @param v, Vertex to be removed
     */
    public void removeVertex(Vertex<L> v){
        if (inEdges.containsKey(v.getName())){
            inEdges.remove(v.getName());
        }
        
        if (outEdges.containsKey(v.getName())){
            outEdges.remove(v.getName());
        }
        checkRep();
    }
    
    /**
     * Removes an edge from outEdges for this vertex, then removes edge from inEdges for source vertex 
     * 
     * @param t, Vertex to be removed
     */
    public void removeEdgeOut(Vertex<L> t){
        if (outEdges.containsKey(t.getName())){
            outEdges.remove(t.getName());
        }
        checkRep();
    }
    
    
    /**
     * Removes an edge from outEdges for this vertex, then removes edge from inEdges for source vertex 
     * 
     * @param src, Vertex to be removed
     */
    public void removeEdgeIn(Vertex<L> src){
        if (inEdges.containsKey(src.getName())){
            inEdges.remove(src.getName());
            src.removeEdgeOut(this);
        }
        checkRep();
    }
    
    @Override
     public String toString(){
         return "\n" + name + ": inEdges" + getInEdges().toString() + "\toutEdges" + getOutEdges().toString();
     }
    
}
