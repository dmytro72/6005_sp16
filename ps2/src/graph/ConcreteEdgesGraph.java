/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An implementation of Graph.
 * 
 * <p>PS2 instructions: you MUST use the provided rep.
 */
public class ConcreteEdgesGraph<L> implements Graph<L> {
    
    private final Set<L> vertices = new HashSet<>();
    private final List<Edge<L>> edges = new ArrayList<>();
    
    // Abstraction function:
    //    Represents a graph composed of vertices, connected by weighted edges
    
    // Representation invariant:
    //   Edges have positive weight
    
    // Safety from rep exposure:
    //   Set of vertices and list of edges are final, sources() and targets() 
    
    /**
     * Default constructor for ConcreteEdgesGraph
     */
    public ConcreteEdgesGraph() {
        checkRep();
    }
    
    private void checkRep(){
        for (Edge<L> e: edges){
            assert e.getWeight() > 0;
            
        }
    }
    
    @Override public boolean add(L vertex) {
        if (vertices.contains(vertex)){
            return false;
        }
        else{
            vertices.add(vertex);
            return true;
        }
    }
    
    @Override public int set(L source, L target, int weight) {
        if (weight != 0){
            if (!vertices.contains(source)){
                vertices.add(source);
            }
            
            if (!vertices.contains(target)){
                vertices.add(target);
            }
        }
        
        //check if edge exists
        for (Iterator<Edge<L>> iterator = edges.iterator(); iterator.hasNext();) {
            Edge<L> e = iterator.next();
            
            if (e.getSource().equals(source) && e.getTarget().equals(target)){
                int prevWeight  = e.getWeight();
                        
                if (weight == 0){
                    iterator.remove();
                    return prevWeight;
                }
                
                e.setWeight(weight);
                return prevWeight;
            }
        }
        
        if (weight != 0){
            Edge<L> newEdge = new Edge<L>(source, target, weight);
            
            edges.add(newEdge);
            return 0;
        }
        return 0;
    }
    
    @Override public boolean remove(L vertex) {
         if (vertices.contains(vertex)){
             //remove all edges with this vertex
             vertices.remove(vertex);
             
             for (Iterator<Edge<L>> iterator = edges.iterator(); iterator.hasNext();) {
               Edge<L> e = iterator.next();

                 if (e.getSource().equals(vertex) || e.getTarget().equals(vertex)){
                     iterator.remove();
                  }
             }  
             
             return true;
         }
         return false;
    }
    
    @Override public Set<L> vertices() {
        Set<L> newSet = new HashSet<>(vertices);
        return newSet;
    }
    
    @Override public Map<L, Integer> sources(L target) {
        Map<L, Integer> newMap = new HashMap<>();
        for (Edge<L> e: edges){
            if (e.getTarget().equals(target)){
               newMap.put(e.getSource(), e.getWeight());
               
            }
        }
        
        return newMap;
    }
    
    @Override public Map<L, Integer> targets(L source) {
        Map<L, Integer> newMap = new HashMap<>();
        for (Edge<L> e: edges){
            if (e.getSource().equals(source)){
               newMap.put(e.getTarget(), e.getWeight());
            }
        }
        
        return newMap;
    }
    
    @Override
    public String toString(){
        String s = "";
                
       for (Edge<L> edge: edges){
           s = s + edge.toString() + "\n"; 
       }
        
       return s;
    }
    
}

/**
 * Edge is an immutable object representing a graph edge, with a source vertex, target vertex
 * and non-zero integer weight
 * Immutable.
 * This class is internal to the rep of ConcreteEdgesGraph.
 * 
 * <p>PS2 instructions: the specification and implementation of this class is
 * up to you.
 */
class Edge<L> {
    
    private L source;
    private L target;
    private Integer weight;
    
    // Abstraction function:
    //   Represents an edge in a graph with a source node and a target node
    // Representation invariant:
    //   Source and target strings are not empty, weight is >0
    // Safety from rep exposure:
    //   Source and target strings are private and immutable. Weight is private and immutable.
    
    /**
     * Constructor for Edge. Creates a new Edge with a source name, target name, and weight. 
     * @param source
     * @param target
     * @param weight
     */
    public Edge(L source, L target, Integer weight){
        this.source = source;
        this.target = target;
        this.weight = weight;
        checkRep();
    }
    
    private void checkRep(){
        assert !source.equals("");
        assert !target.equals("");
        assert !weight.equals(0);
    }
    
    /**
     * Gets the source of the edge
     * @return  source
     */
    public L getSource(){
        return source;
    }

    /**
     * Gets the target of the edge
     * @return target
     */
    public L getTarget(){
        return target;
    }
    
    /**
     * Sets the source of the edge to be source
     * 
     * @param source
     */
    public void setSource(L source) {
        this.source = source;
    }

    /**
     * Sets the target of the edge to be target
     * 
     * @param target
     */
    public void setTarget(L target) {
        this.target = target;
    }

    /**
     * Sets the weight of the edge to be weight
     * 
     * @param weight
     */
    public void setWeight(Integer weight) {
        this.weight = weight;
    }
    
    /**
     * Gets the weight of the edge
     * @return weight
     */
    public Integer getWeight(){
        return weight;
    }
    
    @Override
    public String toString(){
        return source + "->" + target + "[weight = " + weight + "]";
    }
}
