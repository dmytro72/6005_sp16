/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

/**
 * Tests for instance methods of Graph.
 * 
 * <p>PS2 instructions: you MUST NOT add constructors, fields, or non-@Test
 * methods to this class, or change the spec of {@link #emptyInstance()}.
 * Your tests MUST only obtain Graph instances by calling emptyInstance().
 * Your tests MUST NOT refer to specific concrete implementations.
 */
public abstract class GraphInstanceTest {
    
    // Testing strategy:
    //   Test each method
    //   add()
    //       Vertex in graph, vertex not in graph
    //       Test against mutation
    //  remove()
    //      Vertex in graph, vertex not in graph
    //      Vertex is: a source, target, source & target
    // set()
    //      weight is 0, >0
    //      source and target present/not present in graph
    //
    //source() and target()
    //  # edges: 0, 1, >1
    //
    //   
    
    
    /**
     * Overridden by implementation-specific test classes.
     * 
     * @return a new empty graph of the particular implementation being tested
     */
    public abstract Graph<String> emptyInstance();
           
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    @Test
    public void testInitialVerticesEmpty() {
        assertEquals("expected new graph to have no vertices",
                Collections.emptySet(), emptyInstance().vertices());
    }
    
    //Test adding a vertex and re-adding itself
    @Test
    public void testAddVertices() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        
        assertTrue("test vertex added", graph.add(v1));
        
        assertFalse("test vertex already added", graph.add(v1));
        
        //Check that vertices are not mutated
        Set<String> vertices = graph.vertices();
        
        assertTrue("Graph not modified in addVertices", vertices.size() == 1);
        assertTrue("Graph not modified in addVertices 2", vertices.contains(v1));
       
    }
    
    //Test adding and then removing a vertex
    @Test
    public void testRemoveVertices() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        
        graph.add(v1);
        assertTrue("test vertex was removed true", graph.remove(v1));
        
        Set<String> vertices = graph.vertices();
        
        assertTrue("test vertex wasnt modified", vertices.isEmpty());

        assertFalse("test vertex was not present", graph.remove("v10"));

       vertices = graph.vertices();
        
        assertTrue("test graph wasnt modified", vertices.isEmpty());
    }
    
    //Test adding edges and then removing a source vertex. Should remove vertex from edges as well
    @Test
    public void testRemoveSourceVerticesInEdges() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";

        
        graph.add(v1);
        graph.add(v2);
        graph.add(v3);

        
        assertTrue("Graph contains vertices", graph.vertices().contains(v1));
        assertTrue("Graph contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        assertEquals("new edge returns zero", 0, graph.set(v1, v3, 1));
        
        graph.remove(v1);
        
        Map<String, Integer> v2Sources = graph.sources(v2);
        Map<String, Integer> v3Sources = graph.sources(v3);
        
        assertTrue("v2Sources not contains v1", !v2Sources.containsKey(v1));
        assertTrue("v3Sources not contains v1", !v3Sources.containsKey(v1));
    }
    
    //Test adding edges and then removing a target vertex. Should remove vertex from edges as well
    @Test
    public void testRemoveTargetVerticesInEdges() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        
        
        graph.add(v1);
        graph.add(v2);
        graph.add(v3);
        
        
        assertTrue("Graph contains vertices", graph.vertices().contains(v1));
        assertTrue("Graph contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        assertEquals("new edge returns zero", 0, graph.set(v3, v2, 1));
        
        graph.remove(v2);
        
        Map<String, Integer> v1Targets = graph.targets(v1);
        Map<String, Integer> v3Targets = graph.targets(v3);
        
        assertTrue("v1Targets not contains v1", !v1Targets.containsKey(v2));
        assertTrue("v3Targets not contains v1", !v3Targets.containsKey(v2));
    }
    
    //Test adding edges and then removing a target vertex. Should remove vertex from edges as well
    @Test
    public void testRemoveSourceTargetVerticesInEdges() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        
        
        graph.add(v1);
        graph.add(v2);
        graph.add(v3);
        
        
        assertTrue("Graph contains vertices", graph.vertices().contains(v1));
        assertTrue("Graph contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        assertEquals("new edge returns zero", 0, graph.set(v2, v1, 1));
        assertEquals("new edge returns zero", 0, graph.set(v3, v2, 1));
        
        graph.remove(v2);
        
        Map<String, Integer> v1Targets = graph.targets(v1);
        Map<String, Integer> v1Sources = graph.sources(v1);
        Map<String, Integer> v3Targets = graph.targets(v3);
        
        assertTrue("v1Targets not contains v2", !v1Targets.containsKey(v2));
        assertTrue("v1Sources not contains v1", !v3Targets.containsKey(v2));
        assertTrue("v1Sources not contains v2", !v1Sources.containsKey(v2));
    }
    
    //Test adding an edge, modifying its weight, and removing it
    @Test
    public void testAddEdge() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);
        graph.add(v2);
        
        assertTrue("Graph contains vertices", graph.vertices().contains(v1));
        assertTrue("Graph contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        assertEquals("old edge returns 1", 1, graph.set(v1, v2, 1));
        
        Map<String, Integer> v1Targets = graph.targets(v1);
        Map<String, Integer> v2Sources = graph.sources(v2);
        
        assertTrue("Edge is present 1", v1Targets.containsKey(v2));
        assertTrue("Edge is present 2", v2Sources.containsKey(v1));
        
        assertEquals("removed edge, returns 1", 1, graph.set(v1, v2, 0));
        
        v1Targets = graph.targets(v1);
        v2Sources = graph.sources(v2);
        
        assertTrue("Edge was removed 1", !v1Targets.containsKey(v2));
        assertTrue("Edge was removed 2", !v2Sources.containsKey(v1));


    }
    
    //Test when sources and targets may not be in graph
    @Test
    public void testAddEdgeNotInGraph() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        

        assertFalse("Graph not 1 contains vertices", graph.vertices().contains(v1));
        assertFalse("Graph not 2 contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        
        
        assertTrue("Graph contains vertices 1", graph.vertices().contains(v1));
        assertTrue("Graph contains vertices 2", graph.vertices().contains(v2));
        
        assertEquals("old edge returns 1", 1, graph.set(v1, v2, 1));
        
        Map<String, Integer> v1Targets = graph.targets(v1);
        Map<String, Integer> v2Sources = graph.sources(v2);
        
        assertTrue("Edge is present 1", v1Targets.containsKey(v2));
        assertTrue("Edge is present 2", v2Sources.containsKey(v1));
        
        assertEquals("removed edge, returns 1", 1, graph.set(v1, v2, 0));
        
        v1Targets = graph.targets(v1);
        v2Sources = graph.sources(v2);
        
        assertTrue("Edge was removed 1", !v1Targets.containsKey(v2));
        assertTrue("Edge was removed 2", !v2Sources.containsKey(v1));
    }
    
  //Test when source and target not in graph
    @Test
    public void testSetRemoveVerticesNotInGraph() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        

        assertFalse("Graph not 1 contains vertices", graph.vertices().contains(v1));
        assertFalse("Graph not 2 contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 0));
        
        assertFalse("Graph not contains vertices 1. Vertex should never have been added", graph.vertices().contains(v1));
        assertFalse("Graph not contains vertices 2", graph.vertices().contains(v2));
        
    }
    
  //Test when sources and targets are in graph, but edge is not
    @Test
    public void testSetRemoveEdgeNotInGraphButVerticesAre() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);
        graph.add(v2);

        assertTrue("Graph 1 contains vertices", graph.vertices().contains(v1));
        assertTrue("Graph 2 contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 0));
        
        Set<String> vs = graph.vertices();
        
        
        assertTrue("Graph contains vertices 1", graph.vertices().contains(v1));
        assertTrue("Graph contains vertices 2", graph.vertices().contains(v2));
        
        Map<String, Integer> v1Targets = graph.targets(v1);
        Map<String, Integer> v2Sources = graph.sources(v2);
        
        
        assertTrue("Edge not present never present 1", !v1Targets.containsKey(v2));
        assertTrue("Edge not present never present 2", !v2Sources.containsKey(v1));
        
    }
    
  //Test when target is not be in graph
    @Test
    public void testSetRemoveEdgeNotInGraphButSourceIs() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);

        assertTrue("Graph 1 contains vertices", graph.vertices().contains(v1));
        assertFalse("Graph not 2 contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 0));
        
    Set<String> vs = graph.vertices();
        
        
        assertTrue("Graph contains vertices 1", graph.vertices().contains(v1));
        assertFalse("Graph not contains vertices 2", graph.vertices().contains(v2));
      
    }
    
    //Test remove edge when source is not in graph
    @Test
    public void testSetRemoveEdgeNotInGraphButTargetIs() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v2);
        
        assertFalse("Graph not 1 contains vertices", graph.vertices().contains(v1));
        assertTrue("Graph 2 contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 0));
        
        assertFalse("Graph not contains vertices 1. Vertex should never have been added", graph.vertices().contains(v1));
        assertTrue("Graph contains vertices 2", graph.vertices().contains(v2));
        
    }
    
    //Test adding an edge when one vertex is not in the graph
    @Test
    public void testAddEdgeOneNotInGraph() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);
        
        assertTrue("Graph 1 contains vertices", graph.vertices().contains(v1));
        assertFalse("Graph not 2 contains vertices", graph.vertices().contains(v2));
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        
        assertTrue("Graph contains vertices 1", graph.vertices().contains(v1));
        assertTrue("Graph contains vertices 2", graph.vertices().contains(v2));
        
        assertEquals("old edge returns 1", 1, graph.set(v1, v2, 1));
        
        Map<String, Integer> v1Targets = graph.targets(v1);
        Map<String, Integer> v2Sources = graph.sources(v2);
        
        assertTrue("Edge is present 1", v1Targets.containsKey(v2));
        assertTrue("Edge is present 2", v2Sources.containsKey(v1));
        
        assertEquals("removed edge, returns 1", 1, graph.set(v1, v2, 0));
        
        v1Targets = graph.targets(v1);
        v2Sources = graph.sources(v2);
        
        assertTrue("Edge was removed 1", !v1Targets.containsKey(v2));
        assertTrue("Edge was removed 2", !v2Sources.containsKey(v1));
    }
    
    //Test vertices in graph
    @Test
    public void testVertices() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);
        graph.add(v2);
        
        Set<String> vertices = graph.vertices();

        assertEquals("Two vertices in set", vertices.size(),  2 );
        assertTrue("Two vertices in set 2", vertices.contains(v1));
        assertTrue("Two vertices in set 3", vertices.contains(v2));
        
    }
    
    //Test when vertex has one target
    @Test
    public void testTargetsOneEdge() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);
        graph.add(v2);
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        
        Map<String, Integer> map = graph.targets(v1);
        
        assertTrue("map contains v2", map.containsKey(v2));
        assertTrue("v2 weight", map.get(v2).equals(new Integer(1)));
        assertTrue("map size 1", map.size() == 1);

    }
    
    //test when vertex has multiple targets
    @Test 
    public void testTargetsManyEdges() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        
        graph.add(v1);
        graph.add(v2);
        graph.add(v3);
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        assertEquals("new edge returns zero", 0, graph.set(v1, v3, 2));

        assertEquals("new edge returns zero 2", 0, graph.set(v2, v3, 1));
        
        Map<String, Integer> map = graph.targets(v1);
        
        assertTrue("map contains v2", map.containsKey(v2));
        assertTrue("v2 weight", map.get(v2).equals(new Integer(1)));
        assertTrue("map contains v3", map.containsKey(v3));
        assertEquals("v3 weight", map.get(v3), new Integer(2));
        assertTrue("map size 2", map.size() == 2);

    }
    
    //Test targets when no edges
    @Test
    public void testTargetsNoEdges() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);
        graph.add(v2);
        
        Map<String, Integer> map = graph.targets(v1);
        
        assertTrue(map.size() == 0);
    }
    
    //Test when vertex has one source
    @Test
    public void testSourceOneEdge() {
        
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);
        graph.add(v2);
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        
        Map<String, Integer> map = graph.sources(v2);
        
        assertTrue("map contains v1", map.containsKey(v1));
        assertTrue("v1 weight", map.get(v1).equals(new Integer(1)));
        assertTrue("map size 1", map.size() == 1);
    }
    
    //Test source when there are many edges
    @Test
    public void testSourceManyEdges() {
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        
        graph.add(v1);
        graph.add(v2);
        graph.add(v3);
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        assertEquals("new edge returns zero", 0, graph.set(v1, v3, 2));

        assertEquals("new edge returns zero 2", 0, graph.set(v2, v3, 1));
        
        Map<String, Integer> map = graph.sources(v3);
        
        assertTrue("map contains v1", map.containsKey(v1));
        
        assertEquals("v1 weight", map.get(v1), new Integer(2));
        assertTrue("map contains v2", map.containsKey(v2));
        
        assertTrue("v2 weight", map.get(v2).equals(new Integer(1)));
        assertTrue("map size 2", map.size() == 2);
    }
    
    //Test source when there are no edges
    @Test
    public void testSourceNoEdges() {
        
        Graph<String> graph = emptyInstance();
        
        String v1 = "v1";
        String v2 = "v2";
        
        graph.add(v1);
        graph.add(v2);
        
        assertEquals("new edge returns zero", 0, graph.set(v1, v2, 1));
        
        Map<String, Integer> map = graph.sources(v2);
        
        assertTrue(map.size() == 1);
    }
}
