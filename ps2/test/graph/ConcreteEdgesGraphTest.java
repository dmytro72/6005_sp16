/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests for ConcreteEdgesGraph.
 * 
 * This class runs the GraphInstanceTest tests against ConcreteEdgesGraph, as
 * well as tests for that particular implementation.
 * 
 * Tests against the Graph spec should be in GraphInstanceTest.
 */
public class ConcreteEdgesGraphTest extends GraphInstanceTest {
    
    /*
     * Provide a ConcreteEdgesGraph for tests in GraphInstanceTest.
     */
    @Override public Graph<String> emptyInstance() {
        return new ConcreteEdgesGraph<String>();
    }
    
    /*
     * Testing ConcreteEdgesGraph...
     */
    
    // Testing strategy for ConcreteEdgesGraph.toString()
    //    # edges: 0, 1, > 1
    //    # vertices: 1, 2, 3  
    
    //Test toString
    @Test
    public void testToStringOneEdge(){
        Graph<String> graph = emptyInstance();
        graph.add("v1");
        graph.add("v2");
        
        graph.set("v1", "v2", 1);
        System.out.println(graph.toString());
        
        String expected = "v1->v2[weight = 1]\n";
        
        assertEquals("Test edge graph tostring", expected, graph.toString());
    }

    @Test
    public void testToStringManyEdge(){
        Graph<String> graph = emptyInstance();
        graph.add("v1");
        graph.add("v2");
        graph.add("v3");
        
        graph.set("v1", "v2", 1);
        graph.set("v1", "v3", 2);
        System.out.println(graph.toString());
        
        String expected = "v1->v2[weight = 1]\nv1->v3[weight = 2]\n";
        
        assertEquals("Test many edge graph tostring", expected, graph.toString());
    }
    
    /*
     * Testing Edge...
     */
    
    // Testing strategy for Edge
    // getSource, getWeight, getTarget 
    // setSource, setWeight, setTarget                                                               
    
    @Test
    public void testGet(){
       Edge<String> e1 = new Edge<>("v1", "v2", 1);
       assertEquals("Test edge graph tostring", e1.getTarget(), "v2");
       assertEquals("Test edge graph tostring", e1.getSource(), "v1");
       assertEquals("Test edge graph tostring", e1.getWeight(), new Integer(1));
    }
    
    @Test
    public void testSet(){
        Edge<String> e1 = new Edge<>("v1", "v2", 1);
        e1.setSource("v3");
        e1.setTarget("v4");
        e1.setWeight(2);
        assertEquals("Test edge graph tostring", e1.getTarget(), "v4");
        assertEquals("Test edge graph tostring", e1.getSource(), "v3");
        assertEquals("Test edge graph tostring", e1.getWeight(), new Integer(2));
    }
    
}
