/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

/**
 * Tests for ConcreteVerticesGraph.
 * 
 * This class runs the GraphInstanceTest tests against ConcreteVerticesGraph, as
 * well as tests for that particular implementation.
 * 
 * Tests against the Graph spec should be in GraphInstanceTest.
 */
public class ConcreteVerticesGraphTest extends GraphInstanceTest {
    
    /*
     * Provide a ConcreteVerticesGraph for tests in GraphInstanceTest.
     */
    @Override public Graph<String> emptyInstance() {
        return new ConcreteVerticesGraph<String>();
    }
    
    /*
     * Testing ConcreteVerticesGraph...
     */
    
    // Testing strategy for ConcreteVerticesGraph.toString()
    //    # edges: 0, 1, > 1
    //    # vertices: 1, 2, 3  
    //    
    
    
    //Test toString
    @Test
    public void testToStringManyVertex(){
        Graph<String> graph = emptyInstance();
        graph.add("v1");
        graph.add("v2");
        graph.add("v3");
        
        graph.set("v1", "v2", 1);
        graph.set("v1", "v3", 2);
        
        String expected = "\nv1: inEdges{}\toutEdges{v2=1, v3=2}\nv2: inEdges{v1=1}\toutEdges{}\n"
                + "v3: inEdges{v1=2}\toutEdges{}";
        
        assertEquals("Test vertex graph tostring", expected, graph.toString());
    }
    
    
    /*
     * Testing Vertex...
     */
    
    // Testing strategy for Vertex

    // getName()  
    //      
    // addInEdge() and getInEdges()
    //      0, 1, >1 edges
    //     Edge weight is 0 (remove)
    //     Edge weight changed
    //      Edge added
    //
    // addOutEdge() and getOutEdges()
    //      0, 1, >1 edges
    //     Edge weight is 0 (remove)
    //     Edge weight changed
    //      Edge added
    //
    // removeVertex
    //  Vertex in graph
    // vertex not in graph
    //
    //  toString()
    //   
    //
    
    @Test
    public void testGetInEdgesNone(){
        Vertex<String> v1 = new Vertex<>("v1");
        
        Map<String, Integer> map = v1.getInEdges();
        
//        v1.addInEdge("v2", 1);
        
        assertTrue("No inEdges Map is empty", map.isEmpty());
        
    }

    @Test
    public void testGetInEdgesOne(){
        Vertex<String> v1 = new Vertex<>("v1");
        
        v1.addInEdge("v2", 1);

        
        Map<String, Integer> map = v1.getInEdges();
      
      assertTrue("inEdges Map is size 1", map.size() == 1);
      assertTrue("Contains v2", map.containsKey("v2"));
        
    }
    
    @Test
    public void testGetInEdgesMany(){
        Vertex<String> v1 = new Vertex<>("v1");
        
        v1.addInEdge("v2", 1);
        v1.addInEdge("v3", 1);

        
        Map<String, Integer> map = v1.getInEdges();
        
      
      assertTrue("inEdges Map is size 1", map.size() == 2);
      assertTrue("Contains v2", map.containsKey("v2"));
      assertTrue("Contains v3", map.containsKey("v3"));
    }
    
    @Test
    public void testGetOutEdgesNone(){
        Vertex<String> v1 = new Vertex<>("v1");
                
        Map<String, Integer> map = v1.getOutEdges();
        
      
      assertTrue("outEdges Map is size 1", map.size() == 0);
    }
    
    @Test
    public void testGetOutEdgesOne(){
        Vertex<String> v1 = new Vertex<>("v1");
        
        v1.addOutEdge("v2", 1);
        
        Map<String, Integer> map = v1.getOutEdges();
        
        assertTrue("inEdges Map is size 1", map.size() == 1);
        assertTrue("Contains v2", map.containsKey("v2"));
    }
    
    @Test
    public void testGetOutEdgesMany(){
        Vertex<String> v1 = new Vertex<>("v1");
        
        v1.addOutEdge("v2", 1);
        v1.addOutEdge("v3", 1);
        
        Map<String, Integer> map = v1.getOutEdges();
        
        assertTrue("outEdges Map is size 2", map.size() == 2);
        assertTrue("Contains v2", map.containsKey("v2"));
        assertTrue("Contains v3", map.containsKey("v3"));
        
    }
    
    @Test    
    public void testGetName(){
        Vertex<String> v1 = new Vertex<>("v1");
        
        assertEquals("TEst get name", v1.getName(), "v1"); 
        
    }
    
    
    @Test
    public void testRemoveVertex(){
        Vertex<String> v1 = new Vertex<>("v1");
        Vertex<String> v2 = new Vertex<>("v2");
        
        v1.addOutEdge("v2", 1);
        v1.addOutEdge("v3", 1);
        
        Map<String, Integer> map = v1.getOutEdges();


        assertTrue("outEdges Map is size 2", map.size() == 2);
        assertTrue("Contains v2", map.containsKey("v2"));
        assertTrue("Contains v3", map.containsKey("v3"));
        
        
       v1.removeVertex(v2);
       
       map = v1.getOutEdges();

       assertTrue("outEdges Map is now size 1", map.size() == 1);
       assertTrue("not contains v2", !map.containsKey("v2"));
       assertTrue("Contains v3", map.containsKey("v3"));
       
    }
    
    
    
    //Test toString
    @Test
    public void testVertexToString(){
        Vertex<String> v1 = new Vertex<>("v1");
        
        v1.addOutEdge("v2", 1);
        v1.addInEdge("v3", 2);
        
        String expected = "\nv1: inEdges{v3=2}\toutEdges{v2=1}";
        
        assertEquals("Test edge graph tostring", expected, v1.toString());
    }
    
    
}
