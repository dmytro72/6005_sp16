/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package poet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

/**
 * Tests for GraphPoet.
 */
public class GraphPoetTest {
    
    // Testing strategy for poem()
    //   Test # of bridge words: 0, 1, >1
    // Test # of highest-weight bridge words: 1, >1
    // Test case-insensitivity
    //Test multiple lines
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    //Test adding one bridge word in poem
    @Test
    public void testOneBridgeWordCaseInsensitive() throws IOException{
        final GraphPoet poet = new GraphPoet(new File("test/poet/testOneBridgeWord.txt"));
        
        String poem = poet.poem("NAME hello");
        
        assertEquals("Poem with bridge word", poem, "NAME of hello");
        
    }
    
    //Test having no bridge words in poem
    @Test
    public void testNoBridgeWords() throws IOException{
        final GraphPoet poet = new GraphPoet(new File("test/poet/testOneBridgeWord.txt"));
        
        String poem = poet.poem("No BRIDGE WORDS");

        assertEquals("Poem with no bridge words", poem, "No BRIDGE WORDS");
        
    }
    
    //Test having multiple bridge words to choose from
    @Test
    public void testManyBridgeWordCaseInsensitive() throws IOException{
        final GraphPoet poet = new GraphPoet(new File("test/poet/testManyBridgeWords.txt"));
        
        String poem = poet.poem("NAME hello");
        
        assertEquals("Poem with many bridge word", poem, "NAME of hello");
        
    }
    
    //Test having multiple lines and spaces in poem
    @Test
    public void testManyBridgeWordManyLines() throws IOException{
        final GraphPoet poet = new GraphPoet(new File("test/poet/testPoemManyLines.txt"));
        
        String poem = poet.poem("NAME hello");
        
        assertEquals("Poem with many bridge word", poem, "NAME is hello");
        
    }

    //Test having multiple bridge words with same weight
    @Test
    public void testManyBridgeWordManyWeights() throws IOException{
        final GraphPoet poet = new GraphPoet(new File("test/poet/testOneBridgeWordManyWeights.txt"));
        
        String poem = poet.poem("NAME hello");
        
        assertTrue("Poem with many bridge word", poem.equals("NAME is hello") 
                || poem.equals("NAME of hello"));
        
    }
    
    //Test on many worlds
    @Test
    public void testManyWorlds() throws IOException{
        final GraphPoet poet = new GraphPoet(new File("test/poet/testManyWorlds.txt"));
        
        String poem = poet.poem("Seek to explore new and exciting synergies!");
        
        assertEquals("nimoy", poem, "Seek to explore strange new life and exciting synergies!");
        
    }
}


