/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package turtle;

import java.util.List;
import java.util.ArrayList;

public class TurtleSoup {
    
    private static final double QUARTER_ANGLE = 90;
    private static final double HALF_ANGLE = 180;
    private static final double FULL_ANGLE = 360;


    /**
     * Draw a square.
     * 
     * @param turtle the turtle context
     * @param sideLength length of each side
     */
    public static void drawSquare(Turtle turtle, int sideLength) {
        
        int numSides = 4;
        
        for (int i = 0; i < numSides; i++){
            turtle.forward(sideLength);
            turtle.turn(QUARTER_ANGLE);
        }
    }

    /**
     * Determine inside angles of a regular polygon.
     * 
     * There is a simple formula for calculating the inside angles of a polygon;
     * you should derive it and use it here.
     * 
     * @param sides number of sides, where sides must be > 2
     * @return angle in degrees, where 0 <= angle < 360
     */
    public static double calculateRegularPolygonAngle(int sides) {
           return HALF_ANGLE*(sides-2)/sides;
    }

    /**
     * Determine number of sides given the size of interior angles of a regular polygon.
     * 
     * There is a simple formula for this; you should derive it and use it here.
     * Make sure you *properly round* the answer before you return it (see java.lang.Math).
     * HINT: it is easier if you think about the exterior angles.
     * 
     * @param angle size of interior angles in degrees, where 0 < angle < 180
     * @return the integer number of sides
     */
    public static int calculatePolygonSidesFromAngle(double angle) {
        return (int) Math.round(FULL_ANGLE/(HALF_ANGLE-angle));
    }

    /**
     * Given the number of sides, draw a regular polygon.
     * 
     * (0,0) is the lower-left corner of the polygon; use only right-hand turns to draw.
     * 
     * @param turtle the turtle context
     * @param sides number of sides of the polygon to draw
     * @param sideLength length of each side
     */
    public static void drawRegularPolygon(Turtle turtle, int sides, int sideLength) {
        double interiorAngle = calculateRegularPolygonAngle(sides);
        
        for (int i = 0; i < sides; i++){
        turtle.forward(sideLength);
        turtle.turn(HALF_ANGLE-interiorAngle);
        }
    }

    /**
     * Given the current direction, current location, and a target location, calculate the heading
     * towards the target point.
     * 
     * The return value is the angle input to turn() that would point the turtle in the direction of
     * the target point (targetX,targetY), given that the turtle is already at the point
     * (currentX,currentY) and is facing at angle currentHeading. The angle must be expressed in
     * degrees, where 0 <= angle < 360. 
     *
     * HINT: look at http://en.wikipedia.org/wiki/Atan2 and Java's math libraries
     * 
     * @param currentHeading current direction as clockwise from north
     * @param currentX current location x-coordinate
     * @param currentY current location y-coordinate
     * @param targetX target point x-coordinate
     * @param targetY target point y-coordinate
     * @return adjustment to heading (right turn amount) to get to target point,
     *         must be 0 <= angle < 360
     */
    public static double calculateHeadingToPoint(double currentHeading, int currentX, int currentY,
                                                 int targetX, int targetY) {
          //Rethought my implementation of calculateHeadingToPoint, simplified most of the code. 
          //
        
        double xDistance = targetX - currentX;
        double yDistance = targetY - currentY;
        
        double wantedAngle = Math.atan2(xDistance, yDistance);

        wantedAngle = Math.floorMod((int)(Math.toDegrees(wantedAngle)), (int)FULL_ANGLE);
        
        //Calculate the difference between the turtle's current heading and the angle it should have
        double angleToTurn = wantedAngle - currentHeading;
        
        if (angleToTurn < 0){
            angleToTurn+=FULL_ANGLE;
        }
        
        return angleToTurn;
        
    }

    /**
     * Given a sequence of points, calculate the heading adjustments needed to get from each point
     * to the next.
     * 
     * Assumes that the turtle starts at the first point given, facing up (i.e. 0 degrees).
     * For each subsequent point, assumes that the turtle is still facing in the direction it was
     * facing when it moved to the previous point.
     * You should use calculateHeadingToPoint() to implement this function.
     * 
     * @param xCoords list of x-coordinates (must be same length as yCoords)
     * @param yCoords list of y-coordinates (must be same length as xCoords)
     * @return list of heading adjustments between points, of size 0 if (# of points) == 0,
     *         otherwise of size (# of points) - 1
     */
    public static List<Double> calculateHeadings(List<Integer> xCoords, List<Integer> yCoords) {
        //Overlooked a bug where I did not change the heading of the turtle after every step, only reassigning it to the output of
        //calculateHeadingToPoint. Fixed at line 158.
        List<Double> listOfHeadings = new ArrayList<Double>();
        
        if (xCoords.size() == 0 || yCoords.size() == 0){
            return listOfHeadings;
        }
        
        int currentX = xCoords.get(0); 
        int currentY = yCoords.get(0);
        int targetX, targetY;
        
        double turtleHeading = 0;
        
        for (int i = 1; i < xCoords.size(); i++){
            
            targetX = xCoords.get(i);
            targetY = yCoords.get(i);
            
            double headingShift = calculateHeadingToPoint(turtleHeading, currentX, currentY, targetX, targetY);
            
            currentX = targetX; 
            currentY = targetY;
            
            turtleHeading = (headingShift+turtleHeading)%360; 

            listOfHeadings.add(headingShift);
        }
        
        return listOfHeadings;
    }

    /**
     * Draw your personal, custom art.
     * 
     * Many interesting images can be drawn using the simple implementation of a turtle.  For this
     * function, draw something interesting; the complexity can be as little or as much as you want.
     * 
     * @param turtle the turtle context
     */
    public static void drawPersonalArt(Turtle turtle) {
        
        int sidelength = 900; //Included this magic number because it has no meaning other than a drawing scale factor

        turtle.turn(QUARTER_ANGLE);
        fractal(turtle, sidelength);
        turtle.turn(HALF_ANGLE);
        fractal(turtle, sidelength);


    }
    
    /**
     * Recursive method to draw a fractal
     * 
     * @param turtle the turtle context
     * @param sidelength, a scale factor
     */
    public static void fractal(Turtle turtle, int sidelength){
        int minLength = 3;
        int divFactor = 3;
        int numSidesToTurn = 4;
        int interiorAngle = 45;
        
        //base case of fractal
        if (sidelength < minLength){
            turtle.forward(sidelength);
        }
        else{
            //draw the fractal
            
            turtle.turn(-QUARTER_ANGLE);
            
            for (int i = 0; i < numSidesToTurn; i++){
                fractal(turtle, sidelength/divFactor);
                turtle.turn(interiorAngle);
            }
            
            fractal(turtle, sidelength/divFactor);
            turtle.turn(-QUARTER_ANGLE);
        }
            
    }
    

    /**
     * Main method.
     * 
     * This is the method that runs when you run "java TurtleSoup".
     * 
     * @param args unused
     */
    public static void main(String args[]) {
        DrawableTurtle turtle = new DrawableTurtle();

//        drawSquare(turtle, 40);
//        drawRegularPolygon(turtle, 5, 40);
        drawPersonalArt(turtle);
        // draw the window
        turtle.draw();
    }

}
