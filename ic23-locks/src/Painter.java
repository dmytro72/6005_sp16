import java.util.Collection;
import java.util.Collections;
import java.util.Set;

// Original painter
// Don't change me, change the versions below
class Painter {
    
    private Set<Brush> brushes;
    private Palette palette;
    
    public Painter(Set<Brush> brushes) {
        this.brushes = brushes;
        this.palette = new Palette();
    }
    public void mix() {
        // mixing
    }
    public void paint() {
        // painting
    }
}

class Brush {
    // ...
}

class Palette {    
    // ...
}

///////////////////////////////////////////////////////

// ImmutablePainter is immutable
class ImmutablePainter {
    
    
    
    private Set<Brush> brushes;
    private Palette palette;
    
    //Safety from rep exposure:
    // in constrcutor, make defensive copy of brushes
    //
    //
    //
    
    // Thread safety argument:
    //    Uses a synchronized set, threadsafe data type method

    public ImmutablePainter(Set<Brush> brushes) {
        this.brushes = Collections.synchronizedSet( brushes);
        this.palette = new Palette();
    }
    public void mix() {
        // mixing
    }
    public void paint() {
        // painting
    }
}

// MonitorPainter implements the monitor pattern
class MonitorPainter {
    
    private Set<Brush> brushes;
    private Palette palette;
    
    // Thread safety argument:
    //    
    
    public MonitorPainter(Set<Brush> brushes) {
        this.brushes = brushes;
        this.palette = new Palette();
    }
    public synchronized void mix() {
        // mixing
    }
    public synchronized void paint() {
        // painting
    }
}

// LockPainter protects all access to its rep using the lock on brushes
class LockPainter {
    // TODO: change some of the code in this class
    
    private Set<Brush> brushes;
    private Palette palette;
    
    // Thread safety argument:
    //  rep protected by lock on brushes
    // need to address rep exposure
    
    //Brush must be threadsafe
    
    public LockPainter(Set<Brush> brushes) {
        this.brushes = brushes;
        this.palette = new Palette();
    }
    public void mix() {
        // mixing
    }
    public void paint() {
        // painting
    }
}

// NoLocksPainter knows that Brush and Palette are threadsafe, so doesn't use locks
class NoLocksPainter {
    // TODO: change some of the code in this class
    
    private Set<Brush> brushes;
    private Palette palette;
    
    // Thread safety argument:
    //    TODO
    
    public NoLocksPainter(Set<Brush> brushes) {
        this.brushes = brushes;
        this.palette = new Palette();
    }
    public void mix() {
        // mixing
    }
    public void paint() {
        // painting
    }
}
