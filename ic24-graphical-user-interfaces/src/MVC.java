import java.awt.BorderLayout;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

public class MVC extends JFrame {
    
    // required because JFrame is a serializable class
    private static final long serialVersionUID = 1;
    
    public MVC() {
        super("MVC"); // sets the window title
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        
        // make a listbox that displays integers
        JList<Integer> list = new JList<Integer>();
        this.add(list);
        
        // use the built-in Swing list model
        DefaultListModel<Integer> model = new DefaultListModel<Integer>();
        for (int i = 1; i <= 20; ++i) {
            model.addElement(i);
        }
        list.setModel(model);
        
        // TODO #1: make the JList scrollable
        JScrollPane scroll = new JScrollPane(list);
        this.add(scroll);
        
        // TODO #2: make this custom list model work
         SomeNegativeModel model1 = new SomeNegativeModel();
         list.setModel(model1);
        
        // TODO #3: make negation work
         model1.negate(3);
        
        // TODO #4: negate every number that the user selects in the listbox
        //          (hint: use list.getSelectedIndices() in your listener)
        // list.addListSelectionListener(new ListSelectionListener() { ... });
        
        // TODO #5: reset all the numbers back to positive values when the user presses Escape
        //          (hint: use KeyEvent's getKeyCode() method, and VK_ESCAPE constant)
        // list.addKeyListener(new KeyAdapter() { ... });
        
        pack();
        setVisible(true);
    }
    
    private static int LIST_SIZE = 1000 * 1000;
    
    // SomeNegativeModel is a list model representing the positive integers from 1 to LIST_SIZE
    // in order, but with a small number of integers replaced by their negations.
    class SomeNegativeModel extends AbstractListModel<Integer> {
        
        // required because AbstractListModel is a serializable class
        private static final long serialVersionUID = 1;
        
        private final Set<Integer> negated = new HashSet<>(); 
        
        
        @Override public int getSize() {
            return LIST_SIZE;
        }
        
        @Override public Integer getElementAt(int index) {
            return index++;
        }
        
        /**
         * Modify this list model so that it displays value negated.
         * @param value integer to negate, requires 1 <= value <= getSize()
         */
        public void negate(int value) {
            if (value >= 1 && value <= LIST_SIZE){
                negated.add(value-1);
            }
        }
        
        /**
         * Modify this list model so that all values are positive again.
         */
        public void reset() {
            negated.clear();
        }
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new MVC();
            }
        });
    }
}
