import java.util.HashSet;
import java.util.Set;

/**
 * Solver for Sudoku puzzles.
 * 
 * A Sudoku puzzle is a square NxN grid of cells in which the numbers 1..N
 * should be written so that each number appears exactly once in each row, column, and block
 * (where a block is a sqrt(N) x sqrt(N) grid of cells, and the whole grid is tiled by N blocks).
 * See http://en.wikipedia.org/wiki/Sudoku for more information.
 * 
 * In the methods of this class, the int[][] puzzle parameter has the following preconditions:
 *  - it's a square array PUZZLE_SIZE x PUZZLE_SIZE:
 *        puzzle.length = PUZZLE_SIZE
 *        puzzle[i].length = PUZZLE_SIZE for all 0 <= i < PUZZLE_SIZE
 *  - contains only valid numbers or blanks:
 *        puzzle[i][j] in { 0, ..., PUZZLE_SIZE } for all 0 <= i, j < PUZZLE_SIZE
 *  - no positive number appears more than once in any row, column, or block
 */
public class Sudoku {
    public static final int BLOCK_SIZE = 3;
    public static final int PUZZLE_SIZE = BLOCK_SIZE * BLOCK_SIZE;
    
    /**
     * @param puzzle (see above for precondition on puzzle)
     * @param row    0 <= row < PUZZLE_SIZE
     * @param column 0 <= column < PUZZLE_SIZE
     * @return the row of the puzzle that contains the cell at [row][column],
     *  using 1...PUZZLE_SIZE for cells that contain numbers, and 0 for empty cells.
     */
    public static int[] getRow(int[][] puzzle, int row, int column) {
        int[] cells = new int[PUZZLE_SIZE];
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            cells[i] = puzzle[row][i];
        }
        return cells;
    }

    /**
     * @param puzzle (see above for precondition on puzzle)
     * @param row    0 <= row < PUZZLE_SIZE
     * @param column 0 <= column < PUZZLE_SIZE
     * @return the column of the puzzle that contains the cell at [row][column],
     *  using 1...PUZZLE_SIZE for cells that contain numbers, and 0 for empty cells.
     */
    public static int[] getColumn(int[][] puzzle, int row, int column) {
        int[] cells = new int[PUZZLE_SIZE];
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            cells[i] = puzzle[i][column];
        }
        return cells;
    }

    /**
     * @param puzzle (see above for precondition on puzzle)
     * @param row    0 <= row < PUZZLE_SIZE
     * @param column 0 <= column < PUZZLE_SIZE
     * @return the block of the puzzle that contains the cell at [row][column],
     *  using 1...PUZZLE_SIZE for cells that contain numbers, and 0 for empty cells.
     *  The block's cells are represented in the returned array from left to right,
     *  top to bottom.
     */
    public static int[] getBlock(int[][] puzzle, int row, int column) {
        int[] cells = new int[PUZZLE_SIZE];
        int firstRowOfBlock = (row / BLOCK_SIZE) * BLOCK_SIZE;
        int firstColumnOfBlock = (column / BLOCK_SIZE) * BLOCK_SIZE;
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            int cellRow = firstRowOfBlock + (i / BLOCK_SIZE);
            int cellColumn = firstColumnOfBlock + (i % BLOCK_SIZE);
            cells[i] = puzzle[cellRow][cellColumn];
        }
        return cells;
    }
    

    /**
     * @param puzzle (see above for precondition on puzzle)
     * @return true if the puzzle can be solved, and modifies
     *  puzzle to fill in blank cells with a solution.
     *  Returns false if no solution exists.
     */    
    public static boolean solve(int[][] puzzle) {
        // find an empty cell
        assertValid(puzzle);
        
        for (int row = 0; row < PUZZLE_SIZE; ++row) {
            for (int column = 0; column < PUZZLE_SIZE; ++column) {
                if (puzzle[row][column] == 0) {
                    // found an empty cell; try to fill it
                    Set<Integer> alreadyUsed = new HashSet<>();
                    alreadyUsed.addAll(toSet(getRow(puzzle, row, column)));
                    alreadyUsed.addAll(toSet(getColumn(puzzle, row, column)));
                    alreadyUsed.addAll(toSet(getBlock(puzzle, row, column)));

                    for (int number = 1; number <= PUZZLE_SIZE; ++number) {
                        if ( ! alreadyUsed.contains(number)) {
                            puzzle[row][column] = number;
                            if (solve(puzzle)) {
                                return true;
                            }
                        }
                    }
                    
                    return false; // nothing works for this cell! give up and backtrack
                }
            }
        }
        return true; // no empty cells found, so puzzle is already solved
    }
    
    /**
     * @param puzzle (see above for precondition on puzzle)
     * @return true if and only if each row, column, and block of
     *  the puzzle uses all the numbers 1...PUZZLE_SIZE exactly once.
     */
    public static boolean isSolved(int[][] puzzle) {
        // make the set of expected numbers
        Set<Integer> solved = new HashSet<>();
        for (int number = 1; number <= PUZZLE_SIZE; ++number) {
            solved.add(number);
        }
        // look at each row and column (using cells on the diagonal)
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            if ( ! toSet(getRow(puzzle, i, i)).equals(solved)) {
                return false;
            }
            if ( ! toSet(getColumn(puzzle, i, i)).equals(solved)) {
                return false;
            }
        }
        // look at each block (using the upper-left cells)
        for (int row = 0; row < PUZZLE_SIZE; row += BLOCK_SIZE) {
            for (int column = 0; column < PUZZLE_SIZE; column += PUZZLE_SIZE) {
                if ( ! toSet(getBlock(puzzle, row, column)).equals(solved)) {
                    return false;
                }
            }
        }
        return true; // all rows, columns, and blocks are correct
    }
    
    /**
     * @param puzzle (see above for precondition on puzzle)
     * @return string representation of puzzle, suitable for printing.
     */
    public static String toString(int[][] puzzle) {
        String result = "";
        for (int row = 0; row < PUZZLE_SIZE; ++row) {
            if (row > 0 && row % BLOCK_SIZE == 0) {
                result += "\n";
            }
            for (int column = 0; column < PUZZLE_SIZE; ++column) {
                if (column > 0 && column % BLOCK_SIZE == 0) {
                    result += " ";
                }
                int cell = puzzle[row][column];
                if (cell == 0) {
                    result += "_";
                } else {
                    result += puzzle[row][column];
                }
            }
            result += "\n";
        }
        return result;
    }
    
    /**
     * @param puzzle (see above for precondition on puzzle)
     * @return a copy of puzzle, so that mutating the original puzzle
     *  won't affect the copy.
     */
    public static int[][] copyPuzzle(int[][] puzzle) {
        int[][] newPuzzle = new int[PUZZLE_SIZE][PUZZLE_SIZE];
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            for (int j = 0; j < PUZZLE_SIZE; ++j) {
                newPuzzle[i][j] = puzzle[i][j];
            }
        }
        return newPuzzle;
    }
    
    /**
     * @param array
     * @return a set of the values in array
     */
    public static Set<Integer> toSet(int[] array) {
        Set<Integer> set = new HashSet<>();
        for (int value : array) {
            set.add(value);
        }
        return set;
    }

    // asserts that puzzle follows the precondition at the top of this class
    private static void assertValid(int[][] puzzle) {
        assert puzzle.length == PUZZLE_SIZE : "wrong number of rows";
        for (int[] row : puzzle) {
            assert row.length == PUZZLE_SIZE : "wrong number of columns";
        }
        
        Set<Integer> allowed = new HashSet<>();
        for (int number = 0; number <= PUZZLE_SIZE; ++number) {
            allowed.add(number);
        }
        
        for (int row = 0; row < PUZZLE_SIZE; ++row) {
            Set<Integer> found = new HashSet<>();
            for (int column = 0; column < PUZZLE_SIZE; ++column) {
                int cell = puzzle[row][column];
                assert allowed.contains(cell) : "invalid number " + cell;
                assert ! found.contains(cell) : "duplicate " + cell + " in row " + row;
                if (cell != 0) {
                    found.add(cell);
                }
            }
        }
        
        for (int column = 0; column < PUZZLE_SIZE; ++column) {
            Set<Integer> found = new HashSet<>();
            for (int row = 0; row < PUZZLE_SIZE; ++row) {
                int cell = puzzle[row][column];
                assert allowed.contains(cell) : "invalid number " + cell;
                assert ! found.contains(cell) : "duplicate " + cell + " in column " + column;
                if (cell != 0) {
                    found.add(cell);
                }
            }
        }

        for (int block = 0; block < PUZZLE_SIZE; ++block) {
            Set<Integer> found = new HashSet<>();
            for (int r = 0; r < BLOCK_SIZE; ++r) {
                for (int c = 0; c < BLOCK_SIZE; ++c) {
                    int row = (block/BLOCK_SIZE)*BLOCK_SIZE + r;
                    int column = (block%BLOCK_SIZE)*BLOCK_SIZE + c;
                    int cell = puzzle[row][column];
                    assert allowed.contains(cell) : "invalid number " + cell;
                    assert ! found.contains(cell) : "duplicate " + cell + " in block " + block;
                    if (cell != 0) {
                        found.add(cell);
                    }
                }
            }
        }
    }
}
