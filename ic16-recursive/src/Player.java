/**
 * TODO specification
 */
public class Player {
    
    // Abstraction function:
    //   Represents a person with a name and skill
    // Representation invariant:
    //   TODO
    // Safety from rep exposure:
    //   TODO
    
    // TODO fields
    /*
     * 
     */
    
    private String name;
    private int skill;
    
   
    /**
     * TODO specification
     */
    public Player(String name, int skill) {
        this.name = name;
        this.skill = skill;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

}
