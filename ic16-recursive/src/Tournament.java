import java.util.List;

/**
 * Represents an immutable single-elimination tournament.
 */
public interface Tournament {
    
    /**
     * @param player the only player in the tournament
     * @return tournament with only the given player
     */
    public static Tournament single(Player player) {
//        return new Tournament(Player);
    }
    
    /**
     * @param player1 one player in the match
     * @param player2 the second player in the match
     * @return tournament with a match between the two players 
     */
    public static Tournament match(Player player1, Player player2) {
        throw new RuntimeException("unimplemented");
    }
    
    /**
     *  
     *  @Param tournament
     * @return Player who won the tournament 
     */ 
    public static Player winner(Tournament tourn) {
        throw new RuntimeException("unimplemented");
    }
    
    
}

// In general, define every class in its own .java file.
// But for this exercise, write your concrete variants below,
//   and don't use the "public" modifier on the classes.

class TODO implements Tournament {
    
    // Abstraction function:
    //   Represents a tournament of players
    // Representation invariant:
    //   
    // Safety from rep exposure:
    //   
    
    // TODO fields
    
    // TODO constructor
    
    // TODO @Override winner
    
}
