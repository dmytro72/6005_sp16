import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Words {
    
    
    public static String mostCommonWords(String list){
        
    }
    
    public static List<String> splitIntoWords(String text) {
        // ...
    }
    
    /**
     * 
     * @return hash table, where keys are the strings and values are the counts of the strings. 
     */
    // count how many times each word appears in the input
    public static Map<String, Integer> countOccurrences(List<String> words) {
        // ...
    }
    
    /**
     * 
     * @return String, where 
     */
    // find the input word with the highest frequency count
    public static String findMostFrequent(Map<String, Integer> frequencies) {
        // ...
    }
    
}
