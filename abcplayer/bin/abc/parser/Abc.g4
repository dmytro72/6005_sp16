/**
 * This file is the grammar file used by ANTLR.
 *
 * In order to compile this file, navigate to this folder
 * (src/abc/parser) and run the following command:
 * 
 * java -jar ../../../lib/antlr.jar Abc.g4
 * then Refresh in Eclipse.
 *
 */

grammar Abc;
import Configuration; 

abctune : abcheader abcmusic EOF;

abcheader : fieldnumber comment* fieldtitle otherfields* fieldkey;

fieldnumber : 'X:' WHITESPACE? DIGIT+ endofline;
fieldtitle : 'T:' WHITESPACE? text* endofline;
otherfields : fieldcomposer | fielddefaultlength | fieldmeter | fieldtempo | fieldvoice ;
fieldcomposer : 'C:' WHITESPACE? text* endofline;
fielddefaultlength : 'L:' WHITESPACE? notelengthstrict endofline;
fieldmeter : 'M:' WHITESPACE? meter endofline;
fieldtempo : 'Q:' WHITESPACE? tempo endofline;
fieldvoice : 'V:' WHITESPACE? text endofline;
fieldkey : 'K:' WHITESPACE? key endofline;

key : keynote modeminor?;
keynote : basenote keyaccidental?;
keyaccidental : '#' | 'b';
modeminor : 'm';

meter : 'C' | 'C|' | meterfraction;
meterfraction : DIGIT+ '/' DIGIT+;

tempo : meterfraction '=' DIGIT+;

abcmusic : (voice | comment)+;
voice: fieldvoice bar+ endofline? | bar+ endofline?;
bar: newmajorsection? nthrepeat? repeatstart? (element | WHITESPACE)+ repeatend? (barline | endofline);
element : noteelement | tupletelement; 
noteelement : note | multinote;

note : noteorrest notelength?;
noteorrest : pitch | rest;
pitch : accidental? basenote octave?;
accidental : '^' | '^^' | '_' | '__' | '=';
basenote : 'C' | 'D' | 'E' | 'F' | 'G' | 'A' | 'B'
        | 'c' | 'd' | 'e' | 'f' | 'g' | 'a' | 'b';
octave : '\''+ | ','+;
rest : 'z';
notelength : '/' | (DIGIT+)? ('/' (DIGIT+)?)?;
notelengthstrict : DIGIT+ '/' DIGIT+;

multinote : '[' note+ ']';

tupletelement : tupletspec noteelement+;
tupletspec : '(' DIGIT;

barline : '|' | '[|' | '|]';
newmajorsection: ']' | '|';
repeatstart: ':';
repeatend: ':';
nthrepeat : '[1' | '[2';

endofline : comment | NEWLINE;
comment : '%' WHITESPACE? text*  NEWLINE;

DIGIT : [0-9] ;
LETTER : [a-zA-Z] ;
PUNCTUATION : (','|'!'|'?'|'\''|':'|'.') ;
WHITESPACE : ' ' | '\t';
NEWLINE : '\n' | '\r' '\n'?;

text : (DIGIT | LETTER | WHITESPACE | PUNCTUATION | 'D' | 'F' | 'G' | 'A' | 'B' | 'd' | 'e' | 'f' | 'g' | 'a' | 'b'| 
'c' | 'z' | 'E' | '__' | '#' | 'b'| 'm'|'C' | '[2' | '(' | '[' | ']' | '|' | '||' | '[|' | '|]' | ':' | '[1'
| 'C|' | '/' | '=' | ',' | '^' | '\'' | '^^' | '_' | '%') ;


/* Tell Antlr to ignore spaces around tokens. */
SPACES : [ ]+ -> skip;
