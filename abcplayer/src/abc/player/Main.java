package abc.player;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import org.antlr.v4.gui.Trees;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import abc.parser.AbcLexer;
import abc.parser.AbcParser;
import abc.sound.Music;

/**
 * Main entry point of your application.
 */
public class Main {
    
    /**
     * Plays the input file using Java MIDI API and displays
     * header information to the standard output stream.
     * 
     * (Your code should not exit the application abnormally using
     * System.exit().)
     * 
     * @param file the name of input abc file
     * @throws URISyntaxException 
     */
    public static void play(String file) throws IOException, URISyntaxException{
//        System.out.println(FileSystems.getDefault().getPath(".."));
        
//        final String BOARDS_PKG = "sample_abc/";
//        System.out.println(System.getProperty("user.dir"));
//
//        final URL boardURL = ClassLoader.getSystemClassLoader().getResource("sample_abc/" + "piece1.abc");
//        System.out.println(boardURL);
        
        
        FileReader in = new FileReader(new File("sample_abc/" + file));
        BufferedReader br = new BufferedReader(in); 

        StringBuilder sb = new StringBuilder();
        String line;
        while((line = br.readLine()) != null)
        { 
            line = line.trim(); // remove leading and trailing whitespace
            if (!line.equals("")) // don't write out blank lines
            {
                sb.append(line);
                sb.append("\n");
            }
        }
        
        in.close();
//        
//        for (String s : lines)
//        {   
//            System.out.println(s);
//            System.out.println(s == "\r");
//            if (s != "" && s != null){
//                
//            }    
//        }
//        
        System.out.println(sb);
        
            CharStream stream = new ANTLRInputStream(sb.toString());
            
            AbcLexer lexer = new AbcLexer(stream);
            lexer.reportErrorsAsExceptions();
            
            TokenStream tokens = new CommonTokenStream(lexer);
            
            AbcParser parser = new AbcParser(tokens);
            parser.reportErrorsAsExceptions();
            ParseTree tree = parser.abctune();
            
            Trees.inspect(tree, parser);
            
            MakeAbcTune makeAbcTune = new MakeAbcTune();
            
            ParseTreeWalker walker= new ParseTreeWalker();
            walker.walk(makeAbcTune, tree);

            Music music = makeAbcTune.getMusic();
            music.play(10);
        
    }

    public static void main(String[] args) throws IOException, URISyntaxException{
        Queue<String> arguments = new LinkedList<String>(Arrays.asList(args));
        try {
            while ( ! arguments.isEmpty()) {
                String filename = arguments.remove();
                play(filename);
            }
        }
        catch (IllegalArgumentException iae) {
        return;
        }
    }
}
