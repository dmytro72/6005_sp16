package abc.player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import abc.parser.AbcListener;
import abc.parser.AbcParser;
import abc.parser.AbcParser.AbcheaderContext;
import abc.parser.AbcParser.AbcmusicContext;
import abc.parser.AbcParser.AbctuneContext;
import abc.parser.AbcParser.AccidentalContext;
import abc.parser.AbcParser.BarContext;
import abc.parser.AbcParser.BarlineContext;
import abc.parser.AbcParser.BasenoteContext;
import abc.parser.AbcParser.CommentContext;
import abc.parser.AbcParser.ElementContext;
import abc.parser.AbcParser.EndoflineContext;
import abc.parser.AbcParser.FieldcomposerContext;
import abc.parser.AbcParser.FielddefaultlengthContext;
import abc.parser.AbcParser.FieldkeyContext;
import abc.parser.AbcParser.FieldmeterContext;
import abc.parser.AbcParser.FieldnumberContext;
import abc.parser.AbcParser.FieldtempoContext;
import abc.parser.AbcParser.FieldtitleContext;
import abc.parser.AbcParser.FieldvoiceContext;
import abc.parser.AbcParser.KeyContext;
import abc.parser.AbcParser.KeyaccidentalContext;
import abc.parser.AbcParser.KeynoteContext;
import abc.parser.AbcParser.MeterContext;
import abc.parser.AbcParser.MeterfractionContext;
import abc.parser.AbcParser.ModeminorContext;
import abc.parser.AbcParser.MultinoteContext;
import abc.parser.AbcParser.NewmajorsectionContext;
import abc.parser.AbcParser.NoteContext;
import abc.parser.AbcParser.NoteelementContext;
import abc.parser.AbcParser.NotelengthContext;
import abc.parser.AbcParser.NotelengthstrictContext;
import abc.parser.AbcParser.NoteorrestContext;
import abc.parser.AbcParser.NthrepeatContext;
import abc.parser.AbcParser.OctaveContext;
import abc.parser.AbcParser.OtherfieldsContext;
import abc.parser.AbcParser.PitchContext;
import abc.parser.AbcParser.RepeatendContext;
import abc.parser.AbcParser.RepeatstartContext;
import abc.parser.AbcParser.RestContext;
import abc.parser.AbcParser.TempoContext;
import abc.parser.AbcParser.TextContext;
import abc.parser.AbcParser.TupletelementContext;
import abc.parser.AbcParser.TupletspecContext;
import abc.parser.AbcParser.VoiceContext;
import abc.sound.Bar;
import abc.sound.Chord;
import abc.sound.Element;
import abc.sound.Music;
import abc.sound.Note;
import abc.sound.Pitch;
import abc.sound.Repetition;
import abc.sound.Rest;
import abc.sound.Tuplet;
import abc.sound.Voice;

/**
 * Creates an immutable instance of the Music data type by walking
 * the parsed grammar tree of a abc file
 *
 */
public class MakeAbcTune implements AbcListener {
    private Music finalMusicPiece;
    private Stack<Element> stack = new Stack<>();
    private String index = "";
    private String title = "";
    private String composer = "Unknown Composer"; // Default composer is unknown
    private String key = "";
    private String meter = "";
    private String length = "";
    private int tempo = 100; // Default tempo is 100 bpm
    private final Map<String, List<Element>> voices = new HashMap<>();
    private String accidental = "=";    // Default accidental is'=' -> natural
    private String octave = "";
    private char basenote;
    private final int defaultLength = 12;   // used to convert to ticks in exitNote
    private boolean isPitch;
    private boolean beginRepeatSymbol = false;
    private boolean repeatSection = false;
    private boolean firstRepeatEnding = false;
    private final Map<String, Integer> accidentals = new HashMap<>();
    
    
    // Abstraction function:
    // this.finalMusicPiece is used to store the final Music piece. Because it isn't a variant of Element,
    // it cannot be placed in this.stack
    //
    // this.stack contains the Element value of each parse
    // subtree that has been fully-walked so far, but whose parent has not yet
    // been exited by the walk. The stack is ordered by recency of visit, so that
    // the top of the stack is the Element for the most recently walked
    // subtree.
    // At the start of the walk, the stack is empty, because no subtrees have
    // been fully walked.
    // Whenever a node is exited by the walk, the Element values of its
    // children are on top of the stack, in order with the last child on top. To
    // preserve the invariant, we must pop those child Element values
    // from the stack, combine them with the appropriate Element
    // producer, and push back an Element value representing the entire
    // subtree under the node.
    // At the end of the walk, stack will have stored in it all the voices of the Music piece,
    // which must then be popped and combined to create the this.finalMusicPiece
    //
    // this.index is the index number of the music piece
    //
    // this.title is used to keep track of the title of the music piece
    //
    // this.composer is used to keep track of the composer of the music piece
    //
    // this.key is used to keep track of the key of the music piece
    //
    // this.meter represents the meter of the music piece
    // 
    // this.length represents the default length or duration of a note in the music piece
    //
    // this.tempo is the number of beats per minute of the music piece, and is used to create a Sequence Player
    // for the music piece.
    //
    // this.accidental is used to store the accidental found in a Pitch to be used to create a Note
    //
    // this.octave is used to store the octave variation found in a Pitch to be used to create a Note
    //
    // this.basenote is used to store the base note of a Pitch to be used to create a Note
    //
    // this.defaultLength is the default length/duration of a Note
    //
    // this.isPitch is used to differentiate between a Note or a Rest when creating an object in exitNote
    //
    // this.beginRepeatBar is used to keep track of whether we walked over a begin repeat bar symbol, so that
    // when we find an end repeat bar symbol, we know correctly specify the repeat status of the bar before,
    // which is Repetition.REPEAT if a begin repeat bar symbol was found, or Repetition.REPEAT_ALL if a begin repeat bar
    // symbol is not found.
    //
    // this.repeatSection is used to keep track of whether we are currently on a repeat section in the music, so that we
    // correctly specify the repeat status of following bars
    //
    // this.firstRepeatEnding is used to keep track of whether we are currently on a first repeat section in the music, so that we
    // correctly specify the repeat status of following bars
    //
    // this.secondRepeatEnding is used to keep track of whether we are currently on a second repeat section in the music, so that we
    // correctly specify the repeat status of following bars
    //
    // this.accidentals is used to store whether a given note currently has an accidental affected on it.
    // the key of the map is the note, while the value is the amount of accidentals.
    // -2 = two flats, -1 = one flate, 1 = one sharp, 2 = two sharps
    // this map should be emptied after each bar
    // 
    // Representation invariant:
    // this.defaultLength = 12
    // this.tupletNum = 2 || this.tupletNum = 3, || this.tupletNum = 4
    //
    // Safety from rep exposure:
    // All fields are private
    // Some fields are immutable
    // Some fields are final
    
    /**
     * Returns the music constructed by this listener object.
     * Requires that this listener has completely walked over a Music
     * parse tree using ParseTreeWalker.
     * @return Music for the parse tree that was walked
     */
    public Music getMusic() {
        return this.finalMusicPiece;
    }

    @Override
    public void exitAbctune(AbctuneContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitAbcheader(AbcheaderContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }
    
    @Override
    public void exitFieldnumber(FieldnumberContext ctx) {
        List<TerminalNode> titleChars = ctx.DIGIT();
        
        for(TerminalNode character: titleChars) {
            this.index += character.getText();
        }
    }

    @Override
    public void exitFieldtitle(FieldtitleContext ctx) {
        if(ctx.text() != null) {
            List<AbcParser.TextContext> titleChars = ctx.text();
            
            for(AbcParser.TextContext character: titleChars) {
                this.title += character.getText();
            }
        }
    }
    
    @Override
    public void exitOtherfields(OtherfieldsContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }
    
    @Override
    public void exitFieldcomposer(FieldcomposerContext ctx) {
        if(ctx.text() != null) {
            List<AbcParser.TextContext> composerChars = ctx.text();
            
            for(AbcParser.TextContext character: composerChars) {
                this.composer += character.getText();
            }
        }
    }

    @Override
    public void exitFielddefaultlength(FielddefaultlengthContext ctx) {
        this.length = ctx.notelengthstrict().getText();
    }

    @Override
    public void exitFieldmeter(FieldmeterContext ctx) {
        this.meter = ctx.meter().getText();
    }

    @Override
    public void exitFieldtempo(FieldtempoContext ctx) {
        String tempo = ctx.tempo().getText(); // this is in the form 1/4=100; we just want 100
        int equalsIndex = tempo.indexOf('=');
        this.tempo = Integer.parseInt(tempo.substring(equalsIndex + 1));
    }
    
    @Override
    public void exitFieldvoice(FieldvoiceContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitFieldkey(FieldkeyContext ctx) {
        this.key = ctx.key().getText();
    }

    @Override
    public void exitKey(KeyContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information   
    }

    @Override
    public void exitKeynote(KeynoteContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information  
    }

    @Override
    public void exitKeyaccidental(KeyaccidentalContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information  
    }

    @Override
    public void exitModeminor(ModeminorContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information  
    }
    
    @Override
    public void exitMeter(MeterContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitMeterfraction(MeterfractionContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information  
    }

    @Override
    public void exitTempo(TempoContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information  
    }
    
    @Override
    public void exitAbcmusic(AbcmusicContext ctx) {
        // extracts voices from this.voices map
        Collection<List<Element>> voices = this.voices.values();
        
        // pushes each voice to the stack
        for(List<Element> voice: voices) {
            Voice musicVoice = new Voice(voice);
            stack.push(musicVoice);
        }
        
        List<Voice> musicVoices = new ArrayList<>();
        int numVoices = stack.size();
        
        for (int i = 0; i < numVoices; i++) {
            Voice voice = (Voice) stack.pop();
            musicVoices.add(voice);
        }
        
        try {
            this.finalMusicPiece = new Music(this.index, this.title, this.composer, this.key, this.meter, this.length, this.tempo, this.defaultLength, musicVoices);
        } catch (MidiUnavailableException | InvalidMidiDataException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void enterVoice(VoiceContext ctx) {
        String voiceName;
        // gets voice name of voice
        if(ctx.fieldvoice() != null) {
            voiceName = ctx.fieldvoice().getText();
            int indexSpace = voiceName.indexOf(' ');
            voiceName = voiceName.substring(indexSpace + 1);
        } else {
            voiceName ="Untitled Voice";
        }
        
        // adds voice to this.voices if not already in there
        if(!this.voices.containsKey(voiceName)) {
            List<Element> voiceBars = new ArrayList<>();
            this.voices.put(voiceName, voiceBars);
        }
    }

    @Override
    public void exitVoice(VoiceContext ctx) {
        String voiceName;
        // gets voice name of voice
        if(ctx.fieldvoice() != null) {
            voiceName = ctx.fieldvoice().getText();
            int indexSpace = voiceName.indexOf(' ');
            voiceName = voiceName.substring(indexSpace + 1);
        } else {
            voiceName ="Untitled Voice";
        }
        
        List<Element> barList = new ArrayList<>();
        List<AbcParser.BarContext> bars = ctx.bar();
        assert stack.size() >= bars.size();
        
        assert bars.size() > 0;
        Element bar = stack.pop(); // this is the last bar of the voice
        
        barList.add(bar);
        


        for (int i = 1; i < bars.size(); i++) {
            bar = stack.pop();
            barList.add(bar);
        }

        
        // bars is in list backwards, so this puts them in correct order
        Collections.reverse(barList);  
        // add bars in correct order to the list of bars for voice in question
        for(Element element: barList) {
            this.voices.get(voiceName).add(element);
        }
    }
    
    @Override
    public void exitBar(BarContext ctx) {
        Repetition repetition;
        
        if(ctx.newmajorsection() != null) {
            repetition = Repetition.START_MAJOR_SECTION;
        } else if (ctx.nthrepeat() != null) {
            String nthrepeat = ctx.nthrepeat().getText();
            if (nthrepeat.equals("[1")) {
                repetition = Repetition.FIRST_ENDING;
                this.firstRepeatEnding = true;  // we have encounterd a first repeat ending symbol
            } else {
                repetition = Repetition.SECOND_ENDING; // we have encounterd a first repeat ending symbol
            }
        } else if (ctx.repeatstart() !=null) {
            repetition = Repetition.REPEAT_START;
            this.beginRepeatSymbol = true; // we have encountered a beginRepeatSymbol
            this.repeatSection = true;  // firstRepeatEnding
        } else if (ctx.repeatend() !=null) {
            if(beginRepeatSymbol) {
                repetition = Repetition.REPEAT_END;
                this.beginRepeatSymbol = false; // we have reached end of repeat section, so we must make false
                this.repeatSection = false; // we have reached end of repeat section, so we must make false
            } else {
                repetition = Repetition.REPEAT_ALL;
            }
            this.firstRepeatEnding = false; // we have reached end of repeat section, so we must make false
            
        } else if (firstRepeatEnding) {
            repetition = Repetition.FIRST_ENDING;
        } else if (repeatSection) {
            repetition = Repetition.REPEAT;
        } else {
            repetition = Repetition.NORMAL;
        }
        
        List<AbcParser.ElementContext> elements = ctx.element();
        assert stack.size() >= elements.size();
       
        assert elements.size() > 0;
        Element bar = stack.pop();
        
        // if we only have one note element
        if (stack.size() == 0) {
            Element zeroRest = new Rest(0);
            bar = new Bar(zeroRest, bar, repetition);
        } else {
         // pop the older children, one by one, and add them on
            for (int i = 1; i < elements.size(); ++i) {
                bar = new Bar(stack.pop(), bar, repetition);   
            }
        }
        stack.push(bar);
        this.accidentals.clear(); // at the end of a bar accidentals are reset
    }
    
    @Override
    public void exitElement(ElementContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information  
    }
    
    @Override
    public void exitNoteelement(NoteelementContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitNote(NoteContext ctx) {
        double multiplier = 1;  // will be the final number that must be multiplied by default notelength
        
        // will get notelength
        if (ctx.notelength() != null) {
            int numerator;      // will store numerator of notelength (if it has)
            int denominator;    // will store denominator of notelength (if it has)
            
            String notelength = ctx.notelength().getText();
            
            if(!notelength.equals("")) {
             // will enter if notelength is "/" or "/2" (or variant of this)
                if (notelength.charAt(0) == '/') {
                    notelength = "1" + notelength;
                    // will enter if notelength was "/"
                    if (notelength.length() == 2) {
                        notelength += "2";
                    }
                }
                // will enter if notelength is "1/"
                if (notelength.length() == 2 && notelength.charAt(1) == '/') {
                    notelength += "2";
                }
                

                // if notelength was originally an integer
                if(!notelength.contains("/")) {
                    multiplier = Integer.parseInt(notelength);
                } else {
                    // at this point notelength will be of the form INTEGER / INTEGER
                    numerator = Integer.parseInt(notelength.substring(0,1));
                    denominator = Integer.parseInt(notelength.substring(2));
                    multiplier = (double) numerator/denominator;
                } 
            }
        }
        
        if (isPitch) {
            String octave4 = "abcdefg"; // all notes in fourth octave
            int transpose = 0;
            char currentBasenote;
            
            // converts octave 4 basenotes into octave 3 basenotes with a transpostion of 12, because Pitch can only take octave 3 notes as arguments
            if(octave4.contains(basenote + "")) {
                currentBasenote = (basenote + "").toUpperCase().charAt(0);
                transpose += 12;
            } else {
                currentBasenote = basenote;
            }
            
            Pitch pitch = new Pitch(currentBasenote);
            
            if (!accidental.equals("=")) {
                String pitchValue = this.basenote + this.octave;
                int value = 0;
                if (accidental.charAt(0) == '^') {
                    value += accidental.length(); // will be one or two based on number of '^' characters
                    transpose += value;
                } else {
                    // will come here if accidental is "_" or "__"
                    value += -1 * accidental.length();
                    transpose += value;
                }
                // this stores the accidental for future notes that have unwritten accidental effects
                if (value != 0) {
                    this.accidentals.put(pitchValue, value);
                }
                this.accidental = "="; // reset accidental to "=" for next note

              // checks if previous notes in bar of same pitch had accidentals
            } else {
                String pitchValue = this.basenote + this.octave;
                if(this.accidentals.containsKey(pitchValue)) {
                    transpose += this.accidentals.get(pitchValue);
                }
            }
            
            if (octave.length() > 0) {
                int transposeOctave;
                if (octave.charAt(0) == ',') {
                    transposeOctave = -12 * octave.length();
                } else {
                    transposeOctave = 12 * octave.length();
                }
                transpose += transposeOctave;
                this.octave = ""; // reset octave to "" for next note
            }
            
            pitch = pitch.transpose(transpose);
            int notelength = (int) (defaultLength * multiplier);
            Note note = new Note(notelength, pitch);
            stack.push(note);
        } else {
            int notelength = (int) (defaultLength * multiplier);
            Rest rest = new Rest(notelength);
            stack.push(rest);
        }
    }

    @Override
    public void exitNoteorrest(NoteorrestContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitPitch(PitchContext ctx) {
        accidental = ctx.accidental() == null ? "=": ctx.accidental().getText();
        octave = ctx.octave() == null ? "": ctx.octave().getText();
        basenote = ctx.basenote().getText().charAt(0);
        isPitch = true;
    }

    @Override
    public void exitOctave(OctaveContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitNotelength(NotelengthContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitNotelengthstrict(NotelengthstrictContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitAccidental(AccidentalContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitBasenote(BasenoteContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitRest(RestContext ctx) {
        isPitch = false;
    }

    @Override
    public void exitTupletelement(TupletelementContext ctx) {
        String tupletType = ctx.tupletspec().getText().substring(1);
        Element elementOne, elementTwo, elementThree, elementFour;
        
        switch (tupletType)
        {
            case "2":
                elementTwo = stack.pop();
                elementOne = stack.pop();
                
                elementOne = tupletDurationAdjuster(tupletType, elementOne);   // adjusts duration of element to given tuplet time
                elementTwo = tupletDurationAdjuster(tupletType, elementTwo);   // adjusts duration of element to given tuplet time
                Element duplet = new Tuplet(2, elementOne, elementTwo);
                stack.push(duplet);
                break;
            case "3":                
                elementThree = stack.pop();
                elementTwo = stack.pop();
                elementOne = stack.pop();
                
                elementOne = tupletDurationAdjuster(tupletType, elementOne);   // adjusts duration of element to given tuplet time
                elementTwo = tupletDurationAdjuster(tupletType, elementTwo);   // ajusts duration of element to given tuplet time
                elementThree = tupletDurationAdjuster(tupletType, elementThree); // adjusts duration of element to given tuplet time
                Element leftTriplet = new Tuplet(2, elementOne, elementTwo);
                Element triplet = new Tuplet(2, leftTriplet, elementThree);
                stack.push(triplet);
                break;
            case "4":
                elementFour = stack.pop();
                elementThree = stack.pop();
                elementTwo = stack.pop();
                elementOne = stack.pop();
                
                elementOne = tupletDurationAdjuster(tupletType, elementOne);
                elementTwo = tupletDurationAdjuster(tupletType, elementTwo);
                elementThree = tupletDurationAdjuster(tupletType, elementThree);
                elementFour = tupletDurationAdjuster(tupletType, elementFour);
                Element innerLeftQuadruplet = new Tuplet(2, elementOne, elementTwo);
                Element leftQuadruplet = new Tuplet(2, innerLeftQuadruplet, elementThree);
                Element quadruplet = new Tuplet(2, leftQuadruplet, elementFour);
                stack.push(quadruplet);
                break;
        }
    }

    @Override
    public void exitTupletspec(TupletspecContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information 
    }

    @Override
    public void exitMultinote(MultinoteContext ctx) {
     // matched the '[' note+ ']' rule
        List<AbcParser.NoteContext> factors = ctx.note();
        assert stack.size() >= factors.size();
        
        // the pattern above always has at least 1 child;
        // pop the last child
        assert factors.size() > 0;
        Element chord = stack.pop();
        
        // pop the older children, one by one, and add them on
        for (int i = 1; i < factors.size(); ++i) {
            chord = new Chord(stack.pop(), chord);
        }
        stack.push(chord);
    }

    

    @Override
    public void exitBarline(BarlineContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }
    
    @Override
    public void exitNewmajorsection(NewmajorsectionContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }
    
    @Override
    public void exitRepeatstart(RepeatstartContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }

    @Override
    public void exitRepeatend(RepeatendContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }

    @Override
    public void exitNthrepeat(NthrepeatContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
        
    }

    @Override
    public void exitText(TextContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }
    
    @Override
    public void exitEndofline(EndoflineContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }
    
    @Override
    public void exitComment(CommentContext ctx) {
        // we don't have to do anything
        // no part of the class needs this information
    }
    
    /**
     * Adjusts the duration of a note or chord to coincide with the timing of a given tuplet
     * @param tupletType the type of tuplet being used
     * @param element the Note or Chord object whose duration you wish to adjust
     * @return Note or Chord with adjusted duration time
     */
    private Element tupletDurationAdjuster(String tupletType, Element element) {
        
        if (element instanceof Note) {
            Pitch pitch;
            int duration;
            if (tupletType.equals("2")) {
                pitch = ((Note) element).pitch();
                duration = (int) Math.round(element.duration() * 1.5); // duration of a duplet is adjusted by 3/2
            } else if (tupletType.equals("3")) {
                pitch = ((Note) element).pitch();
                duration = (int) Math.round(element.duration() * 0.666667); // duration of a triplet is adjusted by 2/3
            } else if (tupletType.equals("4")) {
                pitch = ((Note) element).pitch();
                duration = (int) Math.round(element.duration() * 0.75); // duration of a quadruplet is adjusted by 3/4
            } else {
                // should not get here
                throw new IllegalArgumentException();
            }
            
            return new Note(duration, pitch);
        // only enters else if the element is an instance of Chord class
        } else {
            Element topChord = ((Chord) element).top();
            Element bottomChord = ((Chord) element).bottom();
            return new Chord(tupletDurationAdjuster(tupletType, topChord), tupletDurationAdjuster(tupletType, bottomChord));
        }
    }
    
    @Override public void visitTerminal(TerminalNode terminal) { }
    @Override public void enterEveryRule(ParserRuleContext context) { }
    @Override public void exitEveryRule(ParserRuleContext context) { }
    @Override public void visitErrorNode(ErrorNode node) { }

    @Override
    public void enterAbctune(AbctuneContext ctx) {}
    @Override
    public void enterAbcheader(AbcheaderContext ctx) {}
    @Override
    public void enterFieldnumber(FieldnumberContext ctx) {}
    @Override
    public void enterFieldtitle(FieldtitleContext ctx) {}
    @Override
    public void enterOtherfields(OtherfieldsContext ctx) {}
    @Override
    public void enterFieldcomposer(FieldcomposerContext ctx) {}
    @Override
    public void enterFielddefaultlength(FielddefaultlengthContext ctx) {}
    @Override
    public void enterFieldmeter(FieldmeterContext ctx) {}
    @Override
    public void enterFieldtempo(FieldtempoContext ctx) {}
    @Override
    public void enterFieldvoice(FieldvoiceContext ctx) {}
    @Override
    public void enterFieldkey(FieldkeyContext ctx) {}
    @Override
    public void enterKey(KeyContext ctx) {}
    @Override
    public void enterKeynote(KeynoteContext ctx) {}
    @Override
    public void enterKeyaccidental(KeyaccidentalContext ctx) {}
    @Override
    public void enterModeminor(ModeminorContext ctx) {}
    @Override
    public void enterMeter(MeterContext ctx) {}
    @Override
    public void enterMeterfraction(MeterfractionContext ctx) {}
    @Override
    public void enterTempo(TempoContext ctx) {}
    @Override
    public void enterAbcmusic(AbcmusicContext ctx) {}
    @Override
    public void enterBar(BarContext ctx) {}
    @Override
    public void enterElement(ElementContext ctx) {}
    @Override
    public void enterNoteelement(NoteelementContext ctx) {}
    @Override
    public void enterNote(NoteContext ctx) {}
    @Override
    public void enterNoteorrest(NoteorrestContext ctx) {}
    @Override
    public void enterPitch(PitchContext ctx) {}
    @Override
    public void enterAccidental(AccidentalContext ctx) {}
    @Override
    public void enterBasenote(BasenoteContext ctx) {}
    @Override
    public void enterOctave(OctaveContext ctx) {}
    @Override
    public void enterNotelength(NotelengthContext ctx) {}
    @Override
    public void enterNotelengthstrict(NotelengthstrictContext ctx) {}
    @Override
    public void enterRest(RestContext ctx) {}
    @Override
    public void enterTupletspec(TupletspecContext ctx) {}
    @Override
    public void enterMultinote(MultinoteContext ctx) {}
    @Override
    public void enterTupletelement(TupletelementContext ctx) {}
    @Override
    public void enterBarline(BarlineContext ctx) {}
    @Override
    public void enterNewmajorsection(NewmajorsectionContext ctx) {}
    @Override
    public void enterRepeatstart(RepeatstartContext ctx) {}
    @Override
    public void enterRepeatend(RepeatendContext ctx) {}
    @Override
    public void enterNthrepeat(NthrepeatContext ctx) {}
    @Override
    public void enterText(TextContext ctx) {}
    @Override
    public void enterEndofline(EndoflineContext ctx) {}
    @Override
    public void enterComment(CommentContext ctx) {}
}
