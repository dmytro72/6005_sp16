// Generated from Abc.g4 by ANTLR 4.5.1

package abc.parser;
// Do not edit this .java file! Edit the .g4 file and re-run Antlr.

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link AbcParser}.
 */
public interface AbcListener extends ParseTreeListener {
  /**
   * Enter a parse tree produced by {@link AbcParser#abctune}.
   * @param ctx the parse tree
   */
  void enterAbctune(AbcParser.AbctuneContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#abctune}.
   * @param ctx the parse tree
   */
  void exitAbctune(AbcParser.AbctuneContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#abcheader}.
   * @param ctx the parse tree
   */
  void enterAbcheader(AbcParser.AbcheaderContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#abcheader}.
   * @param ctx the parse tree
   */
  void exitAbcheader(AbcParser.AbcheaderContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#fieldnumber}.
   * @param ctx the parse tree
   */
  void enterFieldnumber(AbcParser.FieldnumberContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#fieldnumber}.
   * @param ctx the parse tree
   */
  void exitFieldnumber(AbcParser.FieldnumberContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#fieldtitle}.
   * @param ctx the parse tree
   */
  void enterFieldtitle(AbcParser.FieldtitleContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#fieldtitle}.
   * @param ctx the parse tree
   */
  void exitFieldtitle(AbcParser.FieldtitleContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#otherfields}.
   * @param ctx the parse tree
   */
  void enterOtherfields(AbcParser.OtherfieldsContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#otherfields}.
   * @param ctx the parse tree
   */
  void exitOtherfields(AbcParser.OtherfieldsContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#fieldcomposer}.
   * @param ctx the parse tree
   */
  void enterFieldcomposer(AbcParser.FieldcomposerContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#fieldcomposer}.
   * @param ctx the parse tree
   */
  void exitFieldcomposer(AbcParser.FieldcomposerContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#fielddefaultlength}.
   * @param ctx the parse tree
   */
  void enterFielddefaultlength(AbcParser.FielddefaultlengthContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#fielddefaultlength}.
   * @param ctx the parse tree
   */
  void exitFielddefaultlength(AbcParser.FielddefaultlengthContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#fieldmeter}.
   * @param ctx the parse tree
   */
  void enterFieldmeter(AbcParser.FieldmeterContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#fieldmeter}.
   * @param ctx the parse tree
   */
  void exitFieldmeter(AbcParser.FieldmeterContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#fieldtempo}.
   * @param ctx the parse tree
   */
  void enterFieldtempo(AbcParser.FieldtempoContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#fieldtempo}.
   * @param ctx the parse tree
   */
  void exitFieldtempo(AbcParser.FieldtempoContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#fieldvoice}.
   * @param ctx the parse tree
   */
  void enterFieldvoice(AbcParser.FieldvoiceContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#fieldvoice}.
   * @param ctx the parse tree
   */
  void exitFieldvoice(AbcParser.FieldvoiceContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#fieldkey}.
   * @param ctx the parse tree
   */
  void enterFieldkey(AbcParser.FieldkeyContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#fieldkey}.
   * @param ctx the parse tree
   */
  void exitFieldkey(AbcParser.FieldkeyContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#key}.
   * @param ctx the parse tree
   */
  void enterKey(AbcParser.KeyContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#key}.
   * @param ctx the parse tree
   */
  void exitKey(AbcParser.KeyContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#keynote}.
   * @param ctx the parse tree
   */
  void enterKeynote(AbcParser.KeynoteContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#keynote}.
   * @param ctx the parse tree
   */
  void exitKeynote(AbcParser.KeynoteContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#keyaccidental}.
   * @param ctx the parse tree
   */
  void enterKeyaccidental(AbcParser.KeyaccidentalContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#keyaccidental}.
   * @param ctx the parse tree
   */
  void exitKeyaccidental(AbcParser.KeyaccidentalContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#modeminor}.
   * @param ctx the parse tree
   */
  void enterModeminor(AbcParser.ModeminorContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#modeminor}.
   * @param ctx the parse tree
   */
  void exitModeminor(AbcParser.ModeminorContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#meter}.
   * @param ctx the parse tree
   */
  void enterMeter(AbcParser.MeterContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#meter}.
   * @param ctx the parse tree
   */
  void exitMeter(AbcParser.MeterContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#meterfraction}.
   * @param ctx the parse tree
   */
  void enterMeterfraction(AbcParser.MeterfractionContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#meterfraction}.
   * @param ctx the parse tree
   */
  void exitMeterfraction(AbcParser.MeterfractionContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#tempo}.
   * @param ctx the parse tree
   */
  void enterTempo(AbcParser.TempoContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#tempo}.
   * @param ctx the parse tree
   */
  void exitTempo(AbcParser.TempoContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#abcmusic}.
   * @param ctx the parse tree
   */
  void enterAbcmusic(AbcParser.AbcmusicContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#abcmusic}.
   * @param ctx the parse tree
   */
  void exitAbcmusic(AbcParser.AbcmusicContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#voice}.
   * @param ctx the parse tree
   */
  void enterVoice(AbcParser.VoiceContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#voice}.
   * @param ctx the parse tree
   */
  void exitVoice(AbcParser.VoiceContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#bar}.
   * @param ctx the parse tree
   */
  void enterBar(AbcParser.BarContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#bar}.
   * @param ctx the parse tree
   */
  void exitBar(AbcParser.BarContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#element}.
   * @param ctx the parse tree
   */
  void enterElement(AbcParser.ElementContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#element}.
   * @param ctx the parse tree
   */
  void exitElement(AbcParser.ElementContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#noteelement}.
   * @param ctx the parse tree
   */
  void enterNoteelement(AbcParser.NoteelementContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#noteelement}.
   * @param ctx the parse tree
   */
  void exitNoteelement(AbcParser.NoteelementContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#note}.
   * @param ctx the parse tree
   */
  void enterNote(AbcParser.NoteContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#note}.
   * @param ctx the parse tree
   */
  void exitNote(AbcParser.NoteContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#noteorrest}.
   * @param ctx the parse tree
   */
  void enterNoteorrest(AbcParser.NoteorrestContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#noteorrest}.
   * @param ctx the parse tree
   */
  void exitNoteorrest(AbcParser.NoteorrestContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#pitch}.
   * @param ctx the parse tree
   */
  void enterPitch(AbcParser.PitchContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#pitch}.
   * @param ctx the parse tree
   */
  void exitPitch(AbcParser.PitchContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#accidental}.
   * @param ctx the parse tree
   */
  void enterAccidental(AbcParser.AccidentalContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#accidental}.
   * @param ctx the parse tree
   */
  void exitAccidental(AbcParser.AccidentalContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#basenote}.
   * @param ctx the parse tree
   */
  void enterBasenote(AbcParser.BasenoteContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#basenote}.
   * @param ctx the parse tree
   */
  void exitBasenote(AbcParser.BasenoteContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#octave}.
   * @param ctx the parse tree
   */
  void enterOctave(AbcParser.OctaveContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#octave}.
   * @param ctx the parse tree
   */
  void exitOctave(AbcParser.OctaveContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#rest}.
   * @param ctx the parse tree
   */
  void enterRest(AbcParser.RestContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#rest}.
   * @param ctx the parse tree
   */
  void exitRest(AbcParser.RestContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#notelength}.
   * @param ctx the parse tree
   */
  void enterNotelength(AbcParser.NotelengthContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#notelength}.
   * @param ctx the parse tree
   */
  void exitNotelength(AbcParser.NotelengthContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#notelengthstrict}.
   * @param ctx the parse tree
   */
  void enterNotelengthstrict(AbcParser.NotelengthstrictContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#notelengthstrict}.
   * @param ctx the parse tree
   */
  void exitNotelengthstrict(AbcParser.NotelengthstrictContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#multinote}.
   * @param ctx the parse tree
   */
  void enterMultinote(AbcParser.MultinoteContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#multinote}.
   * @param ctx the parse tree
   */
  void exitMultinote(AbcParser.MultinoteContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#tupletelement}.
   * @param ctx the parse tree
   */
  void enterTupletelement(AbcParser.TupletelementContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#tupletelement}.
   * @param ctx the parse tree
   */
  void exitTupletelement(AbcParser.TupletelementContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#tupletspec}.
   * @param ctx the parse tree
   */
  void enterTupletspec(AbcParser.TupletspecContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#tupletspec}.
   * @param ctx the parse tree
   */
  void exitTupletspec(AbcParser.TupletspecContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#barline}.
   * @param ctx the parse tree
   */
  void enterBarline(AbcParser.BarlineContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#barline}.
   * @param ctx the parse tree
   */
  void exitBarline(AbcParser.BarlineContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#newmajorsection}.
   * @param ctx the parse tree
   */
  void enterNewmajorsection(AbcParser.NewmajorsectionContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#newmajorsection}.
   * @param ctx the parse tree
   */
  void exitNewmajorsection(AbcParser.NewmajorsectionContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#repeatstart}.
   * @param ctx the parse tree
   */
  void enterRepeatstart(AbcParser.RepeatstartContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#repeatstart}.
   * @param ctx the parse tree
   */
  void exitRepeatstart(AbcParser.RepeatstartContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#repeatend}.
   * @param ctx the parse tree
   */
  void enterRepeatend(AbcParser.RepeatendContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#repeatend}.
   * @param ctx the parse tree
   */
  void exitRepeatend(AbcParser.RepeatendContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#nthrepeat}.
   * @param ctx the parse tree
   */
  void enterNthrepeat(AbcParser.NthrepeatContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#nthrepeat}.
   * @param ctx the parse tree
   */
  void exitNthrepeat(AbcParser.NthrepeatContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#endofline}.
   * @param ctx the parse tree
   */
  void enterEndofline(AbcParser.EndoflineContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#endofline}.
   * @param ctx the parse tree
   */
  void exitEndofline(AbcParser.EndoflineContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#comment}.
   * @param ctx the parse tree
   */
  void enterComment(AbcParser.CommentContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#comment}.
   * @param ctx the parse tree
   */
  void exitComment(AbcParser.CommentContext ctx);
  /**
   * Enter a parse tree produced by {@link AbcParser#text}.
   * @param ctx the parse tree
   */
  void enterText(AbcParser.TextContext ctx);
  /**
   * Exit a parse tree produced by {@link AbcParser#text}.
   * @param ctx the parse tree
   */
  void exitText(AbcParser.TextContext ctx);
}