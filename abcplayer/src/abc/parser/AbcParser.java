// Generated from Abc.g4 by ANTLR 4.5.1

package abc.parser;
// Do not edit this .java file! Edit the .g4 file and re-run Antlr.

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AbcParser extends Parser {
  static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

  protected static final DFA[] _decisionToDFA;
  protected static final PredictionContextCache _sharedContextCache =
    new PredictionContextCache();
  public static final int
    T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
    T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, 
    T__16=17, T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, 
    T__23=24, T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, 
    T__30=31, T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, 
    T__37=38, T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, 
    T__44=45, DIGIT=46, LETTER=47, PUNCTUATION=48, WHITESPACE=49, NEWLINE=50, 
    SPACES=51;
  public static final int
    RULE_abctune = 0, RULE_abcheader = 1, RULE_fieldnumber = 2, RULE_fieldtitle = 3, 
    RULE_otherfields = 4, RULE_fieldcomposer = 5, RULE_fielddefaultlength = 6, 
    RULE_fieldmeter = 7, RULE_fieldtempo = 8, RULE_fieldvoice = 9, RULE_fieldkey = 10, 
    RULE_key = 11, RULE_keynote = 12, RULE_keyaccidental = 13, RULE_modeminor = 14, 
    RULE_meter = 15, RULE_meterfraction = 16, RULE_tempo = 17, RULE_abcmusic = 18, 
    RULE_voice = 19, RULE_bar = 20, RULE_element = 21, RULE_noteelement = 22, 
    RULE_note = 23, RULE_noteorrest = 24, RULE_pitch = 25, RULE_accidental = 26, 
    RULE_basenote = 27, RULE_octave = 28, RULE_rest = 29, RULE_notelength = 30, 
    RULE_notelengthstrict = 31, RULE_multinote = 32, RULE_tupletelement = 33, 
    RULE_tupletspec = 34, RULE_barline = 35, RULE_newmajorsection = 36, 
    RULE_repeatstart = 37, RULE_repeatend = 38, RULE_nthrepeat = 39, RULE_endofline = 40, 
    RULE_comment = 41, RULE_text = 42;
  public static final String[] ruleNames = {
    "abctune", "abcheader", "fieldnumber", "fieldtitle", "otherfields", 
    "fieldcomposer", "fielddefaultlength", "fieldmeter", "fieldtempo", "fieldvoice", 
    "fieldkey", "key", "keynote", "keyaccidental", "modeminor", "meter", 
    "meterfraction", "tempo", "abcmusic", "voice", "bar", "element", "noteelement", 
    "note", "noteorrest", "pitch", "accidental", "basenote", "octave", "rest", 
    "notelength", "notelengthstrict", "multinote", "tupletelement", "tupletspec", 
    "barline", "newmajorsection", "repeatstart", "repeatend", "nthrepeat", 
    "endofline", "comment", "text"
  };

  private static final String[] _LITERAL_NAMES = {
    null, "'X:'", "'T:'", "'C:'", "'L:'", "'M:'", "'Q:'", "'V:'", "'K:'", 
    "'#'", "'b'", "'m'", "'C'", "'C|'", "'/'", "'='", "'^'", "'^^'", "'_'", 
    "'__'", "'D'", "'E'", "'F'", "'G'", "'A'", "'B'", "'c'", "'d'", "'e'", 
    "'f'", "'g'", "'a'", "'''", "','", "'z'", "'['", "']'", "'('", "'|'", 
    "'[|'", "'|]'", "':'", "'[1'", "'[2'", "'%'", "'||'"
  };
  private static final String[] _SYMBOLIC_NAMES = {
    null, null, null, null, null, null, null, null, null, null, null, null, 
    null, null, null, null, null, null, null, null, null, null, null, null, 
    null, null, null, null, null, null, null, null, null, null, null, null, 
    null, null, null, null, null, null, null, null, null, null, "DIGIT", 
    "LETTER", "PUNCTUATION", "WHITESPACE", "NEWLINE", "SPACES"
  };
  public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

  /**
   * @deprecated Use {@link #VOCABULARY} instead.
   */
  @Deprecated
  public static final String[] tokenNames;
  static {
    tokenNames = new String[_SYMBOLIC_NAMES.length];
    for (int i = 0; i < tokenNames.length; i++) {
      tokenNames[i] = VOCABULARY.getLiteralName(i);
      if (tokenNames[i] == null) {
        tokenNames[i] = VOCABULARY.getSymbolicName(i);
      }

      if (tokenNames[i] == null) {
        tokenNames[i] = "<INVALID>";
      }
    }
  }

  @Override
  @Deprecated
  public String[] getTokenNames() {
    return tokenNames;
  }

  @Override

  public Vocabulary getVocabulary() {
    return VOCABULARY;
  }

  @Override
  public String getGrammarFileName() { return "Abc.g4"; }

  @Override
  public String[] getRuleNames() { return ruleNames; }

  @Override
  public String getSerializedATN() { return _serializedATN; }

  @Override
  public ATN getATN() { return _ATN; }


      // This method makes the parser stop running if it encounters
      // invalid input and throw a RuntimeException.
      public void reportErrorsAsExceptions() {
          // To prevent any reports to standard error, add this line:
          //removeErrorListeners();
          
          addErrorListener(new BaseErrorListener() {
              public void syntaxError(Recognizer<?, ?> recognizer,
                                      Object offendingSymbol, 
                                      int line, int charPositionInLine,
                                      String msg, RecognitionException e) {
                  throw new ParseCancellationException(msg, e);
              }
          });
      }

  public AbcParser(TokenStream input) {
    super(input);
    _interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
  }
  public static class AbctuneContext extends ParserRuleContext {
    public AbcheaderContext abcheader() {
      return getRuleContext(AbcheaderContext.class,0);
    }
    public AbcmusicContext abcmusic() {
      return getRuleContext(AbcmusicContext.class,0);
    }
    public TerminalNode EOF() { return getToken(AbcParser.EOF, 0); }
    public AbctuneContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_abctune; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterAbctune(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitAbctune(this);
    }
  }

  public final AbctuneContext abctune() throws RecognitionException {
    AbctuneContext _localctx = new AbctuneContext(_ctx, getState());
    enterRule(_localctx, 0, RULE_abctune);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(86);
      abcheader();
      setState(87);
      abcmusic();
      setState(88);
      match(EOF);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class AbcheaderContext extends ParserRuleContext {
    public FieldnumberContext fieldnumber() {
      return getRuleContext(FieldnumberContext.class,0);
    }
    public FieldtitleContext fieldtitle() {
      return getRuleContext(FieldtitleContext.class,0);
    }
    public FieldkeyContext fieldkey() {
      return getRuleContext(FieldkeyContext.class,0);
    }
    public List<CommentContext> comment() {
      return getRuleContexts(CommentContext.class);
    }
    public CommentContext comment(int i) {
      return getRuleContext(CommentContext.class,i);
    }
    public List<OtherfieldsContext> otherfields() {
      return getRuleContexts(OtherfieldsContext.class);
    }
    public OtherfieldsContext otherfields(int i) {
      return getRuleContext(OtherfieldsContext.class,i);
    }
    public AbcheaderContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_abcheader; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterAbcheader(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitAbcheader(this);
    }
  }

  public final AbcheaderContext abcheader() throws RecognitionException {
    AbcheaderContext _localctx = new AbcheaderContext(_ctx, getState());
    enterRule(_localctx, 2, RULE_abcheader);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(90);
      fieldnumber();
      setState(94);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while (_la==T__43) {
        {
        {
        setState(91);
        comment();
        }
        }
        setState(96);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(97);
      fieldtitle();
      setState(101);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6))) != 0)) {
        {
        {
        setState(98);
        otherfields();
        }
        }
        setState(103);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(104);
      fieldkey();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class FieldnumberContext extends ParserRuleContext {
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public List<TerminalNode> DIGIT() { return getTokens(AbcParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(AbcParser.DIGIT, i);
    }
    public FieldnumberContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fieldnumber; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterFieldnumber(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitFieldnumber(this);
    }
  }

  public final FieldnumberContext fieldnumber() throws RecognitionException {
    FieldnumberContext _localctx = new FieldnumberContext(_ctx, getState());
    enterRule(_localctx, 4, RULE_fieldnumber);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(106);
      match(T__0);
      setState(108);
      _la = _input.LA(1);
      if (_la==WHITESPACE) {
        {
        setState(107);
        match(WHITESPACE);
        }
      }

      setState(111); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(110);
        match(DIGIT);
        }
        }
        setState(113); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      setState(115);
      endofline();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class FieldtitleContext extends ParserRuleContext {
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public List<TextContext> text() {
      return getRuleContexts(TextContext.class);
    }
    public TextContext text(int i) {
      return getRuleContext(TextContext.class,i);
    }
    public FieldtitleContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fieldtitle; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterFieldtitle(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitFieldtitle(this);
    }
  }

  public final FieldtitleContext fieldtitle() throws RecognitionException {
    FieldtitleContext _localctx = new FieldtitleContext(_ctx, getState());
    enterRule(_localctx, 6, RULE_fieldtitle);
    try {
      int _alt;
      enterOuterAlt(_localctx, 1);
      {
      setState(117);
      match(T__1);
      setState(119);
      switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
      case 1:
        {
        setState(118);
        match(WHITESPACE);
        }
        break;
      }
      setState(124);
      _errHandler.sync(this);
      _alt = getInterpreter().adaptivePredict(_input,5,_ctx);
      while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
        if ( _alt==1 ) {
          {
          {
          setState(121);
          text();
          }
          } 
        }
        setState(126);
        _errHandler.sync(this);
        _alt = getInterpreter().adaptivePredict(_input,5,_ctx);
      }
      setState(127);
      endofline();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class OtherfieldsContext extends ParserRuleContext {
    public FieldcomposerContext fieldcomposer() {
      return getRuleContext(FieldcomposerContext.class,0);
    }
    public FielddefaultlengthContext fielddefaultlength() {
      return getRuleContext(FielddefaultlengthContext.class,0);
    }
    public FieldmeterContext fieldmeter() {
      return getRuleContext(FieldmeterContext.class,0);
    }
    public FieldtempoContext fieldtempo() {
      return getRuleContext(FieldtempoContext.class,0);
    }
    public FieldvoiceContext fieldvoice() {
      return getRuleContext(FieldvoiceContext.class,0);
    }
    public OtherfieldsContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_otherfields; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterOtherfields(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitOtherfields(this);
    }
  }

  public final OtherfieldsContext otherfields() throws RecognitionException {
    OtherfieldsContext _localctx = new OtherfieldsContext(_ctx, getState());
    enterRule(_localctx, 8, RULE_otherfields);
    try {
      setState(134);
      switch (_input.LA(1)) {
      case T__2:
        enterOuterAlt(_localctx, 1);
        {
        setState(129);
        fieldcomposer();
        }
        break;
      case T__3:
        enterOuterAlt(_localctx, 2);
        {
        setState(130);
        fielddefaultlength();
        }
        break;
      case T__4:
        enterOuterAlt(_localctx, 3);
        {
        setState(131);
        fieldmeter();
        }
        break;
      case T__5:
        enterOuterAlt(_localctx, 4);
        {
        setState(132);
        fieldtempo();
        }
        break;
      case T__6:
        enterOuterAlt(_localctx, 5);
        {
        setState(133);
        fieldvoice();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class FieldcomposerContext extends ParserRuleContext {
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public List<TextContext> text() {
      return getRuleContexts(TextContext.class);
    }
    public TextContext text(int i) {
      return getRuleContext(TextContext.class,i);
    }
    public FieldcomposerContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fieldcomposer; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterFieldcomposer(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitFieldcomposer(this);
    }
  }

  public final FieldcomposerContext fieldcomposer() throws RecognitionException {
    FieldcomposerContext _localctx = new FieldcomposerContext(_ctx, getState());
    enterRule(_localctx, 10, RULE_fieldcomposer);
    try {
      int _alt;
      enterOuterAlt(_localctx, 1);
      {
      setState(136);
      match(T__2);
      setState(138);
      switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
      case 1:
        {
        setState(137);
        match(WHITESPACE);
        }
        break;
      }
      setState(143);
      _errHandler.sync(this);
      _alt = getInterpreter().adaptivePredict(_input,8,_ctx);
      while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
        if ( _alt==1 ) {
          {
          {
          setState(140);
          text();
          }
          } 
        }
        setState(145);
        _errHandler.sync(this);
        _alt = getInterpreter().adaptivePredict(_input,8,_ctx);
      }
      setState(146);
      endofline();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class FielddefaultlengthContext extends ParserRuleContext {
    public NotelengthstrictContext notelengthstrict() {
      return getRuleContext(NotelengthstrictContext.class,0);
    }
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public FielddefaultlengthContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fielddefaultlength; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterFielddefaultlength(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitFielddefaultlength(this);
    }
  }

  public final FielddefaultlengthContext fielddefaultlength() throws RecognitionException {
    FielddefaultlengthContext _localctx = new FielddefaultlengthContext(_ctx, getState());
    enterRule(_localctx, 12, RULE_fielddefaultlength);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(148);
      match(T__3);
      setState(150);
      _la = _input.LA(1);
      if (_la==WHITESPACE) {
        {
        setState(149);
        match(WHITESPACE);
        }
      }

      setState(152);
      notelengthstrict();
      setState(153);
      endofline();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class FieldmeterContext extends ParserRuleContext {
    public MeterContext meter() {
      return getRuleContext(MeterContext.class,0);
    }
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public FieldmeterContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fieldmeter; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterFieldmeter(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitFieldmeter(this);
    }
  }

  public final FieldmeterContext fieldmeter() throws RecognitionException {
    FieldmeterContext _localctx = new FieldmeterContext(_ctx, getState());
    enterRule(_localctx, 14, RULE_fieldmeter);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(155);
      match(T__4);
      setState(157);
      _la = _input.LA(1);
      if (_la==WHITESPACE) {
        {
        setState(156);
        match(WHITESPACE);
        }
      }

      setState(159);
      meter();
      setState(160);
      endofline();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class FieldtempoContext extends ParserRuleContext {
    public TempoContext tempo() {
      return getRuleContext(TempoContext.class,0);
    }
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public FieldtempoContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fieldtempo; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterFieldtempo(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitFieldtempo(this);
    }
  }

  public final FieldtempoContext fieldtempo() throws RecognitionException {
    FieldtempoContext _localctx = new FieldtempoContext(_ctx, getState());
    enterRule(_localctx, 16, RULE_fieldtempo);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(162);
      match(T__5);
      setState(164);
      _la = _input.LA(1);
      if (_la==WHITESPACE) {
        {
        setState(163);
        match(WHITESPACE);
        }
      }

      setState(166);
      tempo();
      setState(167);
      endofline();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class FieldvoiceContext extends ParserRuleContext {
    public TextContext text() {
      return getRuleContext(TextContext.class,0);
    }
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public FieldvoiceContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fieldvoice; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterFieldvoice(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitFieldvoice(this);
    }
  }

  public final FieldvoiceContext fieldvoice() throws RecognitionException {
    FieldvoiceContext _localctx = new FieldvoiceContext(_ctx, getState());
    enterRule(_localctx, 18, RULE_fieldvoice);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(169);
      match(T__6);
      setState(171);
      switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
      case 1:
        {
        setState(170);
        match(WHITESPACE);
        }
        break;
      }
      setState(173);
      text();
      setState(174);
      endofline();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class FieldkeyContext extends ParserRuleContext {
    public KeyContext key() {
      return getRuleContext(KeyContext.class,0);
    }
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public FieldkeyContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_fieldkey; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterFieldkey(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitFieldkey(this);
    }
  }

  public final FieldkeyContext fieldkey() throws RecognitionException {
    FieldkeyContext _localctx = new FieldkeyContext(_ctx, getState());
    enterRule(_localctx, 20, RULE_fieldkey);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(176);
      match(T__7);
      setState(178);
      _la = _input.LA(1);
      if (_la==WHITESPACE) {
        {
        setState(177);
        match(WHITESPACE);
        }
      }

      setState(180);
      key();
      setState(181);
      endofline();
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class KeyContext extends ParserRuleContext {
    public KeynoteContext keynote() {
      return getRuleContext(KeynoteContext.class,0);
    }
    public ModeminorContext modeminor() {
      return getRuleContext(ModeminorContext.class,0);
    }
    public KeyContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_key; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterKey(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitKey(this);
    }
  }

  public final KeyContext key() throws RecognitionException {
    KeyContext _localctx = new KeyContext(_ctx, getState());
    enterRule(_localctx, 22, RULE_key);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(183);
      keynote();
      setState(185);
      _la = _input.LA(1);
      if (_la==T__10) {
        {
        setState(184);
        modeminor();
        }
      }

      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class KeynoteContext extends ParserRuleContext {
    public BasenoteContext basenote() {
      return getRuleContext(BasenoteContext.class,0);
    }
    public KeyaccidentalContext keyaccidental() {
      return getRuleContext(KeyaccidentalContext.class,0);
    }
    public KeynoteContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_keynote; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterKeynote(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitKeynote(this);
    }
  }

  public final KeynoteContext keynote() throws RecognitionException {
    KeynoteContext _localctx = new KeynoteContext(_ctx, getState());
    enterRule(_localctx, 24, RULE_keynote);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(187);
      basenote();
      setState(189);
      _la = _input.LA(1);
      if (_la==T__8 || _la==T__9) {
        {
        setState(188);
        keyaccidental();
        }
      }

      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class KeyaccidentalContext extends ParserRuleContext {
    public KeyaccidentalContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_keyaccidental; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterKeyaccidental(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitKeyaccidental(this);
    }
  }

  public final KeyaccidentalContext keyaccidental() throws RecognitionException {
    KeyaccidentalContext _localctx = new KeyaccidentalContext(_ctx, getState());
    enterRule(_localctx, 26, RULE_keyaccidental);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(191);
      _la = _input.LA(1);
      if ( !(_la==T__8 || _la==T__9) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class ModeminorContext extends ParserRuleContext {
    public ModeminorContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_modeminor; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterModeminor(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitModeminor(this);
    }
  }

  public final ModeminorContext modeminor() throws RecognitionException {
    ModeminorContext _localctx = new ModeminorContext(_ctx, getState());
    enterRule(_localctx, 28, RULE_modeminor);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(193);
      match(T__10);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class MeterContext extends ParserRuleContext {
    public MeterfractionContext meterfraction() {
      return getRuleContext(MeterfractionContext.class,0);
    }
    public MeterContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_meter; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterMeter(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitMeter(this);
    }
  }

  public final MeterContext meter() throws RecognitionException {
    MeterContext _localctx = new MeterContext(_ctx, getState());
    enterRule(_localctx, 30, RULE_meter);
    try {
      setState(198);
      switch (_input.LA(1)) {
      case T__11:
        enterOuterAlt(_localctx, 1);
        {
        setState(195);
        match(T__11);
        }
        break;
      case T__12:
        enterOuterAlt(_localctx, 2);
        {
        setState(196);
        match(T__12);
        }
        break;
      case DIGIT:
        enterOuterAlt(_localctx, 3);
        {
        setState(197);
        meterfraction();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class MeterfractionContext extends ParserRuleContext {
    public List<TerminalNode> DIGIT() { return getTokens(AbcParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(AbcParser.DIGIT, i);
    }
    public MeterfractionContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_meterfraction; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterMeterfraction(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitMeterfraction(this);
    }
  }

  public final MeterfractionContext meterfraction() throws RecognitionException {
    MeterfractionContext _localctx = new MeterfractionContext(_ctx, getState());
    enterRule(_localctx, 32, RULE_meterfraction);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(201); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(200);
        match(DIGIT);
        }
        }
        setState(203); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      setState(205);
      match(T__13);
      setState(207); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(206);
        match(DIGIT);
        }
        }
        setState(209); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class TempoContext extends ParserRuleContext {
    public MeterfractionContext meterfraction() {
      return getRuleContext(MeterfractionContext.class,0);
    }
    public List<TerminalNode> DIGIT() { return getTokens(AbcParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(AbcParser.DIGIT, i);
    }
    public TempoContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_tempo; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterTempo(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitTempo(this);
    }
  }

  public final TempoContext tempo() throws RecognitionException {
    TempoContext _localctx = new TempoContext(_ctx, getState());
    enterRule(_localctx, 34, RULE_tempo);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(211);
      meterfraction();
      setState(212);
      match(T__14);
      setState(214); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(213);
        match(DIGIT);
        }
        }
        setState(216); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class AbcmusicContext extends ParserRuleContext {
    public List<VoiceContext> voice() {
      return getRuleContexts(VoiceContext.class);
    }
    public VoiceContext voice(int i) {
      return getRuleContext(VoiceContext.class,i);
    }
    public List<CommentContext> comment() {
      return getRuleContexts(CommentContext.class);
    }
    public CommentContext comment(int i) {
      return getRuleContext(CommentContext.class,i);
    }
    public AbcmusicContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_abcmusic; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterAbcmusic(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitAbcmusic(this);
    }
  }

  public final AbcmusicContext abcmusic() throws RecognitionException {
    AbcmusicContext _localctx = new AbcmusicContext(_ctx, getState());
    enterRule(_localctx, 36, RULE_abcmusic);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(220); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        setState(220);
        switch (_input.LA(1)) {
        case T__6:
        case T__9:
        case T__11:
        case T__14:
        case T__15:
        case T__16:
        case T__17:
        case T__18:
        case T__19:
        case T__20:
        case T__21:
        case T__22:
        case T__23:
        case T__24:
        case T__25:
        case T__26:
        case T__27:
        case T__28:
        case T__29:
        case T__30:
        case T__33:
        case T__34:
        case T__35:
        case T__36:
        case T__37:
        case T__40:
        case T__41:
        case T__42:
        case WHITESPACE:
          {
          setState(218);
          voice();
          }
          break;
        case T__43:
          {
          setState(219);
          comment();
          }
          break;
        default:
          throw new NoViableAltException(this);
        }
        }
        setState(222); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__9) | (1L << T__11) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << WHITESPACE))) != 0) );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class VoiceContext extends ParserRuleContext {
    public FieldvoiceContext fieldvoice() {
      return getRuleContext(FieldvoiceContext.class,0);
    }
    public List<BarContext> bar() {
      return getRuleContexts(BarContext.class);
    }
    public BarContext bar(int i) {
      return getRuleContext(BarContext.class,i);
    }
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public VoiceContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_voice; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterVoice(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitVoice(this);
    }
  }

  public final VoiceContext voice() throws RecognitionException {
    VoiceContext _localctx = new VoiceContext(_ctx, getState());
    enterRule(_localctx, 38, RULE_voice);
    try {
      int _alt;
      setState(241);
      switch (_input.LA(1)) {
      case T__6:
        enterOuterAlt(_localctx, 1);
        {
        setState(224);
        fieldvoice();
        setState(226); 
        _errHandler.sync(this);
        _alt = 1;
        do {
          switch (_alt) {
          case 1:
            {
            {
            setState(225);
            bar();
            }
            }
            break;
          default:
            throw new NoViableAltException(this);
          }
          setState(228); 
          _errHandler.sync(this);
          _alt = getInterpreter().adaptivePredict(_input,22,_ctx);
        } while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
        setState(231);
        switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
        case 1:
          {
          setState(230);
          endofline();
          }
          break;
        }
        }
        break;
      case T__9:
      case T__11:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
      case T__19:
      case T__20:
      case T__21:
      case T__22:
      case T__23:
      case T__24:
      case T__25:
      case T__26:
      case T__27:
      case T__28:
      case T__29:
      case T__30:
      case T__33:
      case T__34:
      case T__35:
      case T__36:
      case T__37:
      case T__40:
      case T__41:
      case T__42:
      case WHITESPACE:
        enterOuterAlt(_localctx, 2);
        {
        setState(234); 
        _errHandler.sync(this);
        _alt = 1;
        do {
          switch (_alt) {
          case 1:
            {
            {
            setState(233);
            bar();
            }
            }
            break;
          default:
            throw new NoViableAltException(this);
          }
          setState(236); 
          _errHandler.sync(this);
          _alt = getInterpreter().adaptivePredict(_input,24,_ctx);
        } while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
        setState(239);
        switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
        case 1:
          {
          setState(238);
          endofline();
          }
          break;
        }
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class BarContext extends ParserRuleContext {
    public BarlineContext barline() {
      return getRuleContext(BarlineContext.class,0);
    }
    public EndoflineContext endofline() {
      return getRuleContext(EndoflineContext.class,0);
    }
    public NewmajorsectionContext newmajorsection() {
      return getRuleContext(NewmajorsectionContext.class,0);
    }
    public NthrepeatContext nthrepeat() {
      return getRuleContext(NthrepeatContext.class,0);
    }
    public RepeatstartContext repeatstart() {
      return getRuleContext(RepeatstartContext.class,0);
    }
    public List<ElementContext> element() {
      return getRuleContexts(ElementContext.class);
    }
    public ElementContext element(int i) {
      return getRuleContext(ElementContext.class,i);
    }
    public List<TerminalNode> WHITESPACE() { return getTokens(AbcParser.WHITESPACE); }
    public TerminalNode WHITESPACE(int i) {
      return getToken(AbcParser.WHITESPACE, i);
    }
    public RepeatendContext repeatend() {
      return getRuleContext(RepeatendContext.class,0);
    }
    public BarContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_bar; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterBar(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitBar(this);
    }
  }

  public final BarContext bar() throws RecognitionException {
    BarContext _localctx = new BarContext(_ctx, getState());
    enterRule(_localctx, 40, RULE_bar);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(244);
      _la = _input.LA(1);
      if (_la==T__35 || _la==T__37) {
        {
        setState(243);
        newmajorsection();
        }
      }

      setState(247);
      _la = _input.LA(1);
      if (_la==T__41 || _la==T__42) {
        {
        setState(246);
        nthrepeat();
        }
      }

      setState(250);
      _la = _input.LA(1);
      if (_la==T__40) {
        {
        setState(249);
        repeatstart();
        }
      }

      setState(254); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        setState(254);
        switch (_input.LA(1)) {
        case T__9:
        case T__11:
        case T__14:
        case T__15:
        case T__16:
        case T__17:
        case T__18:
        case T__19:
        case T__20:
        case T__21:
        case T__22:
        case T__23:
        case T__24:
        case T__25:
        case T__26:
        case T__27:
        case T__28:
        case T__29:
        case T__30:
        case T__33:
        case T__34:
        case T__36:
          {
          setState(252);
          element();
          }
          break;
        case WHITESPACE:
          {
          setState(253);
          match(WHITESPACE);
          }
          break;
        default:
          throw new NoViableAltException(this);
        }
        }
        setState(256); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__11) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__33) | (1L << T__34) | (1L << T__36) | (1L << WHITESPACE))) != 0) );
      setState(259);
      _la = _input.LA(1);
      if (_la==T__40) {
        {
        setState(258);
        repeatend();
        }
      }

      setState(263);
      switch (_input.LA(1)) {
      case T__37:
      case T__38:
      case T__39:
        {
        setState(261);
        barline();
        }
        break;
      case T__43:
      case NEWLINE:
        {
        setState(262);
        endofline();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class ElementContext extends ParserRuleContext {
    public NoteelementContext noteelement() {
      return getRuleContext(NoteelementContext.class,0);
    }
    public TupletelementContext tupletelement() {
      return getRuleContext(TupletelementContext.class,0);
    }
    public ElementContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_element; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterElement(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitElement(this);
    }
  }

  public final ElementContext element() throws RecognitionException {
    ElementContext _localctx = new ElementContext(_ctx, getState());
    enterRule(_localctx, 42, RULE_element);
    try {
      setState(267);
      switch (_input.LA(1)) {
      case T__9:
      case T__11:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
      case T__19:
      case T__20:
      case T__21:
      case T__22:
      case T__23:
      case T__24:
      case T__25:
      case T__26:
      case T__27:
      case T__28:
      case T__29:
      case T__30:
      case T__33:
      case T__34:
        enterOuterAlt(_localctx, 1);
        {
        setState(265);
        noteelement();
        }
        break;
      case T__36:
        enterOuterAlt(_localctx, 2);
        {
        setState(266);
        tupletelement();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class NoteelementContext extends ParserRuleContext {
    public NoteContext note() {
      return getRuleContext(NoteContext.class,0);
    }
    public MultinoteContext multinote() {
      return getRuleContext(MultinoteContext.class,0);
    }
    public NoteelementContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_noteelement; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterNoteelement(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitNoteelement(this);
    }
  }

  public final NoteelementContext noteelement() throws RecognitionException {
    NoteelementContext _localctx = new NoteelementContext(_ctx, getState());
    enterRule(_localctx, 44, RULE_noteelement);
    try {
      setState(271);
      switch (_input.LA(1)) {
      case T__9:
      case T__11:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
      case T__19:
      case T__20:
      case T__21:
      case T__22:
      case T__23:
      case T__24:
      case T__25:
      case T__26:
      case T__27:
      case T__28:
      case T__29:
      case T__30:
      case T__33:
        enterOuterAlt(_localctx, 1);
        {
        setState(269);
        note();
        }
        break;
      case T__34:
        enterOuterAlt(_localctx, 2);
        {
        setState(270);
        multinote();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class NoteContext extends ParserRuleContext {
    public NoteorrestContext noteorrest() {
      return getRuleContext(NoteorrestContext.class,0);
    }
    public NotelengthContext notelength() {
      return getRuleContext(NotelengthContext.class,0);
    }
    public NoteContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_note; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterNote(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitNote(this);
    }
  }

  public final NoteContext note() throws RecognitionException {
    NoteContext _localctx = new NoteContext(_ctx, getState());
    enterRule(_localctx, 46, RULE_note);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(273);
      noteorrest();
      setState(275);
      switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
      case 1:
        {
        setState(274);
        notelength();
        }
        break;
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class NoteorrestContext extends ParserRuleContext {
    public PitchContext pitch() {
      return getRuleContext(PitchContext.class,0);
    }
    public RestContext rest() {
      return getRuleContext(RestContext.class,0);
    }
    public NoteorrestContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_noteorrest; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterNoteorrest(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitNoteorrest(this);
    }
  }

  public final NoteorrestContext noteorrest() throws RecognitionException {
    NoteorrestContext _localctx = new NoteorrestContext(_ctx, getState());
    enterRule(_localctx, 48, RULE_noteorrest);
    try {
      setState(279);
      switch (_input.LA(1)) {
      case T__9:
      case T__11:
      case T__14:
      case T__15:
      case T__16:
      case T__17:
      case T__18:
      case T__19:
      case T__20:
      case T__21:
      case T__22:
      case T__23:
      case T__24:
      case T__25:
      case T__26:
      case T__27:
      case T__28:
      case T__29:
      case T__30:
        enterOuterAlt(_localctx, 1);
        {
        setState(277);
        pitch();
        }
        break;
      case T__33:
        enterOuterAlt(_localctx, 2);
        {
        setState(278);
        rest();
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class PitchContext extends ParserRuleContext {
    public BasenoteContext basenote() {
      return getRuleContext(BasenoteContext.class,0);
    }
    public AccidentalContext accidental() {
      return getRuleContext(AccidentalContext.class,0);
    }
    public OctaveContext octave() {
      return getRuleContext(OctaveContext.class,0);
    }
    public PitchContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_pitch; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterPitch(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitPitch(this);
    }
  }

  public final PitchContext pitch() throws RecognitionException {
    PitchContext _localctx = new PitchContext(_ctx, getState());
    enterRule(_localctx, 50, RULE_pitch);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(282);
      _la = _input.LA(1);
      if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18))) != 0)) {
        {
        setState(281);
        accidental();
        }
      }

      setState(284);
      basenote();
      setState(286);
      _la = _input.LA(1);
      if (_la==T__31 || _la==T__32) {
        {
        setState(285);
        octave();
        }
      }

      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class AccidentalContext extends ParserRuleContext {
    public AccidentalContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_accidental; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterAccidental(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitAccidental(this);
    }
  }

  public final AccidentalContext accidental() throws RecognitionException {
    AccidentalContext _localctx = new AccidentalContext(_ctx, getState());
    enterRule(_localctx, 52, RULE_accidental);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(288);
      _la = _input.LA(1);
      if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18))) != 0)) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class BasenoteContext extends ParserRuleContext {
    public BasenoteContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_basenote; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterBasenote(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitBasenote(this);
    }
  }

  public final BasenoteContext basenote() throws RecognitionException {
    BasenoteContext _localctx = new BasenoteContext(_ctx, getState());
    enterRule(_localctx, 54, RULE_basenote);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(290);
      _la = _input.LA(1);
      if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__11) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30))) != 0)) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class OctaveContext extends ParserRuleContext {
    public OctaveContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_octave; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterOctave(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitOctave(this);
    }
  }

  public final OctaveContext octave() throws RecognitionException {
    OctaveContext _localctx = new OctaveContext(_ctx, getState());
    enterRule(_localctx, 56, RULE_octave);
    int _la;
    try {
      setState(302);
      switch (_input.LA(1)) {
      case T__31:
        enterOuterAlt(_localctx, 1);
        {
        setState(293); 
        _errHandler.sync(this);
        _la = _input.LA(1);
        do {
          {
          {
          setState(292);
          match(T__31);
          }
          }
          setState(295); 
          _errHandler.sync(this);
          _la = _input.LA(1);
        } while ( _la==T__31 );
        }
        break;
      case T__32:
        enterOuterAlt(_localctx, 2);
        {
        setState(298); 
        _errHandler.sync(this);
        _la = _input.LA(1);
        do {
          {
          {
          setState(297);
          match(T__32);
          }
          }
          setState(300); 
          _errHandler.sync(this);
          _la = _input.LA(1);
        } while ( _la==T__32 );
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class RestContext extends ParserRuleContext {
    public RestContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_rest; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterRest(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitRest(this);
    }
  }

  public final RestContext rest() throws RecognitionException {
    RestContext _localctx = new RestContext(_ctx, getState());
    enterRule(_localctx, 58, RULE_rest);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(304);
      match(T__33);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class NotelengthContext extends ParserRuleContext {
    public List<TerminalNode> DIGIT() { return getTokens(AbcParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(AbcParser.DIGIT, i);
    }
    public NotelengthContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_notelength; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterNotelength(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitNotelength(this);
    }
  }

  public final NotelengthContext notelength() throws RecognitionException {
    NotelengthContext _localctx = new NotelengthContext(_ctx, getState());
    enterRule(_localctx, 60, RULE_notelength);
    int _la;
    try {
      setState(324);
      switch ( getInterpreter().adaptivePredict(_input,48,_ctx) ) {
      case 1:
        enterOuterAlt(_localctx, 1);
        {
        setState(306);
        match(T__13);
        }
        break;
      case 2:
        enterOuterAlt(_localctx, 2);
        {
        setState(312);
        _la = _input.LA(1);
        if (_la==DIGIT) {
          {
          setState(308); 
          _errHandler.sync(this);
          _la = _input.LA(1);
          do {
            {
            {
            setState(307);
            match(DIGIT);
            }
            }
            setState(310); 
            _errHandler.sync(this);
            _la = _input.LA(1);
          } while ( _la==DIGIT );
          }
        }

        setState(322);
        _la = _input.LA(1);
        if (_la==T__13) {
          {
          setState(314);
          match(T__13);
          setState(320);
          _la = _input.LA(1);
          if (_la==DIGIT) {
            {
            setState(316); 
            _errHandler.sync(this);
            _la = _input.LA(1);
            do {
              {
              {
              setState(315);
              match(DIGIT);
              }
              }
              setState(318); 
              _errHandler.sync(this);
              _la = _input.LA(1);
            } while ( _la==DIGIT );
            }
          }

          }
        }

        }
        break;
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class NotelengthstrictContext extends ParserRuleContext {
    public List<TerminalNode> DIGIT() { return getTokens(AbcParser.DIGIT); }
    public TerminalNode DIGIT(int i) {
      return getToken(AbcParser.DIGIT, i);
    }
    public NotelengthstrictContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_notelengthstrict; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterNotelengthstrict(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitNotelengthstrict(this);
    }
  }

  public final NotelengthstrictContext notelengthstrict() throws RecognitionException {
    NotelengthstrictContext _localctx = new NotelengthstrictContext(_ctx, getState());
    enterRule(_localctx, 62, RULE_notelengthstrict);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(327); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(326);
        match(DIGIT);
        }
        }
        setState(329); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      setState(331);
      match(T__13);
      setState(333); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(332);
        match(DIGIT);
        }
        }
        setState(335); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( _la==DIGIT );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class MultinoteContext extends ParserRuleContext {
    public List<NoteContext> note() {
      return getRuleContexts(NoteContext.class);
    }
    public NoteContext note(int i) {
      return getRuleContext(NoteContext.class,i);
    }
    public MultinoteContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_multinote; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterMultinote(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitMultinote(this);
    }
  }

  public final MultinoteContext multinote() throws RecognitionException {
    MultinoteContext _localctx = new MultinoteContext(_ctx, getState());
    enterRule(_localctx, 64, RULE_multinote);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(337);
      match(T__34);
      setState(339); 
      _errHandler.sync(this);
      _la = _input.LA(1);
      do {
        {
        {
        setState(338);
        note();
        }
        }
        setState(341); 
        _errHandler.sync(this);
        _la = _input.LA(1);
      } while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__11) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__33))) != 0) );
      setState(343);
      match(T__35);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class TupletelementContext extends ParserRuleContext {
    public TupletspecContext tupletspec() {
      return getRuleContext(TupletspecContext.class,0);
    }
    public List<NoteelementContext> noteelement() {
      return getRuleContexts(NoteelementContext.class);
    }
    public NoteelementContext noteelement(int i) {
      return getRuleContext(NoteelementContext.class,i);
    }
    public TupletelementContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_tupletelement; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterTupletelement(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitTupletelement(this);
    }
  }

  public final TupletelementContext tupletelement() throws RecognitionException {
    TupletelementContext _localctx = new TupletelementContext(_ctx, getState());
    enterRule(_localctx, 66, RULE_tupletelement);
    try {
      int _alt;
      enterOuterAlt(_localctx, 1);
      {
      setState(345);
      tupletspec();
      setState(347); 
      _errHandler.sync(this);
      _alt = 1;
      do {
        switch (_alt) {
        case 1:
          {
          {
          setState(346);
          noteelement();
          }
          }
          break;
        default:
          throw new NoViableAltException(this);
        }
        setState(349); 
        _errHandler.sync(this);
        _alt = getInterpreter().adaptivePredict(_input,52,_ctx);
      } while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class TupletspecContext extends ParserRuleContext {
    public TerminalNode DIGIT() { return getToken(AbcParser.DIGIT, 0); }
    public TupletspecContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_tupletspec; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterTupletspec(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitTupletspec(this);
    }
  }

  public final TupletspecContext tupletspec() throws RecognitionException {
    TupletspecContext _localctx = new TupletspecContext(_ctx, getState());
    enterRule(_localctx, 68, RULE_tupletspec);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(351);
      match(T__36);
      setState(352);
      match(DIGIT);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class BarlineContext extends ParserRuleContext {
    public BarlineContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_barline; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterBarline(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitBarline(this);
    }
  }

  public final BarlineContext barline() throws RecognitionException {
    BarlineContext _localctx = new BarlineContext(_ctx, getState());
    enterRule(_localctx, 70, RULE_barline);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(354);
      _la = _input.LA(1);
      if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__37) | (1L << T__38) | (1L << T__39))) != 0)) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class NewmajorsectionContext extends ParserRuleContext {
    public NewmajorsectionContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_newmajorsection; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterNewmajorsection(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitNewmajorsection(this);
    }
  }

  public final NewmajorsectionContext newmajorsection() throws RecognitionException {
    NewmajorsectionContext _localctx = new NewmajorsectionContext(_ctx, getState());
    enterRule(_localctx, 72, RULE_newmajorsection);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(356);
      _la = _input.LA(1);
      if ( !(_la==T__35 || _la==T__37) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class RepeatstartContext extends ParserRuleContext {
    public RepeatstartContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_repeatstart; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterRepeatstart(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitRepeatstart(this);
    }
  }

  public final RepeatstartContext repeatstart() throws RecognitionException {
    RepeatstartContext _localctx = new RepeatstartContext(_ctx, getState());
    enterRule(_localctx, 74, RULE_repeatstart);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(358);
      match(T__40);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class RepeatendContext extends ParserRuleContext {
    public RepeatendContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_repeatend; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterRepeatend(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitRepeatend(this);
    }
  }

  public final RepeatendContext repeatend() throws RecognitionException {
    RepeatendContext _localctx = new RepeatendContext(_ctx, getState());
    enterRule(_localctx, 76, RULE_repeatend);
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(360);
      match(T__40);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class NthrepeatContext extends ParserRuleContext {
    public NthrepeatContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_nthrepeat; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterNthrepeat(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitNthrepeat(this);
    }
  }

  public final NthrepeatContext nthrepeat() throws RecognitionException {
    NthrepeatContext _localctx = new NthrepeatContext(_ctx, getState());
    enterRule(_localctx, 78, RULE_nthrepeat);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(362);
      _la = _input.LA(1);
      if ( !(_la==T__41 || _la==T__42) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class EndoflineContext extends ParserRuleContext {
    public CommentContext comment() {
      return getRuleContext(CommentContext.class,0);
    }
    public TerminalNode NEWLINE() { return getToken(AbcParser.NEWLINE, 0); }
    public EndoflineContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_endofline; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterEndofline(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitEndofline(this);
    }
  }

  public final EndoflineContext endofline() throws RecognitionException {
    EndoflineContext _localctx = new EndoflineContext(_ctx, getState());
    enterRule(_localctx, 80, RULE_endofline);
    try {
      setState(366);
      switch (_input.LA(1)) {
      case T__43:
        enterOuterAlt(_localctx, 1);
        {
        setState(364);
        comment();
        }
        break;
      case NEWLINE:
        enterOuterAlt(_localctx, 2);
        {
        setState(365);
        match(NEWLINE);
        }
        break;
      default:
        throw new NoViableAltException(this);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class CommentContext extends ParserRuleContext {
    public TerminalNode NEWLINE() { return getToken(AbcParser.NEWLINE, 0); }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public List<TextContext> text() {
      return getRuleContexts(TextContext.class);
    }
    public TextContext text(int i) {
      return getRuleContext(TextContext.class,i);
    }
    public CommentContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_comment; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterComment(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitComment(this);
    }
  }

  public final CommentContext comment() throws RecognitionException {
    CommentContext _localctx = new CommentContext(_ctx, getState());
    enterRule(_localctx, 82, RULE_comment);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(368);
      match(T__43);
      setState(370);
      switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
      case 1:
        {
        setState(369);
        match(WHITESPACE);
        }
        break;
      }
      setState(375);
      _errHandler.sync(this);
      _la = _input.LA(1);
      while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << DIGIT) | (1L << LETTER) | (1L << PUNCTUATION) | (1L << WHITESPACE))) != 0)) {
        {
        {
        setState(372);
        text();
        }
        }
        setState(377);
        _errHandler.sync(this);
        _la = _input.LA(1);
      }
      setState(378);
      match(NEWLINE);
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static class TextContext extends ParserRuleContext {
    public TerminalNode DIGIT() { return getToken(AbcParser.DIGIT, 0); }
    public TerminalNode LETTER() { return getToken(AbcParser.LETTER, 0); }
    public TerminalNode WHITESPACE() { return getToken(AbcParser.WHITESPACE, 0); }
    public TerminalNode PUNCTUATION() { return getToken(AbcParser.PUNCTUATION, 0); }
    public TextContext(ParserRuleContext parent, int invokingState) {
      super(parent, invokingState);
    }
    @Override public int getRuleIndex() { return RULE_text; }
    @Override
    public void enterRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).enterText(this);
    }
    @Override
    public void exitRule(ParseTreeListener listener) {
      if ( listener instanceof AbcListener ) ((AbcListener)listener).exitText(this);
    }
  }

  public final TextContext text() throws RecognitionException {
    TextContext _localctx = new TextContext(_ctx, getState());
    enterRule(_localctx, 84, RULE_text);
    int _la;
    try {
      enterOuterAlt(_localctx, 1);
      {
      setState(380);
      _la = _input.LA(1);
      if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__11) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << DIGIT) | (1L << LETTER) | (1L << PUNCTUATION) | (1L << WHITESPACE))) != 0)) ) {
      _errHandler.recoverInline(this);
      } else {
        consume();
      }
      }
    }
    catch (RecognitionException re) {
      _localctx.exception = re;
      _errHandler.reportError(this, re);
      _errHandler.recover(this, re);
    }
    finally {
      exitRule();
    }
    return _localctx;
  }

  public static final String _serializedATN =
    "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\65\u0181\4\2\t"+
      "\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n"+
      "\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21"+
      "\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30"+
      "\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36"+
      "\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4("+
      "\t(\4)\t)\4*\t*\4+\t+\4,\t,\3\2\3\2\3\2\3\2\3\3\3\3\7\3_\n\3\f\3\16"+
      "\3b\13\3\3\3\3\3\7\3f\n\3\f\3\16\3i\13\3\3\3\3\3\3\4\3\4\5\4o\n\4"+
      "\3\4\6\4r\n\4\r\4\16\4s\3\4\3\4\3\5\3\5\5\5z\n\5\3\5\7\5}\n\5\f\5"+
      "\16\5\u0080\13\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\5\6\u0089\n\6\3\7\3\7"+
      "\5\7\u008d\n\7\3\7\7\7\u0090\n\7\f\7\16\7\u0093\13\7\3\7\3\7\3\b\3"+
      "\b\5\b\u0099\n\b\3\b\3\b\3\b\3\t\3\t\5\t\u00a0\n\t\3\t\3\t\3\t\3\n"+
      "\3\n\5\n\u00a7\n\n\3\n\3\n\3\n\3\13\3\13\5\13\u00ae\n\13\3\13\3\13"+
      "\3\13\3\f\3\f\5\f\u00b5\n\f\3\f\3\f\3\f\3\r\3\r\5\r\u00bc\n\r\3\16"+
      "\3\16\5\16\u00c0\n\16\3\17\3\17\3\20\3\20\3\21\3\21\3\21\5\21\u00c9"+
      "\n\21\3\22\6\22\u00cc\n\22\r\22\16\22\u00cd\3\22\3\22\6\22\u00d2\n"+
      "\22\r\22\16\22\u00d3\3\23\3\23\3\23\6\23\u00d9\n\23\r\23\16\23\u00da"+
      "\3\24\3\24\6\24\u00df\n\24\r\24\16\24\u00e0\3\25\3\25\6\25\u00e5\n"+
      "\25\r\25\16\25\u00e6\3\25\5\25\u00ea\n\25\3\25\6\25\u00ed\n\25\r\25"+
      "\16\25\u00ee\3\25\5\25\u00f2\n\25\5\25\u00f4\n\25\3\26\5\26\u00f7"+
      "\n\26\3\26\5\26\u00fa\n\26\3\26\5\26\u00fd\n\26\3\26\3\26\6\26\u0101"+
      "\n\26\r\26\16\26\u0102\3\26\5\26\u0106\n\26\3\26\3\26\5\26\u010a\n"+
      "\26\3\27\3\27\5\27\u010e\n\27\3\30\3\30\5\30\u0112\n\30\3\31\3\31"+
      "\5\31\u0116\n\31\3\32\3\32\5\32\u011a\n\32\3\33\5\33\u011d\n\33\3"+
      "\33\3\33\5\33\u0121\n\33\3\34\3\34\3\35\3\35\3\36\6\36\u0128\n\36"+
      "\r\36\16\36\u0129\3\36\6\36\u012d\n\36\r\36\16\36\u012e\5\36\u0131"+
      "\n\36\3\37\3\37\3 \3 \6 \u0137\n \r \16 \u0138\5 \u013b\n \3 \3 \6"+
      " \u013f\n \r \16 \u0140\5 \u0143\n \5 \u0145\n \5 \u0147\n \3!\6!"+
      "\u014a\n!\r!\16!\u014b\3!\3!\6!\u0150\n!\r!\16!\u0151\3\"\3\"\6\""+
      "\u0156\n\"\r\"\16\"\u0157\3\"\3\"\3#\3#\6#\u015e\n#\r#\16#\u015f\3"+
      "$\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3*\5*\u0171\n*\3+\3+\5"+
      "+\u0175\n+\3+\7+\u0178\n+\f+\16+\u017b\13+\3+\3+\3,\3,\3,\2\2-\2\4"+
      "\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFH"+
      "JLNPRTV\2\t\3\2\13\f\3\2\21\25\5\2\f\f\16\16\26!\3\2(*\4\2&&((\3\2"+
      ",-\3\2\13\63\u0191\2X\3\2\2\2\4\\\3\2\2\2\6l\3\2\2\2\bw\3\2\2\2\n"+
      "\u0088\3\2\2\2\f\u008a\3\2\2\2\16\u0096\3\2\2\2\20\u009d\3\2\2\2\22"+
      "\u00a4\3\2\2\2\24\u00ab\3\2\2\2\26\u00b2\3\2\2\2\30\u00b9\3\2\2\2"+
      "\32\u00bd\3\2\2\2\34\u00c1\3\2\2\2\36\u00c3\3\2\2\2 \u00c8\3\2\2\2"+
      "\"\u00cb\3\2\2\2$\u00d5\3\2\2\2&\u00de\3\2\2\2(\u00f3\3\2\2\2*\u00f6"+
      "\3\2\2\2,\u010d\3\2\2\2.\u0111\3\2\2\2\60\u0113\3\2\2\2\62\u0119\3"+
      "\2\2\2\64\u011c\3\2\2\2\66\u0122\3\2\2\28\u0124\3\2\2\2:\u0130\3\2"+
      "\2\2<\u0132\3\2\2\2>\u0146\3\2\2\2@\u0149\3\2\2\2B\u0153\3\2\2\2D"+
      "\u015b\3\2\2\2F\u0161\3\2\2\2H\u0164\3\2\2\2J\u0166\3\2\2\2L\u0168"+
      "\3\2\2\2N\u016a\3\2\2\2P\u016c\3\2\2\2R\u0170\3\2\2\2T\u0172\3\2\2"+
      "\2V\u017e\3\2\2\2XY\5\4\3\2YZ\5&\24\2Z[\7\2\2\3[\3\3\2\2\2\\`\5\6"+
      "\4\2]_\5T+\2^]\3\2\2\2_b\3\2\2\2`^\3\2\2\2`a\3\2\2\2ac\3\2\2\2b`\3"+
      "\2\2\2cg\5\b\5\2df\5\n\6\2ed\3\2\2\2fi\3\2\2\2ge\3\2\2\2gh\3\2\2\2"+
      "hj\3\2\2\2ig\3\2\2\2jk\5\26\f\2k\5\3\2\2\2ln\7\3\2\2mo\7\63\2\2nm"+
      "\3\2\2\2no\3\2\2\2oq\3\2\2\2pr\7\60\2\2qp\3\2\2\2rs\3\2\2\2sq\3\2"+
      "\2\2st\3\2\2\2tu\3\2\2\2uv\5R*\2v\7\3\2\2\2wy\7\4\2\2xz\7\63\2\2y"+
      "x\3\2\2\2yz\3\2\2\2z~\3\2\2\2{}\5V,\2|{\3\2\2\2}\u0080\3\2\2\2~|\3"+
      "\2\2\2~\177\3\2\2\2\177\u0081\3\2\2\2\u0080~\3\2\2\2\u0081\u0082\5"+
      "R*\2\u0082\t\3\2\2\2\u0083\u0089\5\f\7\2\u0084\u0089\5\16\b\2\u0085"+
      "\u0089\5\20\t\2\u0086\u0089\5\22\n\2\u0087\u0089\5\24\13\2\u0088\u0083"+
      "\3\2\2\2\u0088\u0084\3\2\2\2\u0088\u0085\3\2\2\2\u0088\u0086\3\2\2"+
      "\2\u0088\u0087\3\2\2\2\u0089\13\3\2\2\2\u008a\u008c\7\5\2\2\u008b"+
      "\u008d\7\63\2\2\u008c\u008b\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u0091"+
      "\3\2\2\2\u008e\u0090\5V,\2\u008f\u008e\3\2\2\2\u0090\u0093\3\2\2\2"+
      "\u0091\u008f\3\2\2\2\u0091\u0092\3\2\2\2\u0092\u0094\3\2\2\2\u0093"+
      "\u0091\3\2\2\2\u0094\u0095\5R*\2\u0095\r\3\2\2\2\u0096\u0098\7\6\2"+
      "\2\u0097\u0099\7\63\2\2\u0098\u0097\3\2\2\2\u0098\u0099\3\2\2\2\u0099"+
      "\u009a\3\2\2\2\u009a\u009b\5@!\2\u009b\u009c\5R*\2\u009c\17\3\2\2"+
      "\2\u009d\u009f\7\7\2\2\u009e\u00a0\7\63\2\2\u009f\u009e\3\2\2\2\u009f"+
      "\u00a0\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a2\5 \21\2\u00a2\u00a3"+
      "\5R*\2\u00a3\21\3\2\2\2\u00a4\u00a6\7\b\2\2\u00a5\u00a7\7\63\2\2\u00a6"+
      "\u00a5\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a9"+
      "\5$\23\2\u00a9\u00aa\5R*\2\u00aa\23\3\2\2\2\u00ab\u00ad\7\t\2\2\u00ac"+
      "\u00ae\7\63\2\2\u00ad\u00ac\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00af"+
      "\3\2\2\2\u00af\u00b0\5V,\2\u00b0\u00b1\5R*\2\u00b1\25\3\2\2\2\u00b2"+
      "\u00b4\7\n\2\2\u00b3\u00b5\7\63\2\2\u00b4\u00b3\3\2\2\2\u00b4\u00b5"+
      "\3\2\2\2\u00b5\u00b6\3\2\2\2\u00b6\u00b7\5\30\r\2\u00b7\u00b8\5R*"+
      "\2\u00b8\27\3\2\2\2\u00b9\u00bb\5\32\16\2\u00ba\u00bc\5\36\20\2\u00bb"+
      "\u00ba\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\31\3\2\2\2\u00bd\u00bf\5"+
      "8\35\2\u00be\u00c0\5\34\17\2\u00bf\u00be\3\2\2\2\u00bf\u00c0\3\2\2"+
      "\2\u00c0\33\3\2\2\2\u00c1\u00c2\t\2\2\2\u00c2\35\3\2\2\2\u00c3\u00c4"+
      "\7\r\2\2\u00c4\37\3\2\2\2\u00c5\u00c9\7\16\2\2\u00c6\u00c9\7\17\2"+
      "\2\u00c7\u00c9\5\"\22\2\u00c8\u00c5\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c8"+
      "\u00c7\3\2\2\2\u00c9!\3\2\2\2\u00ca\u00cc\7\60\2\2\u00cb\u00ca\3\2"+
      "\2\2\u00cc\u00cd\3\2\2\2\u00cd\u00cb\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce"+
      "\u00cf\3\2\2\2\u00cf\u00d1\7\20\2\2\u00d0\u00d2\7\60\2\2\u00d1\u00d0"+
      "\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d3\u00d4\3\2\2"+
      "\2\u00d4#\3\2\2\2\u00d5\u00d6\5\"\22\2\u00d6\u00d8\7\21\2\2\u00d7"+
      "\u00d9\7\60\2\2\u00d8\u00d7\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00d8"+
      "\3\2\2\2\u00da\u00db\3\2\2\2\u00db%\3\2\2\2\u00dc\u00df\5(\25\2\u00dd"+
      "\u00df\5T+\2\u00de\u00dc\3\2\2\2\u00de\u00dd\3\2\2\2\u00df\u00e0\3"+
      "\2\2\2\u00e0\u00de\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1\'\3\2\2\2\u00e2"+
      "\u00e4\5\24\13\2\u00e3\u00e5\5*\26\2\u00e4\u00e3\3\2\2\2\u00e5\u00e6"+
      "\3\2\2\2\u00e6\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7\u00e9\3\2\2"+
      "\2\u00e8\u00ea\5R*\2\u00e9\u00e8\3\2\2\2\u00e9\u00ea\3\2\2\2\u00ea"+
      "\u00f4\3\2\2\2\u00eb\u00ed\5*\26\2\u00ec\u00eb\3\2\2\2\u00ed\u00ee"+
      "\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f1\3\2\2"+
      "\2\u00f0\u00f2\5R*\2\u00f1\u00f0\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2"+
      "\u00f4\3\2\2\2\u00f3\u00e2\3\2\2\2\u00f3\u00ec\3\2\2\2\u00f4)\3\2"+
      "\2\2\u00f5\u00f7\5J&\2\u00f6\u00f5\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7"+
      "\u00f9\3\2\2\2\u00f8\u00fa\5P)\2\u00f9\u00f8\3\2\2\2\u00f9\u00fa\3"+
      "\2\2\2\u00fa\u00fc\3\2\2\2\u00fb\u00fd\5L\'\2\u00fc\u00fb\3\2\2\2"+
      "\u00fc\u00fd\3\2\2\2\u00fd\u0100\3\2\2\2\u00fe\u0101\5,\27\2\u00ff"+
      "\u0101\7\63\2\2\u0100\u00fe\3\2\2\2\u0100\u00ff\3\2\2\2\u0101\u0102"+
      "\3\2\2\2\u0102\u0100\3\2\2\2\u0102\u0103\3\2\2\2\u0103\u0105\3\2\2"+
      "\2\u0104\u0106\5N(\2\u0105\u0104\3\2\2\2\u0105\u0106\3\2\2\2\u0106"+
      "\u0109\3\2\2\2\u0107\u010a\5H%\2\u0108\u010a\5R*\2\u0109\u0107\3\2"+
      "\2\2\u0109\u0108\3\2\2\2\u010a+\3\2\2\2\u010b\u010e\5.\30\2\u010c"+
      "\u010e\5D#\2\u010d\u010b\3\2\2\2\u010d\u010c\3\2\2\2\u010e-\3\2\2"+
      "\2\u010f\u0112\5\60\31\2\u0110\u0112\5B\"\2\u0111\u010f\3\2\2\2\u0111"+
      "\u0110\3\2\2\2\u0112/\3\2\2\2\u0113\u0115\5\62\32\2\u0114\u0116\5"+
      "> \2\u0115\u0114\3\2\2\2\u0115\u0116\3\2\2\2\u0116\61\3\2\2\2\u0117"+
      "\u011a\5\64\33\2\u0118\u011a\5<\37\2\u0119\u0117\3\2\2\2\u0119\u0118"+
      "\3\2\2\2\u011a\63\3\2\2\2\u011b\u011d\5\66\34\2\u011c\u011b\3\2\2"+
      "\2\u011c\u011d\3\2\2\2\u011d\u011e\3\2\2\2\u011e\u0120\58\35\2\u011f"+
      "\u0121\5:\36\2\u0120\u011f\3\2\2\2\u0120\u0121\3\2\2\2\u0121\65\3"+
      "\2\2\2\u0122\u0123\t\3\2\2\u0123\67\3\2\2\2\u0124\u0125\t\4\2\2\u0125"+
      "9\3\2\2\2\u0126\u0128\7\"\2\2\u0127\u0126\3\2\2\2\u0128\u0129\3\2"+
      "\2\2\u0129\u0127\3\2\2\2\u0129\u012a\3\2\2\2\u012a\u0131\3\2\2\2\u012b"+
      "\u012d\7#\2\2\u012c\u012b\3\2\2\2\u012d\u012e\3\2\2\2\u012e\u012c"+
      "\3\2\2\2\u012e\u012f\3\2\2\2\u012f\u0131\3\2\2\2\u0130\u0127\3\2\2"+
      "\2\u0130\u012c\3\2\2\2\u0131;\3\2\2\2\u0132\u0133\7$\2\2\u0133=\3"+
      "\2\2\2\u0134\u0147\7\20\2\2\u0135\u0137\7\60\2\2\u0136\u0135\3\2\2"+
      "\2\u0137\u0138\3\2\2\2\u0138\u0136\3\2\2\2\u0138\u0139\3\2\2\2\u0139"+
      "\u013b\3\2\2\2\u013a\u0136\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u0144"+
      "\3\2\2\2\u013c\u0142\7\20\2\2\u013d\u013f\7\60\2\2\u013e\u013d\3\2"+
      "\2\2\u013f\u0140\3\2\2\2\u0140\u013e\3\2\2\2\u0140\u0141\3\2\2\2\u0141"+
      "\u0143\3\2\2\2\u0142\u013e\3\2\2\2\u0142\u0143\3\2\2\2\u0143\u0145"+
      "\3\2\2\2\u0144\u013c\3\2\2\2\u0144\u0145\3\2\2\2\u0145\u0147\3\2\2"+
      "\2\u0146\u0134\3\2\2\2\u0146\u013a\3\2\2\2\u0147?\3\2\2\2\u0148\u014a"+
      "\7\60\2\2\u0149\u0148\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u0149\3\2"+
      "\2\2\u014b\u014c\3\2\2\2\u014c\u014d\3\2\2\2\u014d\u014f\7\20\2\2"+
      "\u014e\u0150\7\60\2\2\u014f\u014e\3\2\2\2\u0150\u0151\3\2\2\2\u0151"+
      "\u014f\3\2\2\2\u0151\u0152\3\2\2\2\u0152A\3\2\2\2\u0153\u0155\7%\2"+
      "\2\u0154\u0156\5\60\31\2\u0155\u0154\3\2\2\2\u0156\u0157\3\2\2\2\u0157"+
      "\u0155\3\2\2\2\u0157\u0158\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u015a"+
      "\7&\2\2\u015aC\3\2\2\2\u015b\u015d\5F$\2\u015c\u015e\5.\30\2\u015d"+
      "\u015c\3\2\2\2\u015e\u015f\3\2\2\2\u015f\u015d\3\2\2\2\u015f\u0160"+
      "\3\2\2\2\u0160E\3\2\2\2\u0161\u0162\7\'\2\2\u0162\u0163\7\60\2\2\u0163"+
      "G\3\2\2\2\u0164\u0165\t\5\2\2\u0165I\3\2\2\2\u0166\u0167\t\6\2\2\u0167"+
      "K\3\2\2\2\u0168\u0169\7+\2\2\u0169M\3\2\2\2\u016a\u016b\7+\2\2\u016b"+
      "O\3\2\2\2\u016c\u016d\t\7\2\2\u016dQ\3\2\2\2\u016e\u0171\5T+\2\u016f"+
      "\u0171\7\64\2\2\u0170\u016e\3\2\2\2\u0170\u016f\3\2\2\2\u0171S\3\2"+
      "\2\2\u0172\u0174\7.\2\2\u0173\u0175\7\63\2\2\u0174\u0173\3\2\2\2\u0174"+
      "\u0175\3\2\2\2\u0175\u0179\3\2\2\2\u0176\u0178\5V,\2\u0177\u0176\3"+
      "\2\2\2\u0178\u017b\3\2\2\2\u0179\u0177\3\2\2\2\u0179\u017a\3\2\2\2"+
      "\u017a\u017c\3\2\2\2\u017b\u0179\3\2\2\2\u017c\u017d\7\64\2\2\u017d"+
      "U\3\2\2\2\u017e\u017f\t\b\2\2\u017fW\3\2\2\2:`gnsy~\u0088\u008c\u0091"+
      "\u0098\u009f\u00a6\u00ad\u00b4\u00bb\u00bf\u00c8\u00cd\u00d3\u00da"+
      "\u00de\u00e0\u00e6\u00e9\u00ee\u00f1\u00f3\u00f6\u00f9\u00fc\u0100"+
      "\u0102\u0105\u0109\u010d\u0111\u0115\u0119\u011c\u0120\u0129\u012e"+
      "\u0130\u0138\u013a\u0140\u0142\u0144\u0146\u014b\u0151\u0157\u015f"+
      "\u0170\u0174\u0179";
  public static final ATN _ATN =
    new ATNDeserializer().deserialize(_serializedATN.toCharArray());
  static {
    _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
    for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
      _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
    }
  }
}