package abc.sound;

/**
 * Rest represents a pause in a piece of Music.
 */
public class Rest implements Element {
    private final int duration;
    
    // Abstraction function:
    // Represents a rest in music to be played for particular duration of time.
    // field duration represents the duration of the note in ticks
    // 
    // Representation invariant:
    // duration >= 0
    //
    // Safety from rep exposure:
    // Fields are immutable, private, and final
    //
    
    /**
     * Asserts the Rep Invariant.
     */
    private void checkRep() {
        assert this.duration >= 0;
    }
    
    /**
     * Constructor for a Rest instance. Represents a rest for duration beats.
     * @param duration duration in beats, must be >= 0
     */
    public Rest(int duration) { 
        this.duration = duration;
        checkRep();
    }
    
    @Override
    public int duration() { 
        return duration; 
    }
    
    @Override
    public Repetition repetition() {
        return Repetition.NORMAL;
    }
    
    @Override
    public void play(SequencePlayer player, int atTick){
        return;
    }
    
    @Override
    public int hashCode() {
        return (int) (duration ^ (duration >>> 32));
    }
    
    /**
     * @return Returns true if two rests have the same duration
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Rest)) return false;
        final Rest other = (Rest) obj;
        return this.duration == other.duration;
    }
    
    /**
     * @return Returns a String representing the rest symbol with duration in ticks
     */
    @Override
    public String toString() {
        return "z" + duration;
    }
}
