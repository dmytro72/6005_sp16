package abc.sound;

/**
 * Element is an immutable data type that represents a note element in a piece of music.
 * This is either a Voice, a Bar, a Tuplet, a Chord, a Note, or a Rest
 *
 */
public interface Element {
    
    // Data type definition
    //     Element = Voice(bars: List<Element>)
    //             + Bar(previous: Element, element: Element, repetition: Repetition)
    //             + Tuplet(type: int, left: Element, right: Element)
    //             + Chord(top: Element, bottom: Element)
    //             + Note(duration: int, pitch: Pitch)
    //             + Rest(duration: int)                     
    
    /**
     * 
     * @return represents repetition status of an Element
     */
    public Repetition repetition();
    
    /**
     * @return the duration of this music element.
     */
    public int duration();
    
    /**
     * Plays the element
     * @param player MIDI Sequencer that will be used to play the element.
     * @param atTick the tick at which the element should begin
     */
    public void play(SequencePlayer player, int atTick);
}
