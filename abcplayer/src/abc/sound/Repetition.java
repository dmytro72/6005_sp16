package abc.sound;

/**
 * Repetition represents the presence of repetition of a bar
 * A bar can have the following repetition states:
 * NORMAL: If a bar object is not repeated, use this value
 * START_MAJOR_SECTION: If a bar object is at the beginning of the piece or occurs after a "|]", it takes this value
 * REPEAT: If a bar takes this value, it will be played twice
 * REPEAT_START: The first bar after a start repeat bar will take this value
 * REPEAT_END: The last bar before an end repeat bar will take this value
 * REPEAT_ALL: If an end repeat bar is found, without a complementary begin repeat bar, the bar before the end 
 * repeat bar will take this value
 * FIRST_ENDING: This value is taken by an ending bar in the repeated system that will be played
 * as an ending the first time the system plays
 * SECOND_ENDING: This value is taken by the first ending bar in a repeated system that will be played
 * as an ending the second time the system plays.
 */
public enum Repetition {
    NORMAL,
    START_MAJOR_SECTION,
    REPEAT,
    REPEAT_START,
    REPEAT_END,
    REPEAT_ALL,
    FIRST_ENDING,
    SECOND_ENDING
}
