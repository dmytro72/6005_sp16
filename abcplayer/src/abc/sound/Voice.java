package abc.sound;

import java.util.ArrayList;
import java.util.List;

/**
 * Voice represents an immutable singular voice in a piece of music.
 *
 */
public class Voice implements Element{
     private final int duration;
     private final List<Element> bars = new ArrayList<>();
     
     // Abstraction function:
     // Represents an immutable singular voice in a piece of music.
     // Contains a list of bars represented by this.bars that comprise the voices music.
     // this.duration represents the duration of the voice
     // 
     // Representation invariant:
     // this.bars.size > 0;
     //
     // Safety from rep exposure:
     // All fields are private and final
     // Use of defensive copying for bars
     // Bars are immutable, so client cannot modify them if they still have reference to them
     
     /**
      * Asserts the Rep Invariant.
      */
     private void checkRep() {
         assert this.bars != null;
         assert this.bars.size() > 0;
     }
     
     /**
      * Constructor for a Voice instance. Represents a voice in a piece of Music.
      * Used to concatenate a new bars with previous bars in the same voice
      * @param previous previous bars in a voice part
      * @param bar next bar in a voice part
      */
     
     public Voice(List<Element> bars) {
         int tempDuration = 0;
         
         for (Element bar : bars) {
             this.bars.add(bar);
             tempDuration += bar.duration();
         }
         this.duration = tempDuration;
         checkRep();
     }
     
     @Override
     public int duration() {
         checkRep();
         return this.duration;
     }
     
     @Override
     public Repetition repetition() {
         return Repetition.NORMAL;
     }
     
     @Override
     public void play(SequencePlayer player, int atTick) {
         // keeps track of duration of scheduled bars
         int duration = 0;
         // default is beginning of piece. Changed if it incurs a bar after a "|]"
         int majorSectionIndex = 0;
         // used to store bars that must be repeated
         List<Element> repeatBars = new ArrayList<>();
         
         for(int i = 0; i < bars.size(); i++) {
             Element currentBar = this.bars.get(i);
             // find index of a new start of major section
             if (currentBar.repetition().equals(Repetition.START_MAJOR_SECTION)) {
                 majorSectionIndex = i;
                 // play current bar
                 currentBar.play(player, atTick + duration);
                 // add to total duration scheduled so far
                 duration += currentBar.duration();
             }
             
             // if we have a case where we are missing a begin repeat bar, and get to the end and find an end repeat bar
             // we want to find the beginning of the major section to replay those bars
             else if (currentBar.repetition().equals(Repetition.REPEAT_ALL)) {
                 
                 // create list of bars that must be repeated
                 List<Element> repeatedBars = this.bars.subList(majorSectionIndex, i + 1);
                 // play current bar
                 currentBar.play(player, atTick + duration);
                 // add to total duration scheduled so far
                 duration += currentBar.duration();
                 
                 for(Element bar: repeatedBars) {
                     // play current bar
                     bar.play(player, atTick + duration);
                     // add to total duration scheduled so far
                     duration += bar.duration();
                 }
             }
             
             else if (currentBar.repetition().equals(Repetition.REPEAT_START) || currentBar.repetition().equals(Repetition.REPEAT)) {
                 repeatBars.add(currentBar);
                 // play current bar
                 currentBar.play(player, atTick + duration);
                 // add to total duration scheduled so far
                 duration += currentBar.duration();
             }
             
             else if (currentBar.repetition().equals(Repetition.REPEAT_END)) {
                 // play current bar
                 currentBar.play(player, atTick + duration);
                 // add to total duration scheduled so far
                 duration += currentBar.duration();
                 for(Element bar: repeatBars) {
                     // play current bar
                     bar.play(player, atTick + duration);
                     // add to total duration scheduled so far
                     duration += bar.duration();
                 }
                 repeatBars.clear();
             }
             
             else if (currentBar.repetition().equals(Repetition.FIRST_ENDING)) {
                 // check if next bar is a second ending to determine whether we should play the repeated bars
                 boolean nextBarSecondEnding = this.bars.get(i+1).repetition().equals(Repetition.SECOND_ENDING);
                 
                 if(nextBarSecondEnding) {
                     // play current bar
                     currentBar.play(player, atTick + duration);
                     // add to total duration scheduled so far
                     duration += currentBar.duration();
                     
                     for(Element bar: repeatBars) {
                         // play current bar
                         bar.play(player, atTick + duration);
                         // add to total duration scheduled so far
                         duration += bar.duration();
                     }
                     repeatBars.clear();
                     
                 } else {
                     // play current bar
                     currentBar.play(player, atTick + duration);
                     // add to total duration scheduled so far
                     duration += currentBar.duration();
                 }
             }
             // will only get here if the Repetition is NORMAL or SECOND_ENDING, which play as normal
             else {
                 // play current bar
                 currentBar.play(player, atTick + duration);
                 // add to total duration scheduled so far
                 duration += currentBar.duration();
             }
         }
     }
     
     @Override
     public int hashCode() {
         int result = 0;
         
         for (Element bar: this.bars) {
             result += bar.hashCode();
         }
         checkRep();
         return result;
     }
     
     /**
      * @return Returns true iff the bars in bars are equal and the durations are equal
      */
     @Override
     public boolean equals(Object obj) {
         if (!(obj instanceof Voice)) return false;
         final Voice other = (Voice) obj;
         if (bars.size() != other.bars.size()) return false;
         
         for (int i = 0; i < bars.size(); i++) {
             if(!bars.get(i).equals(other.bars.get(i))) {
                 return false;
             }
         }
         checkRep();
         return duration == other.duration;
     }
     
     /**
      * @return Returns a String representing the voice with it's bars and repeat symbols, similar to the original abc file specification
      */
     @Override
     public String toString() {
         String result = "";
         
         for(int i = 0; i < bars.size(); i++) {
             Element currentBar = this.bars.get(i);
             
             if (currentBar.repetition().equals(Repetition.REPEAT_START)) {
                 result += "|:" + currentBar.toString().substring(1, currentBar.toString().length()-1);
             }
             
             else if (currentBar.repetition().equals(Repetition.REPEAT_ALL) || currentBar.repetition().equals(Repetition.REPEAT_END)) {
                 result += currentBar.toString().substring(1,currentBar.toString().length()-1) + ":|";
             }
             
             else if (currentBar.repetition().equals(Repetition.FIRST_ENDING)) {
                 // check if previous bar is not a first ending
                 boolean previousBarNotFirstEnding = !this.bars.get(i-1).repetition().equals(Repetition.FIRST_ENDING);
                 
                 if(previousBarNotFirstEnding) {
                     result += "[1" + currentBar.toString().substring(1,currentBar.toString().length()-1);
                     
                 } else {
                     result += currentBar.toString().substring(1, currentBar.toString().length()-1);
                 }
             }
             
             else if (currentBar.repetition().equals(Repetition.SECOND_ENDING)) {
                 // check if previous bar is not a first ending
                 boolean previousBarNotSecondEnding = !this.bars.get(i-1).repetition().equals(Repetition.SECOND_ENDING);
                 
                 if(previousBarNotSecondEnding) {
                     result += "[2" + currentBar.toString().substring(1,currentBar.toString().length()-1);
                     
                 } else {
                     result += currentBar.toString().substring(1, currentBar.toString().length()-1);
                 }
             }
             // will only get here if the Repetition is NORMAL, REPEAT, or SECOND_ENDING, which play as normal
             else {
                 result += currentBar.toString().substring(1, currentBar.toString().length()-1);
             }
         }
         return result;
     }
}
