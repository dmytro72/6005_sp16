package abc.sound;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

/**
 * Music represents an immutable piece of music with multiple voices.
 * Each voice can contain repeated bar phrases, tuplets (which can include chords), chords,
 * notes, and rests.
 */

public class Music {
    private final List<Voice> voices = new ArrayList<>();
    private final int duration;
    private final String index;
    private final String title;
    private final String composer;
    private final String meter;
    private final String defaultLength;
    private final String key;
    private final SequencePlayer player;
    
    
    // Abstraction function:
    // Represents an immutable piece of music with multiple voices.
    // this.duration represents the longest voice in the music piece, and hence the duration of the music.
    // this.index represents the index number of the music piece
    // this.title represents the title of the music piece
    // this.composer represents the composer of the music piece
    // this.meter represents the meter of the music piece
    // this.defaultLength represents the default length or duration of a note
    // this.key represents the key of the music piece
    // this.player represents the sequence player that will be used to play the music piece
    // 
    // Representation invariant:
    // this.duration > 0
    // this.voices.size() > 0
    //
    // Safety from rep exposure:
    // All fields are private and final
    // Disregarding voices, all other fields are immutable
    // Use of defensive copying for voices
    // voices are immutable, so client cannot modify them if they still have reference to them
    
    /**
     * Asserts the Rep Invariant.
     */
    private void checkRep() {
        assert this.duration >= 0;
        assert this.voices.size() > 0;
        assert this.index != null;
        assert this.title != null;
        assert this.composer != null;
        assert this.defaultLength != null;
        assert this.key != null;
        assert this.meter != null;
        assert this.player != null;
    }
    
    /**
     * Constructor for a Music instance. Represents a piece of Music.
     * @param index the index number of the music piece
     * @param title the title of the music piece
     * @param composer the composer of the music piece
     * @param key the key of a music piece
     * @param meter the meter of the music piece
     * @param tempo the number of beats per minute of the music piece
     * @param ticksPerBeat the number of ticks per beat; every note plays for an integer number of ticks
     * @param voices various voice parts in the same piece of music.
     * @throws InvalidMidiDataException 
     * @throws MidiUnavailableException 
     */
    public Music(String index, String title, String composer, String key, String meter, String defaultLength, int tempo, int ticksPerBeat, List<Voice> voices) throws MidiUnavailableException, InvalidMidiDataException {
        this.index = index;
        this.title = title;
        this.composer = composer;
        this.key = key;
        this.meter = meter;
        this.defaultLength = defaultLength;
        this.player = new SequencePlayer(tempo, ticksPerBeat);
        int maxDuration = voices.get(0).duration();
        
        for (Voice voice : voices) {
            this.voices.add(voice);
            maxDuration = Math.max(maxDuration, voice.duration());
        }
        this.duration = maxDuration;
        checkRep();
    }
    
    /**
     * @return the duration of this music piece.
     */
    public int duration() {
        checkRep();
        return this.duration;
    }
    
    /**
     * @return the title of this music piece.
     */
    public String title() {
        checkRep();
        return this.title;
    }
    
    /**
     * @return the composer of this music piece.
     */
    public String composer() {
        checkRep();
        return this.composer;
    }
    
    /**
     * @return the key of this music piece.
     */
    public String key() {
        checkRep();
        return this.key;
    }
    
    /**
     * @return the meter of this music piece.
     */
    public String meter() {
        checkRep();
        return this.meter;
    }
    
    /**
     * @return the default length of a note in this music piece.
     */
    public String defaultLength() {
        checkRep();
        return this.defaultLength;
    }
    
    /**
     * @return the index number of this music piece.
     */
    public String index() {
        checkRep();
        return this.index;
    }
    
    /**
     * Plays the piece of Music
     * @param player MIDI Sequencer that will be used to play the piece of Music.
     * @param atTick the tick at which the piece of Music should begin
     */
    public void play(int atTick){
        for(Voice voice: this.voices) {
            voice.play(this.player, atTick);
        }
        try {
            this.player.play();
        } catch (MidiUnavailableException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public int hashCode() {
        int result = this.voices.get(0).hashCode();
        final int prime = 31;
        
        for(Voice voice: this.voices) {
            result += prime * voice.hashCode();
        }
        checkRep();
        return result;
    }

    /**
     * @return Returns true iff indexes are equal, meters are equal, titles are equal, defaultLengths are equal,
     * composers are equal, keys are equal, durations are equal, and voices are equal.
     * , types of chords are equal, and durations are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Music)) return false;
        
        final Music other = (Music) obj;
        if (voices.size() != other.voices.size()) return false;
        
        for (int i = 0; i < voices.size(); i++) {
            if(!voices.get(i).equals(other.voices.get(i))) {
                return false;
            }
        }
        checkRep();
        return duration == other.duration
                && index.equals(other.index)
                && meter.equals(other.meter)
                && title.equals(other.title)
                && defaultLength.equals(defaultLength)
                && composer.equals(other.composer)
                && key.equals(other.key);
    }
    
    /**
     * 
     * @return Returns a string representation of the Music file with it's title, composer, meter, defaultLength, key and voices
     */
    @Override
    public String toString() {
         String result = "X: " + this.index
                 + "\nT: " + this.title
                 + "\nC: " + this.composer
                 + "\nM: " + this.meter
                 + "\nL: " + this.defaultLength
                 + "\nK: " + this.key
                 + "\n";
         
         for(Voice voice: this.voices) {
             result += voice.toString() + "\n";
         }
         
         result = result.substring(0, result.length()-1); // gets rid of trailing '\n'
         checkRep();
         return result;
         
    }  
}

