package abc.sound;


/**
 * Note represents a note played in a piece of Music.
 */
public class Note implements Element {
    private final int duration;
    private final Pitch pitch;

    // Abstraction function:
    // Represents a note of music to be played for particular duration of time.
    // this.duration represents the duration of the note in ticks
    // this.pitch represents the pitch of the note as a Pitch Object.
    // 
    // Representation invariant:
    // duration >= 0
    //
    // Safety from rep exposure:
    // Fields are immutable, private and final
    //
    
    /**
     * Asserts the Rep Invariant.
     */
    private void checkRep() {
        assert pitch != null;
        assert duration >= 0;
    }
    
    /**
     * Constructor for a Note instance. Represents a note for duration beats.
     * @param duration duration in ticks, must be >= 0
     * @param pitch pitch to play
     */
    public Note(int duration, Pitch pitch) {
        this.duration = duration;
        this.pitch = pitch;
        checkRep();
    }
    
    /**
     * @return pitch of this note
     */
    public Pitch pitch() {
        return this.pitch;
    }
    
    @Override
    public int duration() {
        return duration;
    }
    
    @Override
    public Repetition repetition() {
        return Repetition.NORMAL;
    }
    
    @Override
    public void play(SequencePlayer player, int atTick){
        int note = this.pitch.toMidiNote();
        player.addNote(note, atTick, this.duration);
    }
    
    @Override
    public int hashCode() {
        return (int) (duration ^ (duration >>> 32))
                + pitch.hashCode();
    }

    /**
     * @return Returns true if note durations are equal and pitches are the same 
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Note)) return false;
        final Note other = (Note) obj;
        return duration == other.duration
                && pitch.equals(other.pitch);
    }
    
    /**
     * @return Returns a String representing the Note's Pitch with duration in ticks
     */
    @Override
    public String toString() {
        return pitch.toString() + duration;
    }
}
