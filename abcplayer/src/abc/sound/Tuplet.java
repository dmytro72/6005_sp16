package abc.sound;


/**
 * Tuplet represents an immutable tuplet played in a piece of Music.
 * Can either be a duplet (two notes), triplet (three notes) or quadruplet (four notes).
 *
 */
public class Tuplet implements Element{
    private final Element left;
    private final Element right;
    private final int type;
    private final int duration;
    
    // Abstraction function:
    // Represents an immutable tuplet in a piece of music
    // this.left is an Element object representing the left Note/Chord of the tuplet.
    // this.right is an Element object representing the right Note/Chord of the tuplet.
    // this.type represents the type of tuplet: duplet, triplet, or quadruplet
    // 
    // Representation invariant:
    // this.type = 2 || this.type = 3, || this.type = 4
    // duration >= 0
    //
    // Safety from rep exposure:
    // All fields are immutable, private and final.
    //
    
    /**
     * Asserts the Rep Invariant.
     */
    private void checkRep() {
        assert this.left != null;
        assert this.right != null;
        assert this.type == 2 || this.type == 3 || this.type == 4;
        assert this.duration >= 0;
    }
    
    /**
     * Constructor for a Tuplet instance. Represents a tuplet.
     * @param left the left Note/Chord of the tuplet. Cannot be a rest.
     * @param right the right Note/Chord of the tuplet. Cannot be rest.
     */
    public Tuplet(int type, Element left, Element right) {
        this.left = left;
        this.right = right;
        this.type = type;
        this.duration = left.duration() + right.duration();
        checkRep();
    }
    
    @Override
    public int duration() {
        checkRep();
        return this.duration;
    }
    
    @Override
    public Repetition repetition() {
        return Repetition.NORMAL;
    }
    
    @Override
    public void play(SequencePlayer player, int atTick){
        this.left.play(player, atTick);
        this.right.play(player, atTick + left.duration());
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        checkRep();
        return left.hashCode() + prime * right.hashCode();
    }

    /**
     * @return Returns true iff left parts of tuplets are equal, right parts of tuplets are equal
     * , types of tuplets are equal, and durations are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Tuplet)) return false;
        final Tuplet other = (Tuplet) obj;
        return this.left.equals(other.left)
                && this.right.equals(other.right)
                && this.type == other.type
                && this.duration == other.duration;
    }
    
    
    /**
     * @return Returns a String representing the Tuplet type along with the Notes/Chords of the tuplet, and their durations in ticks.
     */
    @Override
    public String toString() {
        String result = "(" + type;
        // this.left is the only Element that could be a Tuplet -> remove starting
        if (this.left instanceof Tuplet) {
            // if left is a tuplet, then remove parenthesis and bracket number
            result += this.left.toString().substring(2);
        } else {
            result += this.left.toString();
        }
        
        // right will always be a Chord or Note
        result += this.right.toString();
        
        return result;
    }
}
