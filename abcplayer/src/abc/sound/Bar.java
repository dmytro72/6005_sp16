package abc.sound;

/**
 * Bar represents a bar in a piece of Music.
 */
public class Bar implements Element {
     private final Element previous;
     private final Element element;
     private final Repetition repetition;
     
     // Abstraction function:
     // Represents a bar of music that contains music elements, which are:
     // Tuplets, Chords, Notes, and Rests
     // this.previous represents all the bars that proceeded this.element
     // this.element represents the next bar to be concatenated to the previous bars.
     // this.repetition represents the repetition value of the bar, which helps order the bars to be played in a voice
     // if there are repeated bars in a voice.
     // 
     // Representation invariant:
     // No additional rep. invariants
     //
     // Safety from rep exposure:
     // Fields are private, final, and immutable
     
     /**
      * Asserts the Rep Invariant.
      */
     private void checkRep() {
         assert this.previous != null;
         assert this.element != null;
         assert this.repetition != null;
     }
     
     /**
      * Constructor for a Bar instance. Represents a bar in a piece of Music.
      * Used to concatenate a new note element with previous note elements in the same bar
      * @param previous previous note elements in a bar
      * @param element the next note element in a bar
      */
     public Bar(Element previous, Element element, Repetition repetition) {
         this.previous = previous;
         this.element = element;
         this.repetition = repetition;
         checkRep();
     }
     
     @Override
     public int duration() {
         checkRep();
         return previous.duration() + element.duration();
     }
     
     @Override
     public Repetition repetition() {
         checkRep();
         return this.repetition;
     }
     
     @Override
     public void play(SequencePlayer player, int atTick){
         this.previous.play(player, atTick);
         this.element.play(player, atTick + previous.duration());
     }
     
     @Override
     public int hashCode() {
         final int prime = 31;
         checkRep();
         return previous.hashCode() + prime * element.hashCode();
     }

     /**
      * @return Returns true iff previous parts of bars are equal, element parts of bars are equal
      * and repetition are equal
      */
     @Override
     public boolean equals(Object obj) {
         if (!(obj instanceof Bar)) return false;
         final Bar other = (Bar) obj;
         return this.previous.equals(other.previous) 
                 && this.element.equals(other.element)
                 && this.repetition.equals(other.repetition);
     }

     /**
      * 
      * @return Returns a string representing the bar with barlines and notes/tuplets/chrods with their durations in ticks
      */
     @Override
     public String toString() {
         String element;
         int elementStringLength =this.element.toString().length();
         
         // this.element is the only Element that could be a chord, so we must delete beginning and ending '|'
         if (this.element.toString().substring(elementStringLength-1).equals("|")) {
             element = this.element.toString().substring(1, elementStringLength -1);
         } else {
             element = this.element.toString();
         }
         
         return "| "+ this.previous.toString() + " " +  element +  " " + "|";
     }
}
