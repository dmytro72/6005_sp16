package abc.sound;

/**
 * Chord represents a chord played in a piece of Music.
 */

public class Chord implements Element {
    private final Element top;
    private final Element bottom;
    private final int duration;
    
    // Abstraction function:
    // Represents a chord in a piece of music
    // this.top is an Element object representing the top Note of the chord.
    // this.bottom is an Element object representing the bottom Note of the chord.
    // this.duration represents the duration of the chord in ticks
    // 
    // Representation invariant:
    // this.duration >= 0
    //
    // Safety from rep exposure:
    // All fields are immutable, private and final
    //
    
    /**
     * Asserts the Rep Invariant.
     */
    private void checkRep() {
        assert this.top != null;
        assert this.bottom != null;
        assert this.duration >= 0;
    }
    
    /**
     * Constructor for a Chord instance. Represents a chord.
     * @param top the top Note of the chord. Cannot be a rest or Tuplet.
     * @param bottom the bottom Note of the chord. Cannot be a rest or Tuplet.
     */
    public Chord(Element top, Element bottom ) {
        this.top = top;
        this.bottom = bottom;
        this.duration = Math.max(top.duration(), bottom.duration());
        checkRep();
    }

    @Override
    public int duration() {
        checkRep();
        return duration;
    }
    
    /**
     * 
     * @return root of the chord, which is an instance of Note
     */
    public Element bottom() {
        checkRep();
        return this.bottom;
    }
    
    /**
     * 
     * @return top part of the chord, which cold be an instance of Note or Chord
     */
    public Element top() {
        checkRep();
        return this.top;
    }
    @Override
    public Repetition repetition() {
        return Repetition.NORMAL;
    }
    
    @Override
    public void play(SequencePlayer player, int atTick){
        top.play(player, atTick);
        bottom.play(player, atTick);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        return top.hashCode() + prime * bottom.hashCode();
    }
    
    /**
     * @return Returns true iff top parts of chords are equal, bottom parts of chords are equal
     * and durations of chords are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Chord)) return false;
        final Chord other = (Chord) obj;
        return this.top.equals(other.top)
                && this.bottom.equals(other.bottom)
                && this.duration == other.duration;
    }
    
    /**
     * @return Returns a String representing Chord Notes with each note's duration in ticks
     */
    @Override
    public String toString() {
        String bottom;
        int bottomStringLength =this.bottom.toString().length();
        
        // this.bottom is the only Element that could be a chord, so we must delete beginning and ending ']'
        if (this.bottom.toString().substring(bottomStringLength-1).equals("]")) {
            bottom = this.bottom.toString().substring(1, bottomStringLength -1);    // deletes brackets from chord
        } else {
            bottom = this.bottom.toString();
        }
        
        return "["+ this.top.toString() + bottom + "]";
    }
}
