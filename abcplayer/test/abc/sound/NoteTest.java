package abc.sound;

import static org.junit.Assert.*;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import org.junit.Test;

import abc.parser.AbcParser.OctaveContext;
import abc.sound.*;

/**
 * 
 * Tests for Note class
 *
 */
public class NoteTest {
    //
    //duration()
    // repetition()
    // hashCode()
    // equals()
    // toString()
    //  Pitch is octave below, above, sharped, flatted
    //
    
    public static final Note note1 = new Note(3, PitchTest.middleC);
    public static final Note note2 = new Note(3, PitchTest.middleC);
    public static final Note middleCLen5 = new Note(5, PitchTest.middleC);
    public static final Note aSharp = new Note(3, PitchTest.aSharp);
    public static final Note highC = new Note(3, PitchTest.highC);
    public static final Note bFlat = new Note(3, PitchTest.bFlat);
    public static final Note highAFlatLen4 = new Note(4, PitchTest.highAFlat);
    public static final Note lowGLen4 = new Note(4, PitchTest.lowG);
    
    
    //Test that equals objects have same hashcode
    @Test
    public void testEqualsHashCode() {
        assertEquals("Notes equal", note1, note2);

        assertEquals("Notes hashcode equal", note1.hashCode(), note2.hashCode());

        assertEquals("Notes equal", aSharp, bFlat);
        
        assertEquals("Notes hashcode equal", aSharp.hashCode(), bFlat.hashCode());
        
    }
     
    
    //Test toString
    @Test
    public void testToString(){
        
        assertEquals("toString", note1.toString(), PitchTest.middleC.toString() + "3");
        assertEquals("toString", aSharp.toString(), PitchTest.aSharp.toString() + "3");
        assertEquals("toString", highC.toString(), PitchTest.highC.toString() + ""
                + "3");
    }
    
    //Test duration
    @Test
    public void testDuration(){
        
        assertEquals("toString", note1.duration(), 3);
        
    }
    
    //Test that equals objects have same hashcode
    @Test
    public void testRepetition(){
        assertEquals("rests equal", note1.repetition(), Repetition.NORMAL);
            
    }
    
    //tests Play
    @Test
    public void testPlay() throws InterruptedException{
    	try{
    	   SequencePlayer player = new SequencePlayer(140, 12);
    	   note1.play(player, 0);
    	   player.play();
    	   Thread.sleep(1000);
    	} catch (MidiUnavailableException mue) {
            mue.printStackTrace();
        } catch (InvalidMidiDataException imde) {
            imde.printStackTrace();
        }
    }
    
}
