package abc.sound;

import static org.junit.Assert.*;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import org.junit.Test;

/**
 *Test for Chord class
 * 
 */
public class ChordTest {
    // hashCode()
    // equals()
    // toString()
    //num of notes in chord: 2, >2
    //top()
    // top is a chord or note
    //bottom()
    // bottom is a chord or note
    //repetition()
    //duration()
    //  Inner notes have same duration, different durations.
    
    
    public static final Chord chord1 = new Chord(NoteTest.aSharp, NoteTest.bFlat);
    public static final Chord chord2  = new Chord(chord1, NoteTest.highC);
    
    public static final Chord chord3 = new Chord(NoteTest.aSharp, NoteTest.bFlat);
    public static final Chord chord4Len3  = new Chord(chord1, NoteTest.highC);
    
    public static final Chord chordDuration4  = new Chord(chord1, NoteTest.highAFlatLen4);
    
    public static final Chord chord6Len5  = new Chord(chord1, NoteTest.middleCLen5);
    
    
    //Test that equals objects have same hashcode
    @Test
    public void testEqualsHashCode() {
        assertEquals("Chords equal", chord2, chord4Len3);
        assertEquals("Chords hashcode equal", chord2.hashCode(), chord4Len3.hashCode());
        
    }
    
    //Test top
    @Test
    public void testTop(){
        assertEquals("test top note", chord1.top(), NoteTest.bFlat);
        assertEquals("Test top chord", chord6Len5.top(),chord1);
        
    }
    
    //Test bottom
    @Test
    public void testBottom(){
        assertEquals("test bottom note", chord1.bottom(), NoteTest.aSharp);
        assertEquals("test bottom chord", chord6Len5.bottom(), NoteTest.middleCLen5);
        
        
    }
    
    //Test toString
    @Test
    public void testToString(){
        
        String expected1 = "[[^A3^A3]C'3]";
        
        assertEquals("toString 1 ", expected1, chord2.toString());
        
        expected1 = "[^A3^A3]";
        assertEquals("toString 2 ", expected1, chord1.toString());
        
    }
    
    //Test repetition
    @Test
    public void testRepetition(){
        assertEquals("repetition", chord1.repetition(), Repetition.NORMAL);
    }
    
    //Test duration
    @Test
    public void testDuration(){
        assertEquals("duration longest", chordDuration4.duration(), 4);
        assertEquals("duration all the same", chord4Len3.duration(), 3);
    }
    
    //Test play
    @Test
    public void testPlay() throws InterruptedException{
    	try{
    	   SequencePlayer player = new SequencePlayer(140, 12);
    	   chord6Len5.play(player, 0);
    	   player.play();
    	   Thread.sleep(1000);
    	} catch (MidiUnavailableException mue) {
            mue.printStackTrace();
        } catch (InvalidMidiDataException imde) {
            imde.printStackTrace();
        }
    }
}
