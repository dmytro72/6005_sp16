package abc.sound;


/**
 * 
 * We did not modify pitch, no tests necessary
 * @category no_didit
 */

public class PitchTest {
    public static Pitch middleC = new Pitch('C');
    public static Pitch highC = new Pitch('C').transpose(Pitch.OCTAVE);
    public static Pitch lowG = new Pitch('G').transpose(-Pitch.OCTAVE);
    public static Pitch aSharp = new Pitch('A').transpose(1);
    public static Pitch bFlat = new Pitch('B').transpose(-1);

    public static Pitch highAFlat = new Pitch('A').transpose(Pitch.OCTAVE).transpose(-1);
}
