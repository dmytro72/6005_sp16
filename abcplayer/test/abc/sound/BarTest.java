package abc.sound;

import static org.junit.Assert.*;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import org.junit.Test;

/**
 * 
 * Tests for Bar class
 *
 */
public class BarTest {
    //duration()
    //  Duration is 0, 1, >1
    // repetition()
    // Repetition can be NORMAl, REPEAT
    // hashCode()
    // equals()
    // toString()
        // # of elements in bar:
        // 0, 1, >1
    //      # of tuplets in bar: 0, 1
   //    # of chords in bar: 0, 1
    //   # of notes in bar: 0, 1
    //   # of rests in bar; 0, 1
    //
     
    
    public static final Bar barTwoChord = new Bar(ChordTest.chord1, ChordTest.chord1, Repetition.NORMAL);
    public static final Bar barTwoNote = new Bar(NoteTest.aSharp, NoteTest.highC, Repetition.FIRST_ENDING);
    public static final Bar barTwoRest = new Bar(new Rest(2), new Rest(2), Repetition.REPEAT);
    public static final Bar barTwoTuple = new Bar(TupletTest.threeTupletOfChords, TupletTest.threeTupletOfNotes, Repetition.NORMAL);


    public static final Bar barOneChord = new Bar(new Rest(0), ChordTest.chord1, Repetition.NORMAL);
    public static final Bar barOneNote = new Bar(new Rest(0), NoteTest.highC, Repetition.FIRST_ENDING);
    public static final Bar barOneRest = new Bar(new Rest(0), new Rest(2), Repetition.NORMAL);
    public static final Bar barOneTupleLen9 = new Bar(new Rest(0), TupletTest.threeTupletOfNotes, Repetition.NORMAL);

    public static final Bar barRestTupleLen11 = new Bar(new Rest(2), TupletTest.threeTupletOfNotes, Repetition.NORMAL);
    public static final Bar barChordTwoRest = new Bar(ChordTest.chord1, barTwoRest, Repetition.NORMAL);
    public static final Bar barNoteTupleLen12 = new Bar(NoteTest.aSharp, TupletTest.threeTupletOfNotes, Repetition.NORMAL);

    
    public static final Bar barThreeChord = new Bar(ChordTest.chord3, barTwoChord, Repetition.NORMAL);
    public static final Bar barThreeNote = new Bar(NoteTest.highAFlatLen4, barTwoNote, Repetition.NORMAL);

    public static final Bar barThreeTuple = new Bar(TupletTest.buildFourTupletOfChords, barTwoTuple, Repetition.NORMAL);
    
    public static final Bar barChordRestTupleLen16 = new Bar(ChordTest.chord6Len5, barRestTupleLen11, Repetition.NORMAL);
    public static final Bar barChordNoteTupleLen17 = new Bar(ChordTest.chord6Len5, barNoteTupleLen12, Repetition.NORMAL);
   

    //Test that equals objects have same hashcode
    @Test
    public void testEqualsHashCode() {
        assertEquals("Bar with two elems", barTwoChord, new Bar(ChordTest.chord1, ChordTest.chord1, Repetition.NORMAL));
        
        assertEquals("bar with two elems hashcode" , barTwoChord.hashCode(), 
                new Bar(ChordTest.chord1, ChordTest.chord1, Repetition.NORMAL).hashCode());
        
    }
            
    //Test toString
    @Test
    public void testToString() {
        assertEquals("barOneChord", "| z0 [^A3^A3] |", barOneChord.toString());
        assertEquals("barTwoNote","| ^A3 C'3 |", barTwoNote.toString());
        assertEquals("barChordNoteTuple","| [[^A3^A3]C5]  ^A3 (3C'3C'3^A3  |" , barChordNoteTupleLen17.toString());
        assertEquals("barThreeChord", "| [^A3^A3]  [^A3^A3] [^A3^A3]  |", barThreeChord.toString());
        assertEquals("barChordRestTuple", "| [[^A3^A3]C5]  z2 (3C'3C'3^A3  |", barChordRestTupleLen16.toString());
        assertEquals("barChordTwoRest","| [^A3^A3]  z2 z2  |", barChordTwoRest.toString());
        
    }
    
    //Test duration
    @Test
    public void testDuration(){
        assertEquals("One Chord", 3, barOneChord.duration());
        assertEquals("Two rests", 4, barTwoRest.duration());
        assertEquals("Chord two rests", 7, barChordTwoRest.duration());
        assertEquals("Chord note tuple", 17, barChordNoteTupleLen17.duration());
      
    }
    
    //Test repetition
    @Test
    public void testRepetition() {
        assertEquals("bar repetition",barOneChord.repetition(), Repetition.NORMAL);
        assertEquals("bar repetition", barOneNote.repetition(), Repetition.FIRST_ENDING);
        assertEquals("bar repetition", barTwoRest.repetition(), Repetition.REPEAT);
    }
    
    //Test play
    @Test
    public void testPlay() throws InterruptedException{
    	try{
    	   SequencePlayer player = new SequencePlayer(100, 12);
    	   barThreeNote.play(player, 0);
    	   player.play();
    	   Thread.sleep(1000);
    	} catch (MidiUnavailableException mue) {
            mue.printStackTrace();
        } catch (InvalidMidiDataException imde) {
            imde.printStackTrace();
        }
    }
}
