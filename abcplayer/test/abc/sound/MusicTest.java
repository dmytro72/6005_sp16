package abc.sound;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import javax.jws.Oneway;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import org.junit.Test;

/**
 * 
 * Tests for music class
 * @category no_didit
 */
public class MusicTest {
    // duration()
    //  Voices are same duration, different durations
   //  title()
    // composer()
    // key()
    // meter()
    // defaultLength()
    // index()
    //
    // hashCode()
    // equals()
    // toString()
    // # of voices: 1, >1
    //  
    
    public static final List<Voice> voices1 = Arrays.asList(VoiceTest.voice1Len8, VoiceTest.voice2, VoiceTest.voice3Len8);
    public static final List<Voice> voices2 = Arrays.asList(VoiceTest.voice1Len8, VoiceTest.voice3Len8);
    public static final List<Voice> voices3 = Arrays.asList(VoiceTest.voice1Len8);
  
    //Test that equals objects have same hashcode
    @Test
    public void testEqualsHashCode() throws MidiUnavailableException, InvalidMidiDataException {
        
        Music twoVoicesBothLen8 = new Music("Index2","Title2", "composer2",
                "C", "4/4", "1/8", 100, 12, voices2);
        
        assertEquals("Two musics are equal", twoVoicesBothLen8, new Music("Index2","Title2", "composer2",
                "C", "4/4", "1/8", 100, 12, voices2));
        
        assertEquals("musics hashcode" , twoVoicesBothLen8.hashCode(), 
                new Music("Index2","Title2", "composer2",
                        "C", "4/4", "1/8", 100, 12, voices2).hashCode());
    }
    
    //Test toString
    @Test
    public void testToString() throws MidiUnavailableException, InvalidMidiDataException{
        Music oneVoice = new Music("Index","Title", "composer", 
                "C", "4/4", "1/8", 100, 12, voices3);
        
        Music threeVoices = new Music("Index","Title", "composer", 
                "C", "4/4",  "1/8", 100, 12, voices1);
        
        String expected = "X: Index\n" + 
                        "T: Title\n" +
                        "C: composer\n" + 
                        "M: 4/4\n" + 
                        "L: 1/8\n" +
                        "K: C\n" +
                        " z0 [^A3^A3] [1 z0 C'3  z0 z2 ";
        

        assertEquals("one music to string", expected, oneVoice.toString());
        
        expected = "X: Index\n"+
                    "T: Title\n"+
                    "C: composer\n"+
                    "M: 4/4\n"+
                    "L: 1/8\n"+
                    "K: C\n"+
                    " z0 [^A3^A3] [1 z0 C'3  z0 z2 \n"+ 
                    " [[^A3^A3]C5]  z2 (3C'3C'3^A3   ^A3 (3C'3C'3^A3  z0 (3C'3C'3^A3 \n"+ 
                    " z0 [^A3^A3] [1 z0 C'3  z0 z2 ";
                            
        assertEquals("one music to string", expected, threeVoices.toString()); 
    }
    
    
    //Test duration
    @Test
    public void testDuration() throws MidiUnavailableException, InvalidMidiDataException{
        Music oneVoice = new Music("Index","Title", "composer", 
                "C", "4/4", "1/8", 100, 12, voices3);
        
        Music threeVoices = new Music("Index","Title", "composer", 
                "C", "4/4",  "1/8", 100, 12, voices1);
        
      assertEquals("music duration", oneVoice.duration() , 8);
      assertEquals("music duration", threeVoices.duration() , 37);
      
    }
    
    
    //Test title
    @Test
    public void testTitle() throws MidiUnavailableException, InvalidMidiDataException {
        Music oneVoice = new Music("Index","Title", "composer", 
                "C", "4/4", "1/8", 100, 12, voices3);
        
        assertEquals("Title", oneVoice.title());
    }
    
    //Test composer
    @Test
    public void testComposer() throws MidiUnavailableException, InvalidMidiDataException {
        Music oneVoice = new Music("Index","Title", "composer", 
                "C", "4/4", "1/8", 100, 12, voices3);
        
        assertEquals("composer", oneVoice.composer());
    }
    
    //Test key
    @Test
    public void testKey() throws MidiUnavailableException, InvalidMidiDataException {
        Music oneVoice = new Music("Index","Title", "composer", 
                "C", "4/4", "1/8", 100, 12, voices3);
        
        assertEquals("C", oneVoice.key());
    }
    
    //Test meter
    @Test
    public void testMeter() throws MidiUnavailableException, InvalidMidiDataException {
        Music oneVoice = new Music("Index","Title", "composer", 
                "C", "4/4", "1/8", 100, 12, voices3);
        
        assertEquals("4/4", oneVoice.meter());
    }
    
    //Test default Length
    @Test
    public void testDefaultLength() throws MidiUnavailableException, InvalidMidiDataException {
        Music oneVoice = new Music("Index","Title", "composer", 
                "C", "4/4", "1/8", 100, 12, voices3);
        
        assertEquals("1/8", oneVoice.defaultLength());
    }
    
    //Test index
    @Test
    public void testIndex () throws MidiUnavailableException, InvalidMidiDataException {
        Music oneVoice = new Music("Index","Title", "composer", 
                "C", "4/4", "1/8", 100, 12, voices3);
        
        assertEquals("Index", oneVoice.index());
    }
    
    //Test play
    @Test
    public void testPlay() throws InterruptedException{
        try{
            Music oneVoice = new Music("Index","Title", "composer", 
                    "C", "4/4", "1/8", 100, 12, voices3);
            
           oneVoice.play(0);
           
           Music threeVoices = new Music("Index","Title", "composer", 
                   "C", "4/4",  "1/8", 100, 12, voices1);
           
           threeVoices.play(0);
          
           Thread.sleep(1000);
        } catch (MidiUnavailableException mue) {
            mue.printStackTrace();
        } catch (InvalidMidiDataException imde) {
            imde.printStackTrace();
        }
    }
    
}
