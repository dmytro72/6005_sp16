package abc.sound;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import org.junit.Test;

/**
 * 
 * Tests for voice class
 *
 */
public class VoiceTest {
    //duration()
    //  number of bars is 1, >1
    //
    // hashCode()
    // equals()
    // toString()
    //  # of bars in voice: 1, >1
    //
    
    public static final List<Element> bars1 = Arrays.asList(BarTest.barOneChord, BarTest.barOneNote, BarTest.barOneRest);
    public static final List<Element> barsOneChord = Arrays.asList(BarTest.barOneChord);
    public static final List<Element> bars2 = Arrays.asList(BarTest.barChordRestTupleLen16, BarTest.barNoteTupleLen12, BarTest.barOneTupleLen9);
    public static final List<Element> bars3 = Arrays.asList(BarTest.barOneChord, BarTest.barOneNote, BarTest.barOneRest);
    
    public static final Voice voice1Len8 = new Voice(bars1);
    public static final Voice voiceOneChordLen3 = new Voice(barsOneChord);
    public static final Voice voiceSame = new Voice(bars1);
    public static final Voice voice2 = new Voice(bars2);
    public static final Voice voice3Len8 = new Voice(bars3);
    
    //Test that equals objects have same hashcode
    @Test
    public void testEqualsHashCode() {
        assertEquals("Voice with two elems", voice1Len8, new Voice(bars1));
        
        assertEquals("bar with two elems hashcode", voice1Len8.hashCode() , new Voice(bars1).hashCode());
        
    }
    
    @Test
    public void testToString(){
        assertEquals("Voice 1", " z0 [^A3^A3] [1 z0 C'3  z0 z2 ", voice1Len8.toString()); 
        assertEquals("Voice 1", " z0 [^A3^A3] ", voiceOneChordLen3.toString()); 
        assertEquals("Voice 2 ", " [[^A3^A3]C5]  z2 (3C'3C'3^A3   ^A3 (3C'3C'3^A3  z0 (3C'3C'3^A3 ", voice2.toString()); 
        assertEquals("Voice 3", " z0 [^A3^A3] [1 z0 C'3  z0 z2 ", voice3Len8.toString() ); 
    }
    
    
    //Test duration
    @Test
    public void testDuration(){
      assertEquals("Voice 1 duration", 8, voice1Len8.duration());
      assertEquals("Voice 1 duration", 3, voiceOneChordLen3.duration());
      assertEquals("Voice 2 duration", 37, voice2.duration());
      assertEquals("Voice 3 duration", 8, voice3Len8.duration());
      
    }
    
    
    //Test repetition
    @Test
    public void testRepetition() {
           assertEquals(Repetition.NORMAL, voice1Len8.repetition());
    }
    
    //Test play
    @Test
    public void testPlay() throws InterruptedException{
        try{
           SequencePlayer player = new SequencePlayer(100, 12);
           voice1Len8.play(player, 0);
           player.play();
           Thread.sleep(1000);
        } catch (MidiUnavailableException mue) {
            mue.printStackTrace();
        } catch (InvalidMidiDataException imde) {
            imde.printStackTrace();
        }
    }
}
