package abc.sound;

import static org.junit.Assert.*;
import org.junit.Test;


/**
 * 
 * Tests for Rest class
 *
 */
public class RestTest {
    // partitions for rest:
    // equals()
    //toString()
    // Duration is 0, >0
    //hashCode()
    // repetition()
    //
    
    public static final Rest zeroRest = new Rest(0);
    public static final Rest oneRest = new Rest(1);
    public static final Rest twoRest = new Rest(2);
    public static final Rest threeRest = new Rest(3);
    public static final Rest fourRest = new Rest(4);
    
     //Test that equals objects have same hashcode
     @Test
     public void testEqualsHashCode() throws InterruptedException{
       
         
         assertTrue("rests equal", zeroRest.equals(new Rest(0)));
         assertEquals("same hashcode", zeroRest.hashCode(), new Rest(0).hashCode());
        
     }
     
     //Test that equals objects have same hashcode
     @Test
     public void testRepetition() throws InterruptedException{
     
         assertEquals("rests equal", zeroRest.repetition(), Repetition.NORMAL);
         
     }
     
     //Test toString
     @Test
     public void testToString() throws InterruptedException{
            
         String expected = "z0";
         
         assertEquals("rest toString", expected, zeroRest.toString()); 
         
         Rest rest1 = new Rest(3);
         
         expected = "z3";
         
         assertEquals("rest toString", expected, rest1.toString()); 
     }
}
