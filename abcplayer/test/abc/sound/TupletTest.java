package abc.sound;

import static org.junit.Assert.assertEquals;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * Tests for Tuplet class
 *
 */
public class TupletTest {
    // hashCode()
    // equals()
    // toString()
        // # elements in tuplet: 
        //   2,3,4
        // # of notes in tuplet:
        //   0,1,2,3
        // # of chords in tuplet:
        //   0,1,2,3
    //repetition()
    //duration()
    //   # elements in tuplet: 
    //   2,3,4
    //   
    
    
    public static final Tuplet twoTupletOfChords = new Tuplet(2, ChordTest.chord1, ChordTest.chord2);
    
    public static final Tuplet buildThreeTupletOfChords = new Tuplet(3, ChordTest.chord1, ChordTest.chord2);
    public static final Tuplet threeTupletOfChords = new Tuplet(3, buildThreeTupletOfChords, ChordTest.chord2);
    
    public static final Tuplet buildFourTupletOfChords = new Tuplet(4, ChordTest.chord1, ChordTest.chord2);
    public static final Tuplet buildFourTupletOfChords2 = new Tuplet(4, buildFourTupletOfChords, ChordTest.chord2);
    public static final Tuplet fourTupletOfChords = new Tuplet(4, buildFourTupletOfChords2, ChordTest.chord6Len5);
    
    public static final Tuplet twoTupletOfNotes = new Tuplet(2, NoteTest.aSharp, NoteTest.bFlat);
    
    public static final Tuplet buildThreeTupletOfNotes = new Tuplet(3, NoteTest.highC, NoteTest.highC);
    public static final Tuplet threeTupletOfNotes = new Tuplet(3, buildThreeTupletOfNotes, NoteTest.aSharp);
    
    public static final Tuplet buildFourTupletOfNotes = new Tuplet(4, NoteTest.aSharp, NoteTest.bFlat);
    public static final Tuplet buildFourTupletOfNotes2 = new Tuplet(4, buildFourTupletOfNotes, NoteTest.highC);
    public static final Tuplet fourTupletOfNotes = new Tuplet(4, buildFourTupletOfNotes2, NoteTest.bFlat);
    
    public static final Tuplet twoTupletOfNotesChords = new Tuplet(2, NoteTest.aSharp, ChordTest.chord2);
    
    public static final Tuplet buildThreeTupletOfNotesChords = new Tuplet(3, NoteTest.highC, NoteTest.highC);
    public static final Tuplet threeTupletOfNotesChords = new Tuplet(3, buildThreeTupletOfNotes, ChordTest.chord3);
    
    public static final Tuplet buildFourTupletOfNotesChords = new Tuplet(4, NoteTest.aSharp, NoteTest.bFlat);
    public static final Tuplet buildFourTupletOfNotesChords2 = new Tuplet(4, buildFourTupletOfNotes, ChordTest.chord1);
    public static final Tuplet fourTupletOfNotesChords = new Tuplet(4, buildFourTupletOfNotes2, ChordTest.chord3);
    
    
    //Test that equals objects have same hashcode
    @Test
    public void testEqualsHashCode() {
        Tuplet tempLeft = new Tuplet(3, ChordTest.chord1, ChordTest.chord2);
        Tuplet tempTripletOfChords = new Tuplet(3, tempLeft, ChordTest.chord2);
        
        assertEquals("Tuplets equal", tempTripletOfChords, threeTupletOfChords);
        assertEquals("Chords hashcode equal", tempTripletOfChords.hashCode(), threeTupletOfChords.hashCode());
    }
            
            
    //Test toString
    @Test
    public void testToString(){
       assertEquals("Tuplet to string", threeTupletOfNotes.toString(), "(3C'3C'3^A3");
       System.out.println(fourTupletOfChords.toString());
       assertEquals("Tuplet to string", fourTupletOfChords.toString(), "(4[^A3^A3][[^A3^A3]C'3][[^A3^A3]C'3][[^A3^A3]C5]");
       assertEquals("Tuplet to string", threeTupletOfChords.toString(), "(3[^A3^A3][[^A3^A3]C'3][[^A3^A3]C'3]");
    }

    //Test duration
    @Test
    public void testDuration(){
        
        assertEquals("Duplet", 6, twoTupletOfChords.duration());
        assertEquals("Triplet", 9, threeTupletOfChords.duration());
        assertEquals("quadruplet", 14, fourTupletOfChords.duration());
        
        assertEquals("Duplet", 6, twoTupletOfNotes.duration());
        assertEquals("Triplet", 9, threeTupletOfNotes.duration());
        assertEquals("quadruplet", 12, fourTupletOfNotes.duration());
        
    }
    
    //Test repetition
    @Test
    public void testRepetition() {
        
        assertEquals("tuplet repetition", fourTupletOfChords.repetition(), Repetition.NORMAL);
        assertEquals("tuplet repetition", threeTupletOfChords.repetition(), Repetition.NORMAL);
        assertEquals("tuplet repetition", twoTupletOfChords.repetition(), Repetition.NORMAL);
    }
    
    //Test play
    @Test
    public void testPlay() throws InterruptedException{
    	try{
    	   SequencePlayer player = new SequencePlayer(140, 12);
    	   twoTupletOfNotesChords.play(player, 0);
    	   player.play();
    	   Thread.sleep(1000);
    	} catch (MidiUnavailableException mue) {
            mue.printStackTrace();
        } catch (InvalidMidiDataException imde) {
            imde.printStackTrace();
        }
    }
    
}
