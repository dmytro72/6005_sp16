/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package expressivo;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * Tests for the Expression abstract data type.
 */
public class ExpressionTest {

    // Testing strategy
    //   toString()
    //      - # of Consts: 0, 1, >1
    //      - # of Variables: 0, 1, >1
    //      - # of Plus expressions: 0, 1,
    //      - # of Times expressions: 0, 1,

    //   equals()
    //      - Consts — integer and double, double and double
    //      - Variables — case-sensitivity
    //      - Plus
    //      -Times
    //   hashCode()
    //     - Consts — integer and double, double and double
    //      - Variables — case-sensitivity, 
    //
    //      - # of Plus expressions: 0, 1,
    //               # of Consts: 0, 1, >1
    //      -        # of Variables: 0, 1, >1
     //              order of expressions different, same
    //      - # of Times expressions: 0, 1,
    //              # of Consts: 0, 1, >1
    //      -        # of Variables: 0, 1, >1
    //               order of expressions different, same
    //
    //  parse()
    //      - Constants — Integers and Doubles, with and without digits left of decimal pt, 
    //      - Variables: 0, 1 >1
    //       - # of Plus expressions: 0, 1,
    //               # of Consts: 0, 1,
    //      -        # of Variables: 0, 1
    //      - # of Times expressions: 0, 1,
    //              # of Consts: 0, 1, >1
    //      -        # of Variables: 0, 1, >1
    //      Invalid inputs throw exception
    //          - Negative constant
    //          - Empty variables
    //
    //  differentiate()
    //      - # of Consts: 0, 1, >1
    //      - # of Variables: 0, 1, >1
    //      - # of Plus expressions: 0, 1, 
    //      - # of Times expressions: 0, 1,
    //      - Differentiation variable
    //              - Present, not present in expression
    //
    //  simplify()
    //      - # of Consts: 0, 1, >1
    //      - # of Variables: 0, 1, >1
    //      - # of Plus expressions: 0, 1, 
    //      - # of Times expressions: 0, 1,
    //     - simplifies to constant expression 
    //      - environment contains 0, 1, >1 variable
    //
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    //Test toString on constant
    @Test
    public void testToStringConst(){
        Expression e1 = new Constant(1);
            
        assertEquals("Const is 1", e1.toString(), "1.0");
    }   
    
    //Test toString on variable
    @Test
    public void testToStringVar(){
        Expression e1 = new Variable("x");
        
        assertEquals("var is x", e1.toString(), "x");
    }
    
    //Test toString on sum of two constants
    @Test
    public void testToStringPlusTwoConst(){
        Expression e1 = new Plus(new Constant(3), new Constant(1));
        
        assertEquals("Plus constants", e1.toString(), "(3.0 + 1.0)");
    }

    //Test toString on sum of two variables
    @Test
    public void testToStringPlusTwoVars(){
        Expression e1 = new Plus(new Variable("y"), new Variable("x"));
        
        assertEquals("Plus variables", e1.toString(), "(y + x)");
    }
    
    //Test toString on product of constant sums
    @Test
    public void testToStringTimesofTwoPlusConst(){
        Expression e1 = new Times(new Plus(new Constant(3), new Constant(1)), new Plus(new Constant(2), new Constant(5)));
        
        assertEquals("Times of plus", e1.toString(), "((3.0 + 1.0) * (2.0 + 5.0))");
    }
    
    //Test toString on sum of constant products
    @Test
    public void testToStringPlusofTwoTimesConst(){
        Expression e1 = new Plus( new Times(new Constant(3), new Constant(1)), new Times(new Constant(2), new Constant(5)));
        
        assertEquals("Times of plus", e1.toString(), "((3.0 * 1.0) + (2.0 * 5.0))");
    }
    
    //Test toString on product of variables sums
    @Test
    public void testToStringTimesofTwoPlusVar(){
        Expression e1 = new Times(new Plus(new Variable("x"), new Variable("y")), new Plus(new Variable("t"), new Variable("z")));
        
        assertEquals("Times of plus", e1.toString(), "((x + y) * (t + z))");
    }
    
    //Test equals with two variables
    @Test
    public void testEqualsTwoVariables(){
        Expression e1 = new Variable("x");        
        Expression e2 = new Variable("y"); 
        Expression e3 = new Variable("x"); 
        
        assertFalse("e1 not equals e2 var", e1.equals(e2));
        
        assertTrue("e1 equals e3 var", e1.equals(e3));

    }
    
    //Test equals with two variables case sensitive
    @Test
    public void testEqualsTwoVariablesCase(){
        Expression e1 = new Variable("x");        
        Expression e2 = new Variable("y"); 
        Expression e3 = new Variable("X"); 
        
        assertFalse("e1 not equals e2 var", e1.equals(e2));
        
        assertFalse("e1 not equals e3 uppercase var", e1.equals(e3));
        
    }
    
    //Test equals with two constants
    @Test
    public void testEqualsTwoConstants(){
        Expression e1 = new Constant(1);        
        Expression e2 = new Constant(2); 
        Expression e3 = new Constant(2.0); 
        
        assertFalse("e1 not equals e2 const", e1.equals(e2));
        
        assertTrue("e2 equals e3 const", e2.equals(e3));

        
    }
    
    //Test equals with sum 
    @Test
    public void testNotEqualsPlusVariable(){
        Expression e1 = new Plus(new Constant(1), new Variable("x"));
        Expression e3 = new Plus(new Constant(1), new Variable("x"));
        Expression e2 = new Plus(new Variable("x"), new Constant(1));
        
        assertEquals("Times of plus", e1.toString(), "(1.0 + x)");
        assertEquals("Times of plus", e3.toString(), "(1.0 + x)");

        assertEquals("Times of plus", e2.toString(), "(x + 1.0)");
        
        assertFalse("Diff order so false", e1.equals(e2));
        assertTrue("Same order so false", e1.equals(e3));
    }
    
    //Test equals with products
    @Test
    public void testNotEqualsTimesVariable(){
        Expression e1 = new Times(new Constant(1), new Variable("x"));
        Expression e3 = new Times(new Constant(1), new Variable("x"));
        Expression e2 = new Times(new Variable("x"), new Constant(1));
        
        assertEquals("Times of plus", e1.toString(), "(1.0 * x)");
        assertEquals("Times of plus", e3.toString(), "(1.0 * x)");
        
        assertEquals("Times of plus", e2.toString(), "(x * 1.0)");
        
        assertFalse("Diff order so false", e1.equals(e2));
        assertTrue("Same order so false", e1.equals(e3));
    }
    
    //Test hashCode for Variables c
    @Test
    public void testHashCodeVariables(){
        Expression e1 = new Variable("x");        
        Expression e2 = new Variable("y"); 
        Expression e3 = new Variable("x"); 
        
        assertFalse("e1 not equals e2 var", e1.equals(e2));
        
        assertTrue("e1 equals e3 var", e1.equals(e3));
        
        assertFalse("e1 hashcode not equals e2 hashcode", e1.hashCode() == e2.hashCode());
        assertTrue("e1 hashcode equals e3 hashcode", e1.hashCode() == e3.hashCode());
       
    }
    
    //Test hashCode for Variables case sensitive
    @Test
    public void testHashcodeTwoVariablesCase(){
        Expression e1 = new Variable("x");        
        Expression e2 = new Variable("y"); 
        Expression e3 = new Variable("X"); 
        
        assertFalse("e1 not equals e2 var", e1.equals(e2));
        
        assertFalse("e1 not equals e3 uppercase var", e1.equals(e3));
        
        assertFalse("e1 hashcode not equals e2 hashcode", e1.hashCode() == e2.hashCode());
        assertFalse("e1 hashcode equals e3 hashcode", e1.hashCode() == e3.hashCode());
       
    }
    
    //Test hashCode for Constants
    @Test
    public void testHashCodeConstants(){
        Expression e1 = new Constant(1);        
        Expression e3 = new Constant(1);        
        Expression e2 = new Constant(2); 
        
        assertFalse("e1 not equals e2 const", e1.equals(e2));
        assertTrue("e1 equals e3 const", e1.equals(e3));
        assertTrue("e1 hashcode equals e3 hashcode", e1.hashCode() == e3.hashCode());

        
        assertFalse("e1 hashcode not equals e2 hashcode", e1.hashCode() == e2.hashCode());

    }
    
    //Test hashCode for sums
    @Test 
    public void testHashCodePlus(){
        Expression e1 = new Plus(new Constant(1), new Variable("x"));
        Expression e3 = new Plus(new Constant(1), new Variable("x"));
        Expression e2 = new Plus(new Variable("x"), new Constant(1));
        
        assertEquals("Times of plus", e1.toString(), "(1.0 + x)");
        assertEquals("Times of plus", e3.toString(), "(1.0 + x)");
        
        assertTrue("Same order so true", e1.equals(e3));
        
        assertEquals("hashcodes the same", e1.hashCode(), e2.hashCode());
        
    }
    
    //Test hadeCode with products
    @Test
    public void testHashCodeTimes(){
        Expression e1 = new Times(new Constant(1), new Variable("x"));
        Expression e3 = new Times(new Constant(1), new Variable("x"));
        Expression e2 = new Times(new Variable("x"), new Constant(1));
        
        assertEquals("Times of plus", e1.toString(), "(1.0 * x)");
        assertEquals("Times of plus", e3.toString(), "(1.0 * x)");
        
        assertEquals("Times of plus", e2.toString(), "(x * 1.0)");
        
        assertFalse("Diff order so false", e1.equals(e2));
        assertTrue("Same order so false", e1.equals(e3));
        
        assertEquals("hashcodes the same", e1.hashCode(), e3.hashCode());

    }
    
    //Test parsing a constant
    @Test
    public void testParseConstant(){
        
        String s1 = "1";
        String s2 = "1.0";
        String s3 = ".1";
        
        assertEquals("Parse constant 1", Expression.parse(s1).toString(), "1.0");
        assertEquals("Parse constant 2", Expression.parse(s2).toString(), "1.0");
        assertEquals("Parse constant 3", Expression.parse(s3).toString(), "0.1");
        
    }

    //Test parsing a var
    @Test
    public void testParseVariable(){
        
        String s1 = "1";
        String s2 = "1.0";
        String s3 = ".1";
        
        assertEquals("Parse constant 1", Expression.parse(s1).toString(), "1.0");
        assertEquals("Parse constant 2", Expression.parse(s2).toString(), "1.0");
        assertEquals("Parse constant 3", Expression.parse(s3).toString(), "0.1");
        
    }
    
    //Test parse on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testParseIllegal1(){
        
        String s1 = "3 )";
        
        assertEquals("Parse constant 1", Expression.parse(s1).toString(), "1.0");
       
    }
    
    //Test parse on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testParseIllegal2(){
        
       String s2 = "x 3";
        
        assertEquals("Parse constant 2", Expression.parse(s2).toString(), "1.0");
        
    }
    
    
    //Test parse on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testParseIllegal3(){
        
        String s3 = "3 *";
        
        assertEquals("Parse constant 3", Expression.parse(s3).toString(), "0.1");
        
    }
    
    //Test parsing a sum of products
    @Test
    public void testParsePlusofTwoTimesConst(){
        String s1 = "(3*1)+(2*5)";
        
        assertEquals("Parse constant 3", Expression.parse(s1).toString(), "((3.0 * 1.0) + (2.0 * 5.0))");
        
    }
    
    //Test parsing a product of sums
    @Test
    public void testParseTimesofTwoPlusVar(){
        String s1 = "(3+1)*(2+5)";
        
        assertEquals("Parse constant 3", Expression.parse(s1).toString(), "((3.0 + 1.0) * (2.0 + 5.0))");
        
    }
    
    //Test differentiate on variables
    @Test
    public void testDifferentiateVariable(){
        Expression e1 = new Variable("y");
        Expression e2 = new Variable("x");
        
        assertEquals("Variable is 0", e1.differentiate("y").toString() , "1.0");
        assertEquals("Diff is 0.0", e2.differentiate("y").toString() , "0.0");
    }
    
    //Test differentiate on constants
    @Test
    public void testDifferentiateConstants(){
        Expression e1 = new Constant(2.0);
        
        assertEquals("Diff constant 1", e1.differentiate("y").toString() , "0.0");
    }
    
    //Test differentiate on sums
    @Test
    public void testDifferentiateSum(){
        Expression e1 = new Plus(new Variable("x"), new Variable("x"));
        
        assertEquals("Diff sum 1", e1.differentiate("x").toString() , "(1.0 + 1.0)");
    }
    
    //Test differentiate on products
    @Test
    public void testDifferentiateProduct(){
        Expression e1 = new Times(new Variable("x"), new Variable("x"));
        
        Expression e3 = new Times(new Variable("x"), new Constant(2));
        
        assertEquals("product diff 1", e3.differentiate("x").toString() , "((1.0 * 2.0) + (0.0 * x))");
        assertEquals("product diff 1", e1.differentiate("x").toString() , "((1.0 * x) + (1.0 * x))");
    }
    
    //Test simplify with one variable in environment
    @Test
    public void testSimplifyOneEnvVar(){
        Expression e1 = new Variable("x");
        Map<String, Double> env = new HashMap<>();
        env.put("x", 2.0);
        
        Expression e2 = new Times(new Variable("y"), new Variable("x"));
        env = new HashMap<>();
        env.put("x", 2.0);

        assertEquals("Simplify constant 1", e2.simplify(env).toString() , "(y * 2.0)");
        assertEquals("Simplify constant 1", e1.simplify(env).toString() , "2.0");
        
    }

    //Test simplify with two variable in environment
    @Test
    public void testSimplifyTwoEnvVar(){
        Expression e2 = new Times(new Variable("y"), new Variable("x"));
        Map<String, Double> env = new HashMap<>();
        env.put("x", 2.0);
        env.put("y", 3.0);

        assertEquals("Simplify 2 env vars", e2.simplify(env).toString() , "6.0");
    }

    //Test simplify with no variable in environment
    @Test
    public void testSimplifyZeroEnvVar(){
        Expression e1 = new Constant(2.0);
        Map<String, Double> env = new HashMap<>();
        
        assertEquals("Simplify 0 env var", e1.simplify(env).toString() , "2.0");
        
    }
    
    //Test simplify with constant expression

    @Test
    public void testSimplifyConstantExpr(){
        Expression e1 = new Plus(new Constant(2.0), new Constant(3.0));
        Map<String, Double> env = new HashMap<>();

        
        assertEquals("Simplify sum 1", e1.simplify(env).toString() , "5.0");
        
    }
    
    //Test simplify with constant expression from product
    @Test
    public void testSimplifyConstantExprTimes(){
        Expression e1 = new Times(new Constant(2.0), new Constant(3.0));
        Map<String, Double> env = new HashMap<>();

        
        assertEquals("Simplify times 1", e1.simplify(env).toString() , "6.0");
        
    }
    
    //Test simplify with constant expression from environment variable interpolation
    @Test
    public void testSimplifyConstantExpr2(){
        Expression e1 = new Plus(new Variable("x"), new Variable("y"));
        Expression e2 = new Times(new Variable("y"), new Variable("x"));
        Map<String, Double> env = new HashMap<>();

        env.put("x", 2.0);
        env.put("y", 3.0);
        
        assertEquals("Diff sum 1", e1.simplify(env).toString() , "5.0");
        assertEquals("Diff sum 1", e2.simplify(env).toString() , "6.0");
        
    }
}
