/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package expressivo;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/**
 * Tests for the static methods of Commands.
 */
public class CommandsTest {

    // Testing strategy
    //   differentiate()
    //      - Illegal strings throw exception
    //      - # of Consts: 0, 1, >1
    //      - # of Variables: 0, 1, >1
    //      - # of Plus Strings: 0, 1 
    //      - # of Times Strings: 0, 1 
    //      - Differentiation variable
    //              - Present, not present in String
    //
    //
    //
   //   simplify()  
    //      - Illegal strings throw exception
    //     - # of Consts: 0, 1, >1
    //      - # of Variables: 0, 1, >1
    //      - # of Plus Strings: 0, 1
    //      - # of Times Strings: 0, 1 
    //      - simplifies to constant String 
    //      - environment contains 0, 1, >1 variable
    //     

    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    //Test differentiate on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testDifferentiateIllegal1(){
        
        String s1 = "3 )";
        
        assertEquals("Parse constant 1", Commands.differentiate(s1, "x").toString(), "1.0");
       
    }
    
    //Test differentiate on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testDifferentiateIllegal2(){
        
       String s2 = "x 3";
        
       assertEquals("Parse constant 1", Commands.differentiate(s2, "x").toString(), "1.0");
        
    }
    
    //Test differentiate on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testDifferentiateIllegal3(){
        
        String s3 = "3 *";
        
        assertEquals("Parse constant 1", Commands.differentiate(s3, "x").toString(), "1.0");
        
    }
    
    //Test differentiate on variable
    @Test
    public void testDifferentiateVariable(){
        String s1 = "y";
        String s2 = "x";
        
        assertEquals("Variable is 0", Commands.differentiate(s1, "y") , "1.0");
        assertEquals("Diff is 0.0", Commands.differentiate(s2, "y") , "0.0");

    }
    
    
    //Test differentiate on constants
    @Test
    public void testDifferentiateConstants(){
        String e1 = "2.0";
        
        assertEquals("Diff constant 1", Commands.differentiate(e1, "y") , "0.0");
        
    }
    
    //Test differentiate on sums
    @Test
    public void testDifferentiateSum(){
        String e1 = "x + x"; 
        
        assertEquals("Diff sum 1", Commands.differentiate(e1, "x") , "(1.0 + 1.0)");
        
    }
    
    //Test differentiate on products
    @Test
    public void testDifferentiateProduct(){
        String e1 = "x*x";
        
        String e3 = "x*2";
        
        assertEquals("product diff 1", Commands.differentiate(e3, "x") , "((1.0 * 2.0) + (0.0 * x))");
        assertEquals("product diff 1", Commands.differentiate(e1, "x") , "((1.0 * x) + (1.0 * x))");
    }
    
    //Test simplify with one variable in environment
    @Test
    public void testSimplifyOneEnvVar(){
        String e1 = "x";
        Map<String, Double> env = new HashMap<>();
        env.put("x", 2.0);
        
        String e2 = "y*x";
        env = new HashMap<>();
        env.put("x", 2.0);

        assertEquals("Simplify constant 1", Commands.simplify(e2, env) , "(y * 2.0)");
        assertEquals("Simplify constant 1", Commands.simplify(e1, env) , "2.0");
        
    }
    
    //Test simplify with two variables in environment
    @Test
    public void testSimplifyTwoEnvVar(){
        String e2 = "y*x"; 
        Map<String, Double> env = new HashMap<>();
        env.put("x", 2.0);
        env.put("y", 3.0);

        assertEquals("Simplify 2 env vars", Commands.simplify(e2, env) , "6.0");
        
    }
    
    //Test simplify with no variable in environment
    @Test
    public void testSimplifyZeroEnvVar(){
        String e1 = "2.0";
        String e2 = "x";
        Map<String, Double> env = new HashMap<>();
        
        assertEquals("Simplify 0 env var", Commands.simplify(e1, env) , "2.0");
        assertEquals("Simplify 0 env var", Commands.simplify(e2, env) , "x");
        
    }
    
    //Test simplify with constant expression
    @Test
    public void testSimplifyConstantExpr(){
        String e1 ="2.0 + 3.0";
        Map<String, Double> env = new HashMap<>();
        
        assertEquals("Simplify sum 1", Commands.simplify(e1, env) , "5.0");
        
    }
    
    //Test simplify with constant expression
    @Test
    public void testSimplifyConstantExprTimes(){
        String e1 ="2.0*3.0";
        Map<String, Double> env = new HashMap<>();
        
        assertEquals("Simplify times 1", Commands.simplify(e1, env) , "6.0");
        
    }
    
    //Test simplify with constant expression after interpolating environment variables
    @Test
    public void testSimplifyConstantExpr2(){
        String e1 = "x+y";
        String e2 = "x*y"; 
        String e3 = "y*x"; 
        Map<String, Double> env = new HashMap<>();

        env.put("x", 2.0);
        env.put("y", 3.0);
        
        assertEquals("Diff sum 1", Commands.simplify(e1, env) , "5.0");
        assertEquals("Diff sum 1", Commands.simplify(e2, env) , "6.0");
        assertEquals("Diff sum 1", Commands.simplify(e3, env) , "6.0");
        
    }
    
    //Test simplify on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testSimplifyIllegal1(){
        
        String s1 = "3 )";
        Map<String, Double> env = new HashMap<>();
        
        assertEquals("Parse constant 1", Commands.simplify(s1, env).toString(), "1.0");
    }
    
    //Test simplify on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testParseIllegal2(){
        
       String s2 = "x 3";
       Map<String, Double> env = new HashMap<>();

        
       assertEquals("Parse constant 1", Commands.simplify(s2, env).toString(), "1.0");
    }
    
    //Test simplify on an illegal expression string
    @Test(expected=IllegalArgumentException.class)
    public void testSimplifyIllegal3(){
        
        String s3 = "3 *";
        Map<String, Double> env = new HashMap<>();

        
        assertEquals("Parse constant 1", Commands.simplify(s3, env).toString(), "1.0");
    }
}
