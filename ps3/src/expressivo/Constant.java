package expressivo;

import java.text.DecimalFormat;
import java.util.Map;

/**
 *  An immutable data type that represents a mathematical constant, an integer or floating-point number
 * 
 *
 */
class Constant implements Expression{
    private final Double value;
    
    // Abstraction function:
    //    Represents a nonnegative mathematical constant with value value
    
    // Representation invariant:
    //   value is nonnegative and not null
    
    // Safety from rep exposure:
    //   value field is final, equals(), toString(), and hashCode() do not modify fields
    
    private void checkRep(){
        assert value != null;
        assert value >= 0;
    }
    
    /**
     * Constructor for a constant expression, takes in a double
     * @param constant, a Double
     */
    public Constant(Double constant) {
        this.value = constant;
        checkRep();
    }

    /**
     * Constructor for a constant expression, takes in an integer
     * @param constant, an Integer
     */
    public Constant(Integer constant) {
        this.value = new Double(constant);
        checkRep();
    }
    
    @Override
    public Expression differentiate(String variable){
        return new Constant(0);
    }
    
    @Override
    public Expression simplify(Map<String, Double> env){
        return new Constant(value);
    }
    
    @Override
    public Double getValue(){
        return value;
    }
    
    @Override
    public boolean isNumber(){
        return true;
    }

    @Override 
    public String toString(){
        DecimalFormat df = new DecimalFormat("0.0"); 
        df.setMaximumFractionDigits(200);
        return df.format(value);
    }
    
    @Override
    public int hashCode(){
        return value.intValue();
    }
    
    @Override
    public boolean equals(Object thatObject){
        if (!(thatObject instanceof Constant)) return false;
        Constant that = (Constant) thatObject;
        
        checkRep();
        
        return this.value.equals(that.value);
        
    }
}


