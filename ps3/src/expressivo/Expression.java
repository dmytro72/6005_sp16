/* Copyright (c) 2015-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package expressivo;


import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import expressivo.parser.ExpressionLexer;
import expressivo.parser.ExpressionParser;

/**
 * An immutable data type representing a polynomial expression of:
 *   + and *
 *   nonnegative integers and floating-point numbers
 *   variables (case-sensitive nonempty strings of letters)
 * 
 * <p>PS3 instructions: this is a required ADT interface.
 * You MUST NOT change its name or package or the names or type signatures of existing methods.
 * You may, however, add additional methods, or strengthen the specs of existing methods.
 * Declare concrete variants of Expression in their own Java source files.
 */
public interface Expression {
    
    // Datatype definition
    //   Expression = Variable(n:double) + Constant(s:string) + 
    //                        + Plus(left:Expression, right:Expression)
    //                        + Times(left:Expression, right:Expression)
    
    /**
     * Parse an expression.
     * @param input expression to parse, as defined in the PS3 handout.
     * @return expression AST for the input
     * @throws IllegalArgumentException if the expression is invalid
     */
    public static Expression parse(String input) throws IllegalArgumentException{
            
        try{
            CharStream stream = new ANTLRInputStream(input);
            
            ExpressionLexer lexer = new ExpressionLexer(stream);
            lexer.reportErrorsAsExceptions();
            
            TokenStream tokens = new CommonTokenStream(lexer);
            
            ExpressionParser parser = new ExpressionParser(tokens);
            parser.reportErrorsAsExceptions();
            ParseTree tree = parser.root();
            
            System.out.println(tree.toStringTree());
            
            MakeExpression makeExpr = new MakeExpression();
            
            ParseTreeWalker walker= new ParseTreeWalker();
            walker.walk(makeExpr, tree);
            
            return makeExpr.getExpression();
            }
            catch (Exception e){
                throw new IllegalArgumentException();
            }
    }
    
    /**
     * Takes partial derivatives of an expression wrt some variable variable
     * @param variable, variable wrt differentiate 
     * @return an expression that is the derivative of expression wrt variable
     */
    public Expression differentiate(String variable);
    
    /**
     * Simplify an expression, substituting environment variables, and simplifying constant expressions to 
     * constants
     * @param env, an environment of mappings of strings to doubles, can be empty
     * @return Expression in most simplified form, must be a Constant if 
     * substituted polynomial is a constant expression
     * 
     */
    public Expression simplify(Map<String, Double> env);
    
    /**
     * Checks if expression is a constant expression
     * @return true iff Expression is a constant expression
     */
    public boolean isNumber();
    
    /**
     * Returns constant that the expression computes to 
     * Precondition: expression must be a constant expression
     * @return Integer that is the constant that this expression computes to
     *  -1 if expression is not a constant expression
     */
    public Double getValue();
    
    /**
     * @return a string representation of Expression, such that 
     * for all e:Expression, e.equals(Expression.parse(e.toString()))
     */
    @Override 
    public String toString();

    /**
     * @param thatObject any object
     * @return true if and only if this and thatObject are structurally equal
     * Expressions, as defined in the PS3 handout.
     */
    @Override
    public boolean equals(Object thatObject);
    
    /**
     * @return hash code value consistent with the equals() definition of structural
     * equality, such that for all e1,e2:Expression, 
     *     e1.equals(e2) implies e1.hashCode() == e2.hashCode()
     */
    @Override
    public int hashCode();
    
    
}












