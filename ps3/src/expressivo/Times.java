package expressivo;

import java.util.Map;

/**
 * 
 * An immutable recursive data type that represents the sum of two polynomial expressions
 * @param e1, left Expression
 * @param e2, right Expression
 */
class Times implements Expression{
    private final Expression e1;
    private final Expression e2;
    
    // Abstraction function:
    //    Represents a product of two expressions e1 and e2
    
    // Representation invariant:
    //   Expressions e1 and e2 are not null
    
    // Safety from rep exposure:
    //   Expressions e1 and e2 are final, equals(), toString(), and hashCode() do not modify fields
    
    
    private void checkRep(){
        assert e1 != null;
        assert e2 != null;
        
    }
    
    /**
     * Constructor for a product of expressions
     * @param e1, left Expression
     * @param e2, right Expression
     */
    public Times(Expression e1, Expression e2){
        this.e1 = e1;
        this.e2 = e2;
        checkRep();
    }
    
    @Override
    public Expression differentiate(String variable){
        return new Plus(new Times(e1.differentiate(variable), e2), new Times(e2.differentiate(variable), e1));

    }

    @Override
    public Expression simplify(Map<String, Double> env){
        Expression leftExpr = e1.simplify(env);
        Expression rightExpr = e2.simplify(env);
        
        if (leftExpr.isNumber() && rightExpr.isNumber()){
            return new Constant(leftExpr.getValue() * rightExpr.getValue());
        }
        
        else {
            return new Times(leftExpr, rightExpr);
        }
    }
    

    @Override
    public Double getValue(){
        return e1.getValue() * e2.getValue();
    }
    
    @Override
    public boolean isNumber(){
        if (e1.isNumber() && e2.isNumber()){
            return true;
        }
        
        return false;
    }

    
    @Override 
    public String toString(){
        checkRep();

        return "(" +  e1.toString() + " * " + e2.toString() + ")";
    }
    
    
    /**
     * Returns e1
     */
    public Expression getE1() {
        return e1;
    }

    /**
     * Returns e2
     */
    public Expression getE2() {
        return e2;
    }
    
    @Override
    public int hashCode(){
        checkRep();

        return e1.hashCode() + e2.hashCode();
    }
    
    @Override
    public boolean equals(Object thatObject){
        if (!(thatObject instanceof Times)) return false;
        Times that = (Times) thatObject;
        
        checkRep();
        
        return this.e1.equals(that.e1) && this.e2.equals(that.e2);
    }
    
    
   
}