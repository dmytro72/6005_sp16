package expressivo;

import java.util.Map;

/**
 * 
 * An immutable data type that represents a mathematical variable with name of case-sensitive non-empty sequence of letters
 *
 */
class Variable implements Expression{
    private final String var;
    
    // Abstraction function:
    //    Represents a mathematical variable named by a string var
    
    // Representation invariant:
    //   var is non-empty sequence of letters, cannot have spaces
    
    // Safety from rep exposure:
    //   Expressions e1 and e2 are final, equals(), toString(), and hashCode() do not modify fields 
    
    private void checkRep(){
        assert !var.equals("");
        assert var.matches("[a-zA-Z]+");
    }
    
    /**
     * Constructor for a single-variable expression 
     * @param var, name of variable
     */
    public Variable(String var){
          this.var = var;
          checkRep();
    }
    
    @Override
    public Expression differentiate(String variable){
        if (var.equals(variable)){
            return new Constant(1);
        }
        return new Constant(0);
    }
    
    @Override
    public Expression simplify(Map<String, Double> env){
        if (env.containsKey(var)) {
            return new Constant(env.get(var));   
        }
        
        return new Variable(var);
    }
    
    @Override
    public boolean isNumber(){
        return false;
    }
    

    @Override
    public Double getValue(){
        return new Double(-1);
    }

        
    @Override 
    public String toString(){
        return var;
    }   
    
    @Override
    public int hashCode(){
        return var.hashCode();
    }
    
    @Override
    public boolean equals(Object thatObject){
        if (!(thatObject instanceof Variable)) return false;
        Variable that = (Variable) thatObject;
        
        checkRep();
        
        return this.var.equals(that.var);
    }
}

