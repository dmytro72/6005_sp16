import java.util.*;

public class Racing {

    private static final int NUMBER_OF_THREADS = 2;
    private static final int NUMBER_OF_OPERATIONS = 10;
    
    public static void main(String[] args) {
        // TODO: choose one of these data structures
        //final Set<Integer> set = new HashSet<>();
        final List<Integer> list = new ArrayList<>();
        //final List<Integer> list = new LinkedList<>();
        //final Map<Integer,Integer> map = new HashMap<>();
        
        System.out.println(NUMBER_OF_THREADS
                            + " threads do "
                            + NUMBER_OF_OPERATIONS
                            + " mutations each...");
        
        for (int i = 0; i < NUMBER_OF_THREADS; ++i) {
            final int threadNumber = i; // only final local variables can be seen by inner classes
                                        // like the Runnable below
            Thread t = new Thread(new Runnable() {
                public void run() {
                    System.out.println("thread " + threadNumber + " is starting work");
                    
                    for (int j = 0; j < NUMBER_OF_OPERATIONS; ++j) {
                        int value = j*NUMBER_OF_THREADS + threadNumber;
                        // System.out.println(value);  // uncomment this to mask the race condition
                        
                        list.add(value);
                    }
                    
                    System.out.println("thread " + threadNumber + " is done");
                    
                    System.out.println(list.size());
                    //       to see if it's correct
                }
            });
            t.start(); // don't forget to start the thread!
        }
    }
}
