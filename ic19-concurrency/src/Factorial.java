import java.math.BigInteger;

public class Factorial implements Runnable{
    
    private final int n;
    
    /*
     * 
     * Constructor
     */
    public Factorial(int n){
        this.n = n;
    }
    
    /**
     * Computes n! and prints it on standard output.
     * @param n must be >= 0
     */
//    private static void compute(final int n) {
//        BigInteger result = new BigInteger("1");
//        for (int i = 1; i <= n; ++i) {
//            System.out.println("working on fact " + n);
//            result = result.multiply(new BigInteger(String.valueOf(i)));
//        }
//        System.out.println("fact(" + n + ") = " + result);
//    }
    
    @Override
    public void run(){
        BigInteger result = new BigInteger("1");
        for (int i = 1; i <= n; ++i) {
            System.out.println("working on fact " + n);
            result = result.multiply(new BigInteger(String.valueOf(i)));
        }
        System.out.println("fact(" + n + ") = " + result);
    }
    
    public static void main(String[] args) {
        (new Thread(new Factorial(99))).start();
        (new Thread(new Factorial(100))).start();
    }
    
//    public void run(){
//        computeFact(99);
//        computeFact(100);
//    }
    
}
