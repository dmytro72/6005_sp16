package intexpr;

class Times implements IntegerExpression {
    // TODO: fields
    
    // Abstraction function
    //    TODO
    // Rep invariant
    //    TODO
    // Safety from rep exposure
    //    TODO
    
    /** Make a Times which is the product of left and right. */
    public Times(IntegerExpression left, IntegerExpression right) {
        throw new RuntimeException("Times constructor unimplemented");
    }
    
    @Override public int value() {
        throw new RuntimeException("value() unimplemented");
    }
    
    @Override public String toString() {
        throw new RuntimeException("toString() unimplemented");
    }
}
