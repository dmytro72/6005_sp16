package intexpr;

import org.antlr.v4.gui.Trees;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import intexpr.parser.IntegerExpressionLexer;
import intexpr.parser.IntegerExpressionParser;

public class Main {
 
    public static void main(String[] args) {
        String input = "54+(2+89)";
        IntegerExpression expr = parse(input);
        int value = expr.value();
        System.out.println(input + "=" + expr + "=" + value);
    }

    /**
     * Parse a string into an integer arithmetic expression, displaying various
     * debugging output.
     */
    public static IntegerExpression parse(String string) {
       // Create a stream of characters from the string
       CharStream stream = new ANTLRInputStream(string);

       // Make a parser
       IntegerExpressionParser parser = makeParser(stream);
       
       // Generate the parse tree using the starter rule.
       // root is the starter rule for this grammar.
       // Other grammars may have different names for the starter rule.
       ParseTree tree = parser.root();
       
       // *** Debugging option #1: print the tree to the console
       System.err.println(tree.toStringTree(parser));

       // *** Debugging option #2: show the tree in a window
       Trees.inspect(tree, parser);
       
       // *** Debugging option #3: walk the tree with a listener
       new ParseTreeWalker().walk(new ListenerPrintEverything(), tree);
       
       ListenerMakeIntegerExpression exprMaker = new ListenerMakeIntegerExpression();
       new ParseTreeWalker().walk(exprMaker, tree);
       return exprMaker.getExpression();
   }

    
    /**
     * Make a parser that is ready to parse a stream of characters.
     * To start parsing, the client should call a method on the returned parser
     * corresponding to the start rule of the grammar, e.g. parser.root() or
     * whatever it happens to be.
     * During parsing, if the parser encounters a syntax error, it will throw a
     * ParseCancellationException.
     * @param stream stream of characters
     * @return a parser that is ready to parse the stream
     */
    private static IntegerExpressionParser makeParser(CharStream stream) {
        // Make a lexer.  This converts the stream of characters into a 
        // stream of tokens.  A token is a character group, like "<i>"
        // or "</i>".  Note that this doesn't start reading the character stream yet,
        // it just sets up the lexer to read it.
        IntegerExpressionLexer lexer = new IntegerExpressionLexer(stream);
        lexer.reportErrorsAsExceptions();
        TokenStream tokens = new CommonTokenStream(lexer);
        
        // Make a parser whose input comes from the token stream produced by the lexer.
        IntegerExpressionParser parser = new IntegerExpressionParser(tokens);
        parser.reportErrorsAsExceptions();
        
        return parser;
    }
}
