
public class PowerLeft {
    /**
     * @param n
     * @param k >= 0
     * @return n^k
     */
    public static long power(int n, int k) {
        System.out.println("power(" + n + "," + k + ") = ?");
        long result;
        
        if(k>0){
            result = n * power(n, k-1);
        }
        else{
            result = 1;
        }
        
        System.out.println("power(" + n + "," + k + ") = " + result);
        return result;
    }
    
    public static void main(String[] args) {
        System.out.println("in PowerLeft");
        power(3, 20);
    }
}
