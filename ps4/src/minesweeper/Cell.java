package minesweeper;

/**
 * A mutable data type that represents a cell on a minesweeper board, with states of touched, dug, flagged
 *and containing a "bomb", as described in PS5 
 * 
 *
 */
public class Cell {
    //Abstraction function: 
    // Represents a minesweeper cell that contains a bomb iff isBombed, has been touched iff isTouched
    //has been dug iff isDug, has been flagged iff isFlagged
    //
    //rep invariant:
    // true
    //rep exposure:
    // isTouched, isDug, isBomb, and isFlagged are private variables
    //
    // Thread Safety:
    //  Threads must go through Board to modify/retrieve Cell fields. All methods in board are synchronized,
    // which prevents race condition from concurrent access to a Cell. Thus, Cell is threadsafe.
    //
    
    private boolean isTouched;
    private boolean isDug;
    private boolean isBomb;
    private boolean isFlagged;
    
    /**
     * 
     */
    public Cell() {
        isBomb = false;
        isDug = false;
        isTouched = false;
        isFlagged = false;
        
        checkRep();
    }
    
    private void checkRep(){
        assert true;
    }


    /**
     * Reports whether cell is touched
     * @return isTouched, true if cell is touched
     */
    public boolean isTouched() {
        return isTouched;
    }

    /**
     * Set cell to be touched
     * @param isTouched
     */
    public void setTouched(boolean isTouched) {
        this.isTouched = isTouched;
    }

    /**
     * Reports whether cell is dug
     * @return isDug, true if cell is dug
     */
    public boolean isDug() {
        return isDug;
    }

    
    /**
     * Set cell to be dug
     * @param isDug
     */
    public void setDug(boolean isDug) {
        this.isDug = isDug;
    }

    /**
     * Reports whether cell has bomb
     * @return true if cell has bomb
     */
    public boolean isBomb() {
        return isBomb;
    }

    /**
     * Set cell to have a bomb
     * @param isBomb
     */
    public void setBomb(boolean isBomb) {
        this.isBomb = isBomb;
    }

    /**
     * Reports whether cell is flagged
     * @return true if cell is flagged
     */
    public boolean isFlagged() {
        return isFlagged;
    }

    /**
     * Set cell to flagged
     * @param isFlagged
     */
    public void setFlagged(boolean isFlagged) {
        this.isFlagged = isFlagged;
    }

    /**
     * Touches and digs the cell
     */
    public void digCell(){
       isTouched = true;
       isDug = true;
    }
    
}
