/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package minesweeper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * A mutable data type representing a minesweeper board with # of columns width and # of rows height.
 */
public class Board {
    
    //Abstraction function: 
    // Represent a minesweeper board with # of columns width and # of rows height
    //
    //Rep invariant:
    // width > 0 and height > 0, width = # of columns of board, height = # of rows of board
    //
    //Rep exposure: 
    // board, width, and height are private and final variables. 
    // No methods return access to individual Cell elements in board
    //
    //Thread safety:
    // All methods are synchronized, which prevents race conditions from multiple threads entering a method
    // and updating a cell at the same time
    // One lock synchronizes all methods, because there is only one board object and no board object references another board. 
    //Thus, deadlock cannot occur and progress will always be made.
    //
    
    
    private final Cell[][] board;
    private final int width;
    private final int height;
    private static final double BOMBCHANCE = .25; 

    
    /**
     * Constructor for minesweeper Board. 
     * 
     * @param width, positive int
     * @param height, positive int
     */
    public Board(int width, int height){
        if (width <= 0 || height <= 0){
            throw new RuntimeException("Bad format"); 
        }
       
        this.board = new Cell[width][height];
        this.width = width;
        this.height = height;
        
        for (int i = 0; i < width; i++){
            for (int j = 0; j < height; j++){
                board[i][j] = new Cell();
                if (Math.random() < BOMBCHANCE){
                    board[i][j].setBomb(true);
                }
            }
        }
        
        checkRep();
    }
    
    
    /**
     * Constructor for minesweeper Board, fills the board from a file
     * @param file
     */
    public Board(File file) throws IOException{
        List<String> lines = Files.readAllLines(file.toPath());
        
        String firstline = lines.get(0);
        String[] splitLine = firstline.split(" ");
        
        if (splitLine.length != 2){
            throw new RuntimeException("Bad format"); 
        }
        else if (!splitLine[0].matches("[0-9]+") || !splitLine[1].matches("[0-9]+")){
            throw new RuntimeException("Bad format"); 
        }
        
        int width = Integer.parseInt(splitLine[0]);
        int height = Integer.parseInt(splitLine[1]);
        
        if (height != lines.size()-1){
            throw new RuntimeException("Bad format"); 
        }
 
        this.board = new Cell[width][height];
        this.width = width;
        this.height = height;
        
        //fill up board with values from file
        for (int i = 1; i < lines.size(); i++){
            String[] line = lines.get(i).split(" ");
            
            if (line.length != width){
                throw new RuntimeException("Bad format"); 
            }
            
            for (int j = 0; j < width; j++){
                board[j][i-1] = new Cell();
                if (line[j].equals("1")){
                    board[j][i-1].setBomb(true);
                }
            }
        }
        
        checkRep();
    }
    
    private void checkRep(){
        assert width > 0;
        assert height > 0;
        assert width == board.length;
    }
    
    
    /**
     * Counts number of neighboring cells with bombs
     * @param x
     * @param y
     * @return count, int number of neighboring cells with bombs
     */
    private synchronized int countNeighborBombs(int x, int y){
        int count = 0;
        int[][] coords = {{x-1, y}, {x+1, y}, {x, y-1}, {x, y+1}, {x-1, y-1}, {x-1, y+1}, {x+1, y-1}, {x+1, y+1}};
        
        for (int k = 0; k < coords.length; k++){
            if (coords[k][0] > -1 && coords[k][0] < width && coords[k][1] > -1 && coords[k][1] < height){
                if (board[coords[k][0]][coords[k][1]].isBomb()){
                count += 1;
                }
            }
        }
        checkRep();
        return count;
    }
    
    @Override
    public synchronized String toString(){
        String boardString = "";
        
        for (int y = 0; y < height; y++){
            for (int x = 0; x < width; x++){
                Cell cell = board[x][y];
                int neighborBombCount = countNeighborBombs(x, y);
                if (cell.isDug() && neighborBombCount == 0){
                    boardString += " ";
                } 
                else if (cell.isFlagged()){
                    boardString += "F";
                } 
                else if (cell.isDug() && neighborBombCount > 0){
                    boardString += Integer.toString(neighborBombCount);
                }
                else{
                    boardString += "-";
                }
                
                boardString += " ";
            }
            boardString = boardString.substring(0, boardString.length()-1);
            boardString += "\n";
        }
        checkRep();
        return boardString;
    }
    
    /**
     * Gets width of board
     * @return width, int width of board
     */
    public synchronized int getWidth() {
        return width;
    }

    /**
     * Gets height of board
     * @return height, int height of board
     */
    public synchronized int getHeight() {
        return height;
    }
    
    /**
     * Digs a cell
     * @param x, column # of cell, x > 0 && x < width 
     * @param y, row # of cell, y > 0 && y < height
     * @return true if dug a bomb, false if not a bomb
     */
    public synchronized boolean dig(int x, int y){
        board[x][y].digCell();
        if (isBomb(x, y)){
            removeBomb(x, y);
            uncoverNeighbors(x, y);
            return true;
        }
        uncoverNeighbors(x, y);
        checkRep();
        return false;
    }
    
    /**
     * Returns true if cell has been touched
     * @param x, column # of cell, x > 0 && x < width 
     * @param y, row # of cell, y > 0 && y < height
     * @return  true if cell has been touched
     */
    public synchronized boolean isTouched(int x, int y){
        return board[x][y].isTouched();
    }
    
    /**
     * Returns true if cell has a bomb
     * @param x, column # of cell, x > 0 && x < width 
     * @param y, row # of cell, y > 0 && y < height
     * @return true iff cell has a bomb
     */
    public synchronized boolean isBomb(int x, int y){
        return board[x][y].isBomb();
    }
    
    /**
     * Remove a bomb from cell
     * @param x, column # of cell, x > 0 && x < width 
     * @param y, row # of cell, y > 0 && y < height
     */
    public synchronized void removeBomb(int x, int y){
        board[x][y].setBomb(false);
     }
    
    /**
     * Flags a cell
     * @param x, column # of cell, x > 0 && x < width 
     * @param y, row # of cell, y > 0 && y < height
     */
    public synchronized void flag(int x, int y){
        board[x][y].setFlagged(true);
    }

    /**
     * Removes a flag from cell
     * @param x, column # of cell, x > 0 && x < width 
     * @param y, row # of cell, y > 0 && y < height
     */
    public synchronized void deflag(int x, int y){
        board[x][y].setFlagged(false);
    }
    
    
    /**
     * If a cell has no neighboring bombs, digs all connected cells that also have no neighboring bombs.
     * 
     * @param x, column # of cell, x > 0 && x < width 
     * @param y, row # of cell, y > 0 && y < height
     */
    public synchronized void uncoverNeighbors(int x, int y){
        int neighborBombCount = countNeighborBombs(x, y);
        int[][] coords = {{x-1, y}, {x+1, y}, {x, y-1}, {x, y+1}, {x-1, y-1}, {x-1, y+1}, {x+1, y-1}, {x+1, y+1}};

        if (neighborBombCount == 0){
            for (int k = 0; k < coords.length; k++){
                int xCoord = coords[k][0];
                int yCoord = coords[k][1];
                
                if (xCoord > -1 && xCoord < width && yCoord > -1 && yCoord < height 
                && !board[xCoord][yCoord].isDug()){
                    dig(xCoord, yCoord);
                    uncoverNeighbors(xCoord, yCoord);
                }
            }
        }
        checkRep();
    }
}
