/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package minesweeper;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

/**
 * Tests for Board class
 */
        
    //Board
    //  - Test invalid file
    // Don't test private countNeighborBombs
    // # of neighbor bombs: 0, 1, >1
    //
    //toString()
    //  - Board created from file, from width and height
    //  - Board is square, board is not square
    //  - Board has not been touched, board has been dug 
    //
    //getWidth()
    //getHeight()
    //
    //dig
    // cell is bomb, cell is not bomb
    // recursive dig, non-recursive dig
    // # neighbor bombs: 0, 1, > 1
    //
    //isTouched
    // cell is touched, cell is not touched
    //
    //isBomb
    //  cell is bomb, is not bomb
    // removeBomb
    //  cell is bomb, is not bomb
    //flag
    // cell is flagged, cell is not flagged
    //deflag
    //cell is flagged, cell is not flagged
    //

public class BoardTest {
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    
    
    //Test toString with square board from file
    @Test
    public void testToStringFromFile() throws IOException{
        File file = new File("./test/minesweeper/board_file_5");
        Board board = new Board(file);
        
        String expected = "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n";
        
        assertEquals("toString from file", board.toString(), expected);
    }
    
    //Test toString with non-square board
    @Test
    public void testToStringFromSize() throws IOException{
        Board board = new Board(7, 5);
        
        String expected = "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n";
        
        assertEquals("toString from file", board.toString(), expected);
    }

    //Test dig and toString when no neighbor bombs 
    @Test
    public void testDigNeighborBombsNone() throws IOException{
        File file = new File("./test/minesweeper/board_file_6");
        Board board = new Board(file);
        
        board.dig(0, 0);
        
        String expected = "    1 - - 2  \n"
                + "    1 - - 2  \n"
                + "    1 2 2 1  \n"
                + "             \n"
                + "             \n"
                + "1 1          \n"
                + "- 1          \n";
        
        assertEquals("toString from file", expected, board.toString());
    }
    
    //Test when one neighbor bomb
    @Test
    public void testDigNeighborBombsOne() throws IOException{
        File file = new File("./test/minesweeper/board_file_5");
        Board board = new Board(file);
        
        board.dig(0, 5);
        
        String expected = "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "1 - - - - - -\n"
                + "- - - - - - -\n";
        
        assertEquals("toString from file", expected, board.toString());
    }
    
    //Test when two neighbor bombs
    @Test
    public void testDigNeighborBombs() throws IOException{
        File file = new File("./test/minesweeper/board_file_6");
        Board board = new Board(file);
        
        board.dig(3, 0);
        
        String expected = "- - - 3 - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n";
        
        assertEquals("toString from file", board.toString(), expected);
    }
    
    //Test when two neighbor bombs
    @Test
    public void testDigBomb() throws IOException{
        File file = new File("./test/minesweeper/board_file_6");
        Board board = new Board(file);
        
        board.dig(4, 0);
        
        String expected = "- - - - 2 - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n";
        
        assertEquals("toString from file", board.toString(), expected);
    }
    
    //Test dig non-recursively
    @Test
    public void testDig() throws IOException{
        File file = new File("./test/minesweeper/board_file_5");
        Board board = new Board(file);
        
        board.dig(4, 0);
        
        String expected = "- - - - 1 - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n";
                
        assertEquals("toString from file", expected, board.toString());
        
    }
    
    //Test dig recursively
    @Test
    public void testDigRecursive() throws IOException{
        File file = new File("./test/minesweeper/board_file_5");
        Board board = new Board(file);
        
        board.dig(0, 0);
        
        String expected = "      1 - 1  \n"
                        + "      1 - 1  \n"
                        + "      1 1 1  \n"
                        + "             \n"
                        + "             \n"
                        + "1 1          \n"
                        + "- 1          \n";
        
        assertEquals("toString from file", expected, board.toString());
        
    }
    
    //Test height
    @Test
    public void testHeight() throws IOException{
        File file = new File("./test/minesweeper/board_file_5");
        Board board = new Board(file);
        
        assertEquals("test height", 7, board.getHeight());
    }
    
    //Test width
    @Test
    public void testWidth() throws IOException{
        File file = new File("./test/minesweeper/board_file_5");
        Board board = new Board(file);
        
        assertEquals("test width", 7, board.getWidth());
    }
    
    
    //Test flag
    @Test
    public void testFlag() throws IOException{
        File file = new File("./test/minesweeper/board_file_5");
        Board board = new Board(file);
        
        board.flag(0, 6);
        
        String expected = "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "F - - - - - -\n";
        
        assertEquals("test flag", expected, board.toString());
    }
    
    //Test width
    @Test
    public void testDeflag() throws IOException{
        File file = new File("./test/minesweeper/board_file_5");
        Board board = new Board(file);
        
        board.flag(0, 6);
        
        board.deflag(0, 6);
        
        String expected = "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n"
                + "- - - - - - -\n";
        
        assertEquals("test deflag", expected, board.toString());
    }

    //Test invalid file
    @Test(expected=RuntimeException.class)
    public void testInvalidFile() throws IOException{
        File file = new File("./test/minesweeper/invalid_board_file");
        Board board = new Board(file);
        
        assertEquals("test width", 7, board.getWidth());
    }
    
}
