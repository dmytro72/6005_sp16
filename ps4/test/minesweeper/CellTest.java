package minesweeper;

import static org.junit.Assert.*;

import org.junit.Test;



/**
 * Tests for Cell class
 *
 */
public class CellTest {
    
    //isTouched and setTouched
    // - cell is touched, cell is untouched
    //isBomb and setBomb
    //  - cell is bomb, cell is not bomb
    //isFlagged and setFlagged
    // - cell is flagged, cell is not flagged
    //digCell
    // -cell is dug, cell is not dug
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    //Test isTouched and setTouched
    @Test
    public void testTouched(){
        Cell c = new Cell();
        
        assertFalse("not touched", c.isTouched());
        
        c.setTouched(true);
        assertTrue("touched", c.isTouched());
        
    }
    
    //test isBomb and setBomb
    @Test
    public void testBomb(){
        Cell c = new Cell();
        
        assertFalse("not touched", c.isBomb());
        
        c.setBomb(true);
        assertTrue("touched", c.isBomb());
      
    }
    
    //test isFlagged and setFlagged
    @Test
    public void testFlagged(){
        Cell c = new Cell();
        
        assertFalse("not touched", c.isFlagged());
        
        c.setFlagged(true);
        assertTrue("touched", c.isFlagged());
      
    }
    
    //test dig cell
    @Test
    public void testDigCell(){
        Cell c = new Cell();
        
        assertFalse("touched", c.isTouched());
        assertFalse("dug", c.isDug());
        
        c.digCell();
        
        assertTrue("touched", c.isTouched());
        assertTrue("dug", c.isDug());
      
    }
}
