/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */

/**
 * An immutable point in floating-point pixel space.
 */
public class Point {
    private final double x;
    private final double y;
    
    // Abstraction function:
    //      Represents a point (x,y) in the plane.
    // Rep invariant:
    //      true
    // Safety from rep exposure:
    //      All fields are private, all types in the rep are immutable.

    // assert the rep invariant
    private void checkRep() {
        // nothing to do. can't even compare x and y against null
    }
    
    /**
     * Construct a point at the given coordinates.
     * @param x x-coordinate
     * @param y y-coordinate
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
        checkRep();
    }

    /**
     * @return x-coordinate of the point
     */
    public double x() {
        return x;
    }

    /**
     * @return y-coordinate of the point
     */
    public double y() {
        return y;
    }
    
    @Override 
    public String toString() {
        return "(" + x + "," + y + ")";
    }
    
    @Override
    public boolean equals(Object thatObject) { //Must take in Object
        if ( ! (thatObject instanceof Point)) { return false; }
        Point that = (Point)thatObject;
        
        return (this.x() == that.x()  && this.y() == that.y());
    }
    
    @Override 
    public int hashCode() {
        return (int)x * (int)y;
    }
}
