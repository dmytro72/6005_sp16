/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package twitter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * SocialNetwork provides methods that operate on a social network.
 * 
 * A social network is represented by a Map<String, Set<String>> where map[A] is
 * the set of people that person A follows on Twitter, and all people are
 * represented by their Twitter usernames. Users can't follow themselves. If A
 * doesn't follow anybody, then map[A] may be the empty set, or A may not even exist
 * as a key in the map; this is true even if A is followed by other people in the network.
 * Twitter usernames are not case sensitive, so "ernie" is the same as "ERNie".
 * A username should appear at most once as a key in the map or in any given
 * map[A] set.
 * 
 * DO NOT change the method signatures and specifications of these methods, but
 * you should implement their method bodies, and you may add new public or
 * private methods or classes if you like.
 */

public class SocialNetwork {

    /**
     * Finds hashtags in a String
     * 
     * @param s
     *            String of tweet text
     * @return 
     *         List of hashtags in tweet text
     */
    public static List<String> findHashtagsInString(String s){
        ArrayList<String> hashtags = new ArrayList<String>();
        
        Pattern r = Pattern.compile("#([A-Za-z0-9_-]+)");
        
        Matcher m = r.matcher(s);
               
        while (m.find()){
            hashtags.add(m.group(1));
        }
        return hashtags;
        
     }
    
    
    /**
     * Guess who might follow whom, from evidence found in tweets.
     * 
     * @param tweets
     *            a list of tweets providing the evidence, not modified by this
     *            method.
     * @return a social network (as defined above) in which Ernie follows Bert
     *         if and only if there is evidence for it in the given list of
     *         tweets.
     *         One kind of evidence that Ernie follows Bert is if Ernie
     *         @-mentions Bert in a tweet. This must be implemented. Other kinds
     *         of evidence may be used at the implementor's discretion.
     *         All the Twitter usernames in the returned social network must be
     *         either authors or @-mentions in the list of tweets.
     *         
     */
    
    public static Map<String, Set<String>> guessFollowsGraph(List<Tweet> tweets) {
        Map<String, Set<String>> followMap = new HashMap<String, Set<String>>();
        
        Map<String, Set<String>> hashtagsToUsers = new HashMap<>();
        
        for (Tweet t: tweets){
            String author = t.getAuthor().toLowerCase();
            Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(t));
            
            Set<String> usersWithoutSelf = new HashSet<>();
            
            usersWithoutSelf.addAll(mentionedUsers);
            usersWithoutSelf.remove(author);
            
            if (!followMap.containsKey(author)){
                followMap.put(author, usersWithoutSelf);
            }
            else if (followMap.containsKey(author)){
                followMap.get(author).addAll(usersWithoutSelf);
//                followMap.put(author, );
            }
            
            //added hashtag inferences. If multiple people use a hashtag, assume that everyone follows 
            //others that use the hashtag 
            
            List<String> hashtags = findHashtagsInString(t.getText());
            
            for (String tag: hashtags){
                if (hashtagsToUsers.containsKey(tag)) {
                    hashtagsToUsers.get(tag).add(author);
                }
                else{
                    hashtagsToUsers.put(tag, new HashSet<String>(Arrays.asList(author)));
                }
            }
            
            for (Set<String> users: hashtagsToUsers.values()){
                for (String user : users){
                    Set<String> usersWithoutSelfHashtag = new HashSet<>();
                    
                    usersWithoutSelfHashtag.addAll(users);
                    usersWithoutSelfHashtag.remove(user);
                 
                    if (!followMap.containsKey(user)){
                        followMap.put(user, usersWithoutSelfHashtag);
                    }
                    else if (followMap.containsKey(author)){
                        followMap.get(user).addAll(usersWithoutSelfHashtag);
                    }
                }
            }
        }
        
        for (String author: followMap.keySet()){
            if (followMap.get(author).contains(author)){
                followMap.get(author).remove(author);
            }
        }
        
        return followMap;
    }

    /**
     * Find the people in a social network who have the greatest influence, in
     * the sense that they have the most followers.
     * 
     * @param followsGraph
     *            a social network (as defined above)
     * @return a list of all distinct Twitter usernames in followsGraph, in
     *         descending order of follower count.
     */
    public static List<String> influencers(Map<String, Set<String>> followsGraph) {
        List<String> orderedNames = new ArrayList<String>();
        Map<String, Integer> nameMap = new HashMap<String, Integer>();
        
        for (Set<String> set : followsGraph.values()) {
            for (String followedPerson: set){
                followedPerson = followedPerson.toLowerCase();
                if (nameMap.containsKey(followedPerson)){
                    //nameMap contains the followedPerson, so increment the count for the followed person
                    nameMap.put(followedPerson, nameMap.get(followedPerson)+1);
                }
                else{
                    //nameMap contains the followedPerson, so create a count for the followed person
                    nameMap.put(followedPerson, 1);
                }
            }
        }
        
        for (String user: followsGraph.keySet()){
            if (!nameMap.containsKey(user)){
                //nameMap contains the followedPerson, so increment the count for the followed person
                nameMap.put(user, 0);
            }
        }
        
        List<Map.Entry<String, Integer>> nameList = new ArrayList<>(nameMap.entrySet());
        
        //Sort in descending order
        Collections.sort(nameList, new Comparator<Map.Entry<String, Integer>>(){
            public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2){
                return -entry1.getValue().compareTo(entry2.getValue()); 
            }
        });
        
        for (Map.Entry<String, Integer> entry : nameList){
            orderedNames.add(entry.getKey());
        }
        
        return orderedNames;
    
    }
   
}
