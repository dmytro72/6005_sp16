/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package twitter;

import static org.junit.Assert.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class SocialNetworkTest {
    
    /*
    *Partitions:
    *guessFollowsGraph():
    * # of @mentions: 0, 1, >1
    * tweets.length: 0, 1, >1
    * # of usernames: 0, 1, >1
    * 
    * 
    *influencers():
    * # of usernames in followGraph: 0, 1, >1
    * # of followers for each username: 0, 1, >1
    * 
     */
    
    private static final Instant D1 = Instant.parse("2016-02-17T10:00:00Z");
    private static final Instant D2 = Instant.parse("2016-02-17T11:00:00Z");
    private static final Instant D3 = Instant.parse("2016-02-18T12:00:00Z");
    private static final Instant D4 = Instant.parse("2016-02-19T11:00:00Z");
    private static final Instant D5 = Instant.parse("2016-02-19T13:00:00Z");
    
    private static final Tweet TWEET1 = new Tweet(1, "alyssa", "is it reasonable to talk about rivest so much?", D1);
    private static final Tweet TWEET2 = new Tweet(2, "bbitdiddle", "rivest talk in 30 minutes #hype", D2);
    private static final Tweet TWEET3 = new Tweet(3, "bbitdiddle", "username is mit: @mit.edu", D3);
    private static final Tweet TWEET4 = new Tweet(4, "bbitdiddle", "invalid: ben@mit.edu", D2);
    private static final Tweet TWEET5 = new Tweet(5, "bbitdiddle", "here's an emoji with username _: @user@", D2);
    private static final Tweet TWEET6 = new Tweet(6, "bbitdiddle", "username is mit: @mit.edu, ben@yale.edu, @ ", D3);
    private static final Tweet TWEET7 = new Tweet(7, "bbitdiddle", "username is mit: @mit.edu, username is david: @david", D3);
    
    private static final Tweet TWEET9 = new Tweet(9, "max_goldman", "I follow all my students @jane @bily @steven", D3);
    private static final Tweet TWEET10 = new Tweet(10, "Max_goldMAN", "I also follow faculty @bill @joe @rob", D5);
    
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    //Test when list of tweet is empty
    @Test
    public void testGuessFollowsGraphEmpty() {
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(new ArrayList<>());
        
        assertTrue("expected empty graph", followsGraph.isEmpty());
    } 
    
    //Test when there are no mentions in the tweets
    @Test
    public void testGuessFollowsGraphNoMention() {
        List<Tweet> tweets = Arrays.asList(TWEET2);
        
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(tweets);
        
        assertTrue("expected bbitdiddle", (followsGraph.containsKey("bbitdiddle") && followsGraph.get("bbitdiddle").size() == 0)
                || !followsGraph.containsKey("bbitdiddle")); 
    } 
    
    //Test when there is one mention in the tweets
    @Test
    public void testGuessFollowsGraphOneMention() {
        List<Tweet> tweets = Arrays.asList(TWEET3);
        
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(tweets);
        
        assertTrue("expected bbitdiddle", followsGraph.get("bbitdiddle").size() == 1);     
        assertTrue("expected one followee", followsGraph.get("bbitdiddle").contains("mit"));
        assertTrue("mit may have the empty set or not even be a key", (followsGraph.containsKey("mit") && 
                followsGraph.get("mit").size() == 0) || !followsGraph.containsKey("mit"));
    } 
    
    //Test when there are several mentions in the tweets
    @Test
    public void testGuessFollowsGraphSeveralMention() {
        List<Tweet> tweets = Arrays.asList(TWEET1, TWEET2, TWEET3, TWEET4, TWEET5, TWEET6, TWEET7);
        
        Map<String, Set<String>> followsGraph = SocialNetwork.guessFollowsGraph(tweets);
        
        assertTrue("expected one tweet author in graph", followsGraph.containsKey("bbitdiddle"));
        assertTrue("expected one followee", followsGraph.get("bbitdiddle").containsAll(Arrays.asList("mit", "david")));
        assertTrue("mit may have the empty set or not even be a key", (followsGraph.containsKey("mit") && 
                followsGraph.get("mit").size() == 0) || !followsGraph.containsKey("mit"));
         
        assertTrue("expected alyssa", (followsGraph.containsKey("alyssa") && followsGraph.get("alyssa").size() == 0)
                || !followsGraph.containsKey("alyssa"));
    } 
    
    //Test when followsGraph is empty
    @Test
    public void testInfluencersEmpty() {
        Map<String, Set<String>> followsGraph = new HashMap<>();
        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertTrue("expected empty list", influencers.isEmpty());
    }
    
    //Test when there are two people, one has zero followers
    @Test
    public void testInfluencersOneInfluencer() {
        Map<String, Set<String>> followsGraph = new HashMap<>();
        
        followsGraph.put("bbitdiddle", new HashSet<String>(Arrays.asList("mit")));
        
        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertTrue("expected one influencer", influencers.size() == 2);
        assertTrue("expected one influencer", influencers.get(0).equals("mit"));
    }

    //Test when two people have diff follower counts
    @Test
    public void testInfluencersTwoInfluencerDiffCounts() {
        Map<String, Set<String>> followsGraph = new HashMap<>();
        
        followsGraph.put("bbitdiddle", new HashSet<String>(Arrays.asList("MIT", "david")));
        followsGraph.put("alyssa", new HashSet<String>(Arrays.asList("mit")));

        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertTrue("expected first influencer", influencers.get(0).equals("mit"));
        assertTrue("expected second influencer", influencers.get(1).equals("david"));
        
    }
    
    //Test when two influencers have the same follower counts
    @Test
    public void testInfluencersTwoInfluencerSameCounts() {
        Map<String, Set<String>> followsGraph = new HashMap<>();
        
        followsGraph.put("bbitdiddle", new HashSet<String>(Arrays.asList("MIT", "david")));
        followsGraph.put("alyssa", new HashSet<String>(Arrays.asList("mit", "DAvId")));

        List<String> influencers = SocialNetwork.influencers(followsGraph);
        
        assertTrue("expected first influencer", influencers.contains("mit"));
        assertTrue("expected second influencer", influencers.contains("david"));
        
    }
    
    
    /*
     * Warning: all the tests you write here must be runnable against any
     * SocialNetwork class that follows the spec. It will be run against several
     * staff implementations of SocialNetwork, which will be done by overwriting
     * (temporarily) your version of SocialNetwork with the staff's version.
     * DO NOT strengthen the spec of SocialNetwork or its methods.
     * 
     * In particular, your test cases must not call helper methods of your own
     * that you have put in SocialNetwork, because that means you're testing a
     * stronger spec than SocialNetwork says. If you need such helper methods,
     * define them in a different class. If you only need them in this test
     * class, then keep them in this test class.
     */

}
