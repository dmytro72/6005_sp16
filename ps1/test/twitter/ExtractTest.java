/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package twitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class ExtractTest {

    /*
     * Testing strategy:
     *
     * Partition for getTimespan(list) -> Timespan
     * list.length = 0, 1, 2, >2
     * 
     *
     * 
     * Also, check that list of tweets is not modified.
     * 
     * Partition for getMentionedUsers(tweets) -> Set 
     * 
     * num of mentioned users: 0, 1, >1
     * tweets.length: 0, >0
     * Invalid username-mentions: @ sign is preceded by valid chars, followed by valid chars, or both. @ sign is alone.
     * Valid username-mentions: @ sign is not preceded or followed by valid chars.
     * 
     * Also, check that list of tweets is not modified.
     * 
     *  Chose num of mentioned users =0 and tweets.length >=0
     *  Chose num of mentioned users =1 and tweets.length >=1
     *      — invalid username-mentions present
     *      — no invalid usernames present 
     * Chose num of mentioned users >1 and tweets.length >=1
     *      — invalid username-mentions present
     *      —no invalid username-mentions present
     * 
     */
    
    private static final Instant D1 = Instant.parse("2016-02-17T10:00:00Z");
    private static final Instant D2 = Instant.parse("2016-02-17T11:00:00Z");
    private static final Instant D3 = Instant.parse("2016-02-18T12:00:00Z");
    
    private static final Tweet TWEET1 = new Tweet(1, "alyssa", "is it reasonable to talk about rivest so much?", D1);
    private static final Tweet TWEET2 = new Tweet(2, "bbitdiddle", "rivest talk in 30 minutes #hype", D2);
    private static final Tweet TWEET3 = new Tweet(3, "bbitdiddle", "username is mit: @mit.edu", D3);
    private static final Tweet TWEET4 = new Tweet(4, "bbitdiddle", "invalid: @ben.edu, case-insensitive: @BEN.edu", D2);
    private static final Tweet TWEET5 = new Tweet(5, "bbitdiddle", "here's an emoji with username _: @_-_@", D2);
    private static final Tweet TWEET6 = new Tweet(6, "bbitdiddle", "username is mit: @mit.edu, ben@yale.edu, @ ", D3);
    private static final Tweet TWEET7 = new Tweet(7, "bbitdiddle", "username is mit: @mit.edu, username is david: @david", D3);

    
    /**
     * Converts all strings in a set to lowercase
     * 
     * @param strings
     *           A set of strings
     * 
     * @return new Set with all the strings lowercase 
  
     */
    public static Set<String> toLowerCase(Set<String> strings)
    {
        Set<String> newStrings = new HashSet<String>();
        for (String s: strings){
            newStrings.add(s.toLowerCase());
        }
        
        return newStrings;
    }
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    //Test timespan for two tweets
    @Test
    public void testGetTimespanTwoTweets() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(TWEET1, TWEET2));
        
        assertEquals("expected start", D1, timespan.getStart());
        assertEquals("expected end", D2, timespan.getEnd());
    }
    
    //Test timespans for two tweets that are not in order
    @Test
    public void testGetTimespanNotInOrder() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(TWEET2, TWEET1));
        
        assertEquals("expected start", D1, timespan.getStart());
        assertEquals("expected end", D2, timespan.getEnd());
    }
    
    //Test timespan for list of no tweets
    @Test
    public void testGetTimespanNoTweets() {
        List<Tweet> tweets = new ArrayList<Tweet>();
        
        Timespan timespan = Extract.getTimespan(tweets);
   
        assertEquals("expected start equals end, no tweets", timespan.getStart(), timespan.getEnd());
        
    }
    
    //Test timespan for one tweet
    @Test
    public void testGetTimespanOneTweet() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(TWEET1));

        assertEquals("expected start equals end, one tweet", timespan.getStart(), timespan.getEnd());
    }
    
    //Test timespan for three tweets
    @Test
    public void testGetTimespanThreeTweets() {
        Timespan timespan = Extract.getTimespan(Arrays.asList(TWEET1, TWEET2, TWEET3));
        
        assertEquals("expected start", D1, timespan.getStart());
        assertEquals("expected end", D3, timespan.getEnd());
    }
    
    //Test if mutation occurs
    @Test
    public void testGetTimespanMutation() {
        //Checks to make sure that the array passed in is not modified1
        List<Tweet> tweets = Arrays.asList(TWEET6, TWEET7);
        Timespan timespan = Extract.getTimespan(tweets);
        
        assertEquals(Arrays.asList(TWEET6, TWEET7), tweets);

    }
    
    //Test when there are no mentions in tweets
    @Test
    public void testGetMentionedUsersNoMention() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(TWEET1));
        
        assertTrue("expected empty set", mentionedUsers.isEmpty());
    }
    
    //Test when the list of tweets is empty
    @Test
    public void testGetMentionedUsersEmptyList() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Collections.<Tweet>emptyList());
        
        assertTrue("expected empty set", mentionedUsers.isEmpty());
    }
    
    //Test when there is one mentioned user
    @Test
    public void testGetMentionedUsersOneList() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(TWEET3));
        
        mentionedUsers = toLowerCase(mentionedUsers);
        
        assertTrue("expected user mit", mentionedUsers.contains("mit"));
    }
    
    //Test when there is one mentioned user, excluding an invalid email
    @Test
    public void testGetMentionedUsersOneListWithInvalids() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(TWEET6));
        
        mentionedUsers = toLowerCase(mentionedUsers);
        
        assertEquals("only one username in set", mentionedUsers.size(), 1);
        assertTrue("expected user mit, not yale", mentionedUsers.contains("mit"));
    }
    
    //Test when there are multiple mentioned users in one tweet
    @Test
    public void testGetMentionedUsersManyMentionsOneTweet() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(TWEET7));
        
        mentionedUsers = toLowerCase(mentionedUsers);

        assertEquals("two username in set", mentionedUsers.size(), 2);
        assertTrue("expected user mit", mentionedUsers.contains("mit"));
        assertTrue("expected user david", mentionedUsers.contains("david"));
    }
    
    //Test mentioned users are case insensitive
    @Test
    public void testGetMentionedUsersCaseInsensitive() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(TWEET4));
        
        assertEquals("only one username in set", mentionedUsers.size(), 1);
        assertTrue("expected user ben, case-insensitive", mentionedUsers.contains("ben"));

    }
    
    //Test when there are multiple mentioned users in many tweets
    @Test
    public void testGetMentionedUsersManyMentions() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(TWEET7, TWEET5));
        
        mentionedUsers = toLowerCase(mentionedUsers);
        
        assertTrue("expected user mit", mentionedUsers.contains("mit"));
        assertTrue("expected user david", mentionedUsers.contains("david"));
        assertTrue("expected user _-_", mentionedUsers.contains("_-_"));
        
    }
    
    //Test that some @ mentions are invalid
    @Test
    public void testGetMentionedUsersManyMentionsWithInvalids() {
        Set<String> mentionedUsers = Extract.getMentionedUsers(Arrays.asList(TWEET6, TWEET7));
        
        mentionedUsers = toLowerCase(mentionedUsers);
        
        assertEquals("two username in set", mentionedUsers.size(), 2);
        assertTrue("expected user mit", mentionedUsers.contains("mit"));
        assertTrue("expected user david", mentionedUsers.contains("david"));  
    }
    
    //Test that getMentionedUsers does not cause mutation
    @Test
    public void testGetMentionedUsersMutation() {
        List<Tweet> tweets = Arrays.asList(TWEET6, TWEET7);
        Set<String> mentionedUsers = Extract.getMentionedUsers(tweets);
        
        assertEquals(Arrays.asList(TWEET6, TWEET7), tweets);

    }
    

    /*
     * Warning: all the tests you write here must be runnable against any
     * Extract class that follows the spec. It will be run against several staff
     * implementations of Extract, which will be done by overwriting
     * (temporarily) your version of Extract with the staff's version.
     * DO NOT strengthen the spec of Extract or its methods.
     * 
     * In particular, your test cases must not call helper methods of your own
     * that you have put in Extract, because that means you're testing a
     * stronger spec than Extract says. If you need such helper methods, define
     * them in a different class. If you only need them in this test class, then
     * keep them in this test class.
     */

}
