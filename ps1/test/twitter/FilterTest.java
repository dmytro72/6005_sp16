/* Copyright (c) 2007-2016 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package twitter;

import static org.junit.Assert.*;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.junit.Test;

public class FilterTest {

    /*
     * Testing strategy
     * 
     * Partitions for writtenBy() 
     * tweets.length: 0, >1 
     * num of tweets user has written: 0, 1, >1
     *  
     *  
     *  Partitions for inTimespan()
     *  tweets.length: 0, 1, >1
     *  length of timespan: 0, >0
     *  num of tweets in the timespan: 0, 1, >1
     * 
     * 
     * Partitions for containing()
     * tweets.length: 0, 1, >1
     * words.length: 0, 1, >1
     * num tweets containing words: 0, 1, >1
     * Words contain uppercase, punctuation, lowercase
     * 
     * 
    */ 
    
    private static final Instant D1 = Instant.parse("2016-02-17T10:00:00Z");
    private static final Instant D2 = Instant.parse("2016-02-17T11:00:00Z");
    private static final Instant D3 = Instant.parse("2016-02-18T12:00:00Z");
    
    private static final Tweet TWEET1 = new Tweet(1, "alyssa", "is it reasonable to talk about rivest so much?", D1);
    private static final Tweet TWEET2 = new Tweet(2, "bbitdiddle", "rivest talk in 30 minutes #hype", D2);
    private static final Tweet TWEET3 = new Tweet(3, "alyssa", "duplicate talk tweet #hype #strange!", D3);
    private static final Tweet TWEET4 = new Tweet(4, "BBITDIDDLE", "invalid: @ben.edu, case-insensitive: @BEN.edu", D2);
    private static final Tweet TWEET5 = new Tweet(5, "alyssa", "is it reasonable to talksomuch about rivest so much?", D1);

    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    //Test when author has written one tweet
    @Test
    public void testWrittenByMultipleTweetsSingleResult() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(TWEET1, TWEET2), "alyssa");
                
        assertEquals("expected singleton list", 1, writtenBy.size());
        assertTrue("expected list to contain tweet", writtenBy.contains(TWEET1));
    }
    
    //Test case-sensitivity of author 
    @Test
    public void testWrittnByCaseInsensitive() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(TWEET2, TWEET4), "bbitdiddle");
                
        assertEquals("expected two list", 2, writtenBy.size());
        assertTrue("expected list to contain tweets", writtenBy.containsAll(Arrays.asList(TWEET2, TWEET4)));
    }
    
    //Test when author has written multiple tweets
    @Test
    public void testWrittenByMultipleTweetsMultipleResult() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(TWEET1, TWEET3), "alyssa");
        
        assertEquals("expected size 2 list", 2, writtenBy.size());
        assertTrue("expected list to contain tweet", writtenBy.containsAll(Arrays.asList(TWEET1, TWEET3)));
        assertEquals("expected same order", 0, writtenBy.indexOf(TWEET1));
    }
    
    //Test when no tweets are written by author
    @Test
    public void testWrittenByMultipleTweetsNoResult() {
        List<Tweet> writtenBy = Filter.writtenBy(Arrays.asList(TWEET1, TWEET3), "ben");
        
        assertTrue("expected empty list", writtenBy.isEmpty());
    }
    
    //Test when list of tweets is empty
    @Test
    public void testWrittenByNoTweets() {
        List<Tweet> writtenBy = Filter.writtenBy(Collections.<Tweet>emptyList(), "ben");
        
        assertTrue("expected empty list", writtenBy.isEmpty());

    }
    
    //Test when two tweets are in the timespan
    @Test
    public void testInTimespanMultipleTweetsMultipleResults() {
        Instant testStart = Instant.parse("2016-02-17T09:00:00Z");
        Instant testEnd = Instant.parse("2016-02-17T12:00:00Z");
        
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(TWEET1, TWEET2), new Timespan(testStart, testEnd));
        
        assertFalse("expected non-empty list", inTimespan.isEmpty());
        assertTrue("expected list to contain tweets", inTimespan.containsAll(Arrays.asList(TWEET1, TWEET2)));
        assertEquals("expected same order", 0, inTimespan.indexOf(TWEET1));
    }
    
    //Test when three tweets are in the timespan
    @Test
    public void testInTimespanMultipleTweetsMultipleResults2() {
        Instant testStart = Instant.parse("2016-02-17T09:00:00Z");
        Instant testEnd = Instant.parse("2016-02-18T12:00:00Z");
        
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(TWEET1, TWEET2, TWEET3), new Timespan(testStart, testEnd));
        
        assertFalse("expected non-empty list", inTimespan.isEmpty());
        assertTrue("expected list to contain tweets", inTimespan.containsAll(Arrays.asList(TWEET1, TWEET2, TWEET3)));
        assertEquals("expected same order", 0, inTimespan.indexOf(TWEET1));
        assertEquals("expected same order", 1, inTimespan.indexOf(TWEET2));
        assertEquals("expected same order", 2, inTimespan.indexOf(TWEET3));
    }
    
    //Test when no tweets are in the timespan
    @Test
    public void testInTimespanMultipleTweetsNoResults() {
        Instant testStart = Instant.parse("2016-02-16T09:00:00Z");
        Instant testEnd = Instant.parse("2016-02-16T12:00:00Z");
        
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(TWEET1, TWEET2), new Timespan(testStart, testEnd));
        
        assertTrue("expected empty list", inTimespan.isEmpty());
    }
    
    //Test when timespan is of zero length
    @Test
    public void testInTimespanMultipleTweetsZeroTimespan() {
        Instant testStart = Instant.parse("2016-02-17T10:00:00Z");
        Instant testEnd = Instant.parse("2016-02-17T10:00:00Z");
        
        List<Tweet> inTimespan = Filter.inTimespan(Arrays.asList(TWEET1, TWEET2), new Timespan(testStart, testEnd));
        
        assertFalse("expected non-empty list", inTimespan.isEmpty());
        assertTrue("expected list to contain one tweet", inTimespan.containsAll(Arrays.asList(TWEET1)));
        assertEquals("expected same order", 0, inTimespan.indexOf(TWEET1));
    }
    
    //Test when list of tweets is empty
    @Test
    public void testInTimespanZeroTweets() {
        Instant testStart = Instant.parse("2016-02-17T09:00:00Z");
        Instant testEnd = Instant.parse("2016-02-17T12:00:00Z");
        
        List<Tweet> inTimespan = Filter.inTimespan(Collections.<Tweet>emptyList(), new Timespan(testStart, testEnd));
        
        assertTrue("expected empty list", inTimespan.isEmpty());

    }

    //Test for one tweet containing a word
    @Test
    public void testContaining() {
        List<Tweet> containing = Filter.containing(Arrays.asList(TWEET1, TWEET2), Arrays.asList("talk"));
        
        assertFalse("expected non-empty list", containing.isEmpty());
        assertTrue("expected list to contain tweets", containing.containsAll(Arrays.asList(TWEET1, TWEET2)));
        assertEquals("expected same order", 0, containing.indexOf(TWEET1));
    }
    
    
    //Test that no substring matching occurs
    @Test
    public void testContainingSubstring() {
        List<Tweet> containing = Filter.containing(Arrays.asList(TWEET5), Arrays.asList("talk"));
        
        assertTrue("expected empty list", containing.isEmpty());
    }
    
    //Test for many tweets containing a word
    @Test
    public void testContainingManyAnswers() {
        List<Tweet> containing = Filter.containing(Arrays.asList(TWEET1, TWEET2, TWEET3), Arrays.asList("talk"));
        
        assertFalse("expected non-empty list", containing.isEmpty());
        assertTrue("expected list to contain tweets", containing.containsAll(Arrays.asList(TWEET1, TWEET2, TWEET3)));
        assertEquals("expected same order", 0, containing.indexOf(TWEET1));
        assertEquals("expected same order", 1, containing.indexOf(TWEET2));
        assertEquals("expected same order", 2, containing.indexOf(TWEET3));
    }
  
    //Test for many tweets containing a word, case-insensitive
    @Test
    public void testContainingManyAnswersUppercase() {
        List<Tweet> containing = Filter.containing(Arrays.asList(TWEET1, TWEET2, TWEET3), Arrays.asList("TAlk"));
        
        assertFalse("expected non-empty list", containing.isEmpty());
        assertTrue("expected list to contain tweets", containing.containsAll(Arrays.asList(TWEET1, TWEET2, TWEET3)));
        assertEquals("expected same order", 0, containing.indexOf(TWEET1));
        assertEquals("expected same order", 1, containing.indexOf(TWEET2));
        assertEquals("expected same order", 2, containing.indexOf(TWEET3));
    }
    
    //Test for many tweets containing one of a list of words
    @Test
    public void testContainingManyWords() {
        List<Tweet> containing = Filter.containing(Arrays.asList(TWEET1, TWEET2, TWEET3), Arrays.asList("alsonot", "notpresent", "#strange!"));
        
        assertFalse("expected non-empty list", containing.isEmpty());
        assertTrue("expected list to contain tweets", containing.containsAll(Arrays.asList(TWEET3)));
        
    }
    
    //Test for an empty of list of words
    @Test
    public void testContainingZeroWords() {
        List<Tweet> containing = Filter.containing(Arrays.asList(TWEET1, TWEET2), Collections.<String>emptyList());
        
        assertTrue("expected empty list", containing.isEmpty());
    }
    
    //Test for an empty of list of tweets
    @Test
    public void testContainingZeroTweets() {
        List<Tweet> containing = Filter.containing(Collections.<Tweet>emptyList(), Arrays.asList("talk"));
        
        assertTrue("expected empty list", containing.isEmpty());
    }
    
    /*
     * Warning: all the tests you write here must be runnable against any Filter
     * class that follows the spec. It will be run against several staff
     * implementations of Filter, which will be done by overwriting
     * (temporarily) your version of Filter with the staff's version.
     * DO NOT strengthen the spec of Filter or its methods.
     * 
     * In particular, your test cases must not call helper methods of your own
     * that you have put in Filter, because that means you're testing a stronger
     * spec than Filter says. If you need such helper methods, define them in a
     * different class. If you only need them in this test class, then keep them
     * in this test class.
     */

}
