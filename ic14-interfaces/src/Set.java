/**
 * A mutable set of characters.
 */
public interface Set<E> {
    
    //////
    // creators:
    
    // TODO
    
    //////
    // observers:
    /**
     * Get size of the set.
     * @return the number of characters in this set
     */
    public int size(); 
       
    
    /**
     * Test for membership.
     * @param c a character
     * @return true iff this set contains c
     */
    public boolean contains(char c);
    
    
    //////
    // producers:
    
    // TODO
    
    //////
    // mutators:
    
    public void add(char c);
    public void remove(char c);
    
    
    
}
