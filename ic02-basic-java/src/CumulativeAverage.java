import java.util.ArrayList;

public class CumulativeAverage {
    
    /**
     * Compute cumulative averages for example data.
     * @param a list of integer inputs to average
     */
    public static ArrayList<Double> printCumulativeAverages(int[] data){
                
        int n = 0;
        int total = 0;
        double average = 0;
        
        ArrayList<Double> averages = new ArrayList<Double>();
        
        for (int value : data) {
            n += 1;
            total += value;
            average = ((double)total) / n;
            averages.add(average);
   
        }
        
        return averages;
    }
    
    public static void main(String[] args) {
        int[] data = new int[] { 1, 2, 3 };
        
        ArrayList<Double> averages = printCumulativeAverages(data);
        
        for (double i : averages){
            
        }
             
    }
     
}
