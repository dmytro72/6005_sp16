import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/** Search web pages for lines containing a string. */
public class Grep {
    public static void main(String[] args) throws Exception {
        
        // substring to search for
        String substring = "specification";
        
        // URLs to search
        String[] urls = new String[] {
                "http://web.mit.edu/6.005/www/sp16/psets/ps0/",
                "http://web.mit.edu/6.005/www/sp16/psets/ps1/",
                "http://web.mit.edu/6.005/www/sp16/psets/ps2/",
                "http://web.mit.edu/6.005/www/sp16/psets/ps3/",
        };
        
        // list for accumulating matching lines
        List<Item> matches = Collections.synchronizedList(new ArrayList<>());
        
        // queue for sending lines from producers to consumers
        BlockingQueue<Item> queue = new LinkedBlockingQueue<Item>();
        
        Thread[] downloaders = new Thread[urls.length]; // one downloader per URL
        Thread[] searchers = new Thread[1]; // TODO use multiple searchers
        
        for (int ii = 0; ii < searchers.length; ii++) { // create & start searching threads
            Thread searcher = searchers[ii] = new Thread(new Searcher(queue, matches, substring));
            searcher.start();
        }
        
        for (int ii = 0; ii < urls.length; ii++) { // create & start downloading threads
            Thread downloader = downloaders[ii] = new Thread(new Downloader(new URL(urls[ii]), queue));
            downloader.start();
        }
        
        for (Thread downloader : downloaders) { // wait for downloaders to stop
            downloader.join();
        }
        
        // stop searching threads somehow
        // ...
        // ...
        
        for (Thread searcher : searchers) { // wait for searchers to stop
            searcher.join();
        }
        
        for (Item match : matches) {
            System.out.println(match);
        }
        System.out.println(matches.size() + " lines matched");
    }
}

class Downloader implements Runnable {
    
    private final URL url;
    private final BlockingQueue<Item> queue;
    
    Downloader(URL url, BlockingQueue<Item> queue) {
        this.url = url;
        this.queue = queue;
        // TODO construct your Downloader here!
    }

    public void run() {
        try {
            InputStream inputStream = url.openStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            int lineNumber = 0;
            for(String line; (line = reader.readLine()) != null; ) {
                queue.put(new Text(url.toString(), lineNumber, line));
                ++lineNumber;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        // TODO read lines and push them onto the queue for consumers
    }
}

class Searcher implements Runnable {
    
    BlockingQueue<Item> queue;
    List<Item> matches;
    String substring;
    
    Searcher(BlockingQueue<Item> queue, List<Item> matches, String substring) {
        this.queue = queue;
        this.matches = matches;
        this.substring = substring;
    
    }

    public void run() {
        
        while(true) {   //TODO
           try {
               Item item = queue.take();
               if(item.text().contains(substring)) {
                   matches.add(item);
               }
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
           
        }
        
    }
}


/** Represents single item of work. */
interface Item {
    /** @return the filename */
    public String filename();
    /** @return the line number */
    public int lineNumber();
    /** @return the text */
    public String text();
}

class Text implements Item {
    private final String filename;
    private final int lineNumber;
    private final String text;
    
    public Text(String filename, int lineNumber, String text) {
        this.filename = filename;
        this.lineNumber = lineNumber;
        this.text = text;
    }
    
    public String filename() {
        return filename;
    }
    
    public int lineNumber() {
        return lineNumber;
    }
    
    public String text() {
        return text;
    }
    
    @Override public String toString() {
        return filename + ":" + lineNumber + " " + text;
    }
}
