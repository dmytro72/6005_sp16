import java.util.HashSet;
import java.util.Set;

/**
 * Solver for Sudoku puzzles.
 * 
 * A Sudoku puzzle is a square NxN grid of cells in which the numbers 1..N
 * should be written so that each number appears exactly once in each row, column, and block
 * (where a block is a sqrt(N) x sqrt(N) grid of cells, and the whole grid is tiled by N blocks).
 * See http://en.wikipedia.org/wiki/Sudoku for more information.
 * 
 */
public class Sudoku {
    public static final int BLOCK_SIZE = 3;
    public static final int PUZZLE_SIZE = BLOCK_SIZE * BLOCK_SIZE;
    

    private int[][] puzzle;
    
    private Map<Coordinate, Integer> puzzle;
    
    
    /**
     * Empty onstructor for Sudoku puzzle
     * sets this.puzzle to an empty array of arrays of PUZZLE_SIZE
     * @param puzzle
     */
    public Sudoku() {
    	this.puzzle = new int[PUZZLE_SIZE][PUZZLE_SIZE];
    }
    
    /**
 	 * @param int[][] puzzle has the following preconditions:
 	 *  - it's a square array PUZZLE_SIZE x PUZZLE_SIZE:
 	 *        puzzle.length = PUZZLE_SIZE
 	 *        puzzle[i].length = PUZZLE_SIZE for all 0 <= i < PUZZLE_SIZE
 	 *  - contains only valid numbers or blanks:
 	 *        puzzle[i][j] in { 0, ..., PUZZLE_SIZE } for all 0 <= i, j < PUZZLE_SIZE
 	 *  - no positive number appears more than once in any row, column, or block
     */
    public Sudoku(int[][] puzzle) {
        this.puzzle = new int[PUZZLE_SIZE][PUZZLE_SIZE];
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            for (int j = 0; j < PUZZLE_SIZE; ++j) {
                this.puzzle[i][j] = puzzle[i][j];
            }
        }
    }
        
//    }
//    public Sudoku(int[][] puzzle){
//        this.puzzle = puzzle;
//                
//   }
//   
    /**
     * @param row    0 <= row < PUZZLE_SIZE
     * @param column 0 <= column < PUZZLE_SIZE
     * @return the set of numbers that are entered in the same row of the puzzle
     *  as the cell at [row][column], all elements in the set are in [1...PUZZLE_SIZE]
     */
    public Set<Integer> getRow(int row, int column) {
        Set<Integer> numbers = new HashSet<>();
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            numbers.add(puzzle[row][i]);
        }
        numbers.remove(0); // don't include empty cells
        return numbers;
    }

    /**
     * @param row    0 <= row < PUZZLE_SIZE
     * @param column 0 <= column < PUZZLE_SIZE
     * @return the set of numbers that are entered in the same column of the puzzle
     *  as the cell at [row][column], all elements in the set are in [1...PUZZLE_SIZE]
     */
    public Set<Integer> getColumn(int row, int column) {
        Set<Integer> numbers = new HashSet<>();
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            numbers.add(puzzle[i][column]);
        }
        numbers.remove(0); // don't include empty cells
        return numbers;
    }

    /**
     * @param row    0 <= row < PUZZLE_SIZE
     * @param column 0 <= column < PUZZLE_SIZE
     * @return the set of digits that are entered in the same block of the puzzle
     *  as the cell at [row][column], all elements in the set are in [1...PUZZLE_SIZE]
     */
    public Set<Integer> getBlock(int row, int column) {
        Set<Integer> numbers = new HashSet<>();
        int firstRowOfBlock = (row / BLOCK_SIZE) * BLOCK_SIZE;
        int firstColumnOfBlock = (column / BLOCK_SIZE) * BLOCK_SIZE;
        for (int i = 0; i < PUZZLE_SIZE; ++i) {
            int cellRow = firstRowOfBlock + (i / BLOCK_SIZE);
            int cellColumn = firstColumnOfBlock + (i % BLOCK_SIZE); 
            numbers.add(puzzle[cellRow][cellColumn]);
        }
        numbers.remove(0); // don't include empty cells
        return numbers;
    }
    

    /**
     * @return true if the puzzle can be solved, and modifies
     *  puzzle to fill in blank cells with a solution.
     *  Returns false if no solution exists.
     */    
    public boolean solve() {
        assertValid();
        // find an empty cell
        for (int row = 0; row < PUZZLE_SIZE; ++row) {
            for (int column = 0; column < PUZZLE_SIZE; ++column) {
                if (puzzle[row][column] == 0) {
                    // found an empty cell; try to fill it
                    Set<Integer> alreadyUsed = new HashSet<>();
                    alreadyUsed.addAll(getRow(row, column));
                    alreadyUsed.addAll(getColumn(row, column));
                    alreadyUsed.addAll(getBlock(row, column));

                    for (int number = 1; number <= PUZZLE_SIZE; ++number) {
                        if ( ! alreadyUsed.contains(number)) {
                            puzzle[row][column] = number;
                            if (solve()) {
                                return true;
                            }
                            // couldn't solve it with that choice,
                            // so clear the cell again and backtrack
                            puzzle[row][column] = 0;
                        }
                    }
                    
                    return false; // nothing works for this cell! give up and backtrack
                }
            }
        }
        return true; // no empty cells found, so puzzle is already solved
    }
    
    /**
     * @return true if and only if each row, column, and block of
     *  the puzzle uses all the numbers 1...PUZZLE_SIZE exactly once.
     */
    public boolean isSolved() {
        for (int row = 0; row < PUZZLE_SIZE; ++row) {
            if (getRow(row, 0).size() != PUZZLE_SIZE) return false;
        }
        for (int column = 0; column < PUZZLE_SIZE; ++column) {
            if (getColumn(0, column).size() != PUZZLE_SIZE) return false;
        }
        for (int row = 0; row < PUZZLE_SIZE; row += BLOCK_SIZE) {
            for (int column = 0; column < PUZZLE_SIZE; column += BLOCK_SIZE) {
                if (getBlock(row, column).size() != PUZZLE_SIZE) return false;
            }
        }
        return true;
    }
    
    /**
     * @param puzzle (see above for precondition on puzzle)
     * @return string representation of puzzle, suitable for printing.
     */
    public String toString() {
        String result = "";
        for (int row = 0; row < PUZZLE_SIZE; ++row) {
            if (row > 0 && row % BLOCK_SIZE == 0) {
                result += "\n";
            }
            for (int column = 0; column < PUZZLE_SIZE; ++column) {
                if (column > 0 && column % BLOCK_SIZE == 0) {
                    result += " ";
                }
                int cell = puzzle[row][column];
                if (cell == 0) {
                    result += "_";
                } else {
                    result += puzzle[row][column];
                }
            }
            result += "\n";
        }
        return result;
    }

    // asserts that puzzle follows the precondition at the top of this class
    private void assertValid() {
        assert puzzle.length == PUZZLE_SIZE : "wrong number of rows";
        for (int[] row : puzzle) {
            assert row.length == PUZZLE_SIZE : "wrong number of columns";
        }
        
        Set<Integer> allowed = new HashSet<>();
        for (int number = 0; number <= PUZZLE_SIZE; ++number) {
            allowed.add(number);
        }
        
        for (int row = 0; row < PUZZLE_SIZE; ++row) {
            Set<Integer> found = new HashSet<>();
            for (int column = 0; column < PUZZLE_SIZE; ++column) {
                int cell = puzzle[row][column];
                assert allowed.contains(cell) : "invalid number " + cell;
                assert ! found.contains(cell) : "duplicate " + cell + " in row " + row;
                if (cell != 0) {
                    found.add(cell);
                }
            }
        }
        
        for (int column = 0; column < PUZZLE_SIZE; ++column) {
            Set<Integer> found = new HashSet<>();
            for (int row = 0; row < PUZZLE_SIZE; ++row) {
                int cell = puzzle[row][column];
                assert allowed.contains(cell) : "invalid number " + cell;
                assert ! found.contains(cell) : "duplicate " + cell + " in column " + column;
                if (cell != 0) {
                    found.add(cell);
                }
            }
        }

        for (int block = 0; block < PUZZLE_SIZE; ++block) {
            Set<Integer> found = new HashSet<>();
            for (int r = 0; r < BLOCK_SIZE; ++r) {
                for (int c = 0; c < BLOCK_SIZE; ++c) {
                    int row = (block/BLOCK_SIZE)*BLOCK_SIZE + r;
                    int column = (block%BLOCK_SIZE)*BLOCK_SIZE + c;1
                    int cell = puzzle[row][column];
                    assert allowed.contains(cell) : "invalid number " + cell;
                    assert ! found.contains(cell) : "duplicate " + cell + " in block " + block;
                    if (cell != 0) {
                        found.add(cell);
                    }
                }
            }
        }
    }
}
